webpackJsonp([21],{

/***/ 437:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaDetalhesModule", function() { return ListaDetalhesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lista_detalhes__ = __webpack_require__(510);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ListaDetalhesModule = (function () {
    function ListaDetalhesModule() {
    }
    return ListaDetalhesModule;
}());
ListaDetalhesModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__lista_detalhes__["a" /* ListaDetalhesPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__lista_detalhes__["a" /* ListaDetalhesPage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__lista_detalhes__["a" /* ListaDetalhesPage */]
        ]
    })
], ListaDetalhesModule);

//# sourceMappingURL=lista-detalhes.module.js.map

/***/ }),

/***/ 510:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListaDetalhesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ListaDetalhes page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ListaDetalhesPage = (function () {
    function ListaDetalhesPage(navCtrl, viewCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        if (this.navParams.get("lista")) {
            this.lista = this.navParams.get("lista");
        }
    }
    ListaDetalhesPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ListaDetalhes");
    };
    ListaDetalhesPage.prototype.dismiss = function (data) {
        this.viewCtrl.dismiss();
    };
    ListaDetalhesPage.prototype.pushCardapioPage = function (c) {
        this.navCtrl.setPages([
            { page: "HomePage" },
            { page: "CardapiosPage" },
            { page: "CardapioPage", params: { c: c } }
        ]);
    };
    return ListaDetalhesPage;
}());
ListaDetalhesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-lista-detalhes",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/lista-detalhes/lista-detalhes.html"*/'<!--\n  Generated template for the ListaDetalhes page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar hideBackButton>\n    <ion-title>Detalhes lista</ion-title>\n    <ion-buttons end>\n      <button class="close" ion-button (click)="dismiss()"><ion-icon color="light" class="fa fa-times"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n	<ion-list>\n		<ion-item>\n			<ion-label stacked>Titulo</ion-label>\n			<ion-input placeholder="Titulo" [(ngModel)]="lista.titulo"></ion-input>\n		</ion-item>\n		<ion-item *ngIf="lista.cardapio_id" (click)="pushCardapioPage(lista.cardapio_id)">\n			<ion-label stacked>Cardapio base</ion-label>\n			<ion-input disabled placeholder="Cardapio base" [(ngModel)]="lista.cardapio_id.titulo"></ion-input>\n			<button ion-button item-right clear>\n				<ion-icon class="fa fa-external-link" color="primary"></ion-icon>\n			</button>\n		</ion-item>\n		<ion-item *ngIf="lista.createdAt">\n			<ion-label stacked>Criado em</ion-label>\n			<ion-datetime disabled item-right displayFormat="DD/MM/YYYY" [(ngModel)]="lista.createdAt"></ion-datetime>\n		</ion-item>\n	</ion-list>\n</ion-content>\n<ion-footer>\n	<button ion-button (click)="dismiss()" color="secondary" full>OK</button>\n</ion-footer>'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/lista-detalhes/lista-detalhes.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */]])
], ListaDetalhesPage);

//# sourceMappingURL=lista-detalhes.js.map

/***/ })

});
//# sourceMappingURL=21.main.js.map