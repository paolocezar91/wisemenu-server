webpackJsonp([2],{

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaModule", function() { return ListaModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lista__ = __webpack_require__(509);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ListaModule = (function () {
    function ListaModule() {
    }
    return ListaModule;
}());
ListaModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__lista__["a" /* ListaPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__lista__["a" /* ListaPage */]),
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__lista__["a" /* ListaPage */]
        ]
    })
], ListaModule);

//# sourceMappingURL=lista.module.js.map

/***/ }),

/***/ 448:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = slice;
function slice(arrayLike, start) {
    start = start|0;
    var newLen = Math.max(arrayLike.length - start, 0);
    var newArr = Array(newLen);
    for(var idx = 0; idx < newLen; idx++)  {
        newArr[idx] = arrayLike[start + idx];
    }
    return newArr;
}


/***/ }),

/***/ 450:
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Listas provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var ListasProvider = (function () {
    function ListasProvider(http, ap, ENV) {
        this.http = http;
        this.ap = ap;
        this.ENV = ENV;
        console.log("Hello Listas Provider");
    }
    ListasProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (listas) {
                _this.listas = listas;
                resolve(_this.listas);
            });
        });
    };
    ListasProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (lista) {
                _this.lista = lista;
                resolve(_this.lista);
            });
        });
    };
    ListasProvider.prototype.create = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/create", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (lista) {
                _this.lista = lista;
                resolve(_this.lista);
            });
        });
    };
    ListasProvider.prototype.update = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/update", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ListasProvider.prototype.delete = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/delete", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ListasProvider.prototype.newLista = function (c) {
        var _this = this;
        return new Promise(function (resolve) {
            var titulo = c.titulo;
            console.log("c.gerado", c.gerado);
            console.log((c.gerado - 1) > 0);
            if (c.gerado - 1 > 0) {
                titulo = titulo + " (" + (c.gerado - 1) + ")";
            }
            var data = {
                titulo: titulo,
                receitas: _this.extractReceitas(c),
                user_id: _this.ap.getCurrentUser()._id,
                cardapio_id: c._id
            };
            resolve(data);
        });
    };
    ListasProvider.prototype.extractReceitas = function (c) {
        var allReceitas = [];
        for (var _i = 0, _a = c.dias; _i < _a.length; _i++) {
            var d = _a[_i];
            for (var _b = 0, _c = d.refeicoes; _b < _c.length; _b++) {
                var r = _c[_b];
                for (var _d = 0, _e = r.receitas; _d < _e.length; _d++) {
                    var re = _e[_d];
                    allReceitas.push({ _id: re._id, porcoes: re.porcoes !== undefined ? re.porcoes : c.porcoes });
                }
            }
        }
        return allReceitas;
    };
    return ListasProvider;
}());
ListasProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], ListasProvider);

//# sourceMappingURL=listas.js.map

/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash_es_isArray__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_es_noop__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__internal_once__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__internal_slice__ = __webpack_require__(448);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__internal_onlyOnce__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__internal_wrapAsync__ = __webpack_require__(459);








/**
 * Runs the `tasks` array of functions in series, each passing their results to
 * the next in the array. However, if any of the `tasks` pass an error to their
 * own callback, the next function is not executed, and the main `callback` is
 * immediately called with the error.
 *
 * @name waterfall
 * @static
 * @memberOf module:ControlFlow
 * @method
 * @category Control Flow
 * @param {Array} tasks - An array of [async functions]{@link AsyncFunction}
 * to run.
 * Each function should complete with any number of `result` values.
 * The `result` values will be passed as arguments, in order, to the next task.
 * @param {Function} [callback] - An optional callback to run once all the
 * functions have completed. This will be passed the results of the last task's
 * callback. Invoked with (err, [results]).
 * @returns undefined
 * @example
 *
 * async.waterfall([
 *     function(callback) {
 *         callback(null, 'one', 'two');
 *     },
 *     function(arg1, arg2, callback) {
 *         // arg1 now equals 'one' and arg2 now equals 'two'
 *         callback(null, 'three');
 *     },
 *     function(arg1, callback) {
 *         // arg1 now equals 'three'
 *         callback(null, 'done');
 *     }
 * ], function (err, result) {
 *     // result now equals 'done'
 * });
 *
 * // Or, with named functions:
 * async.waterfall([
 *     myFirstFunction,
 *     mySecondFunction,
 *     myLastFunction,
 * ], function (err, result) {
 *     // result now equals 'done'
 * });
 * function myFirstFunction(callback) {
 *     callback(null, 'one', 'two');
 * }
 * function mySecondFunction(arg1, arg2, callback) {
 *     // arg1 now equals 'one' and arg2 now equals 'two'
 *     callback(null, 'three');
 * }
 * function myLastFunction(arg1, callback) {
 *     // arg1 now equals 'three'
 *     callback(null, 'done');
 * }
 */
/* harmony default export */ __webpack_exports__["a"] = (function(tasks, callback) {
    callback = Object(__WEBPACK_IMPORTED_MODULE_2__internal_once__["a" /* default */])(callback || __WEBPACK_IMPORTED_MODULE_1_lodash_es_noop__["a" /* default */]);
    if (!Object(__WEBPACK_IMPORTED_MODULE_0_lodash_es_isArray__["a" /* default */])(tasks)) return callback(new Error('First argument to waterfall must be an array of functions'));
    if (!tasks.length) return callback();
    var taskIndex = 0;

    function nextTask(args) {
        var task = Object(__WEBPACK_IMPORTED_MODULE_5__internal_wrapAsync__["a" /* default */])(tasks[taskIndex++]);
        args.push(Object(__WEBPACK_IMPORTED_MODULE_4__internal_onlyOnce__["a" /* default */])(next));
        task.apply(null, args);
    }

    function next(err/*, ...args*/) {
        if (err || taskIndex === tasks.length) {
            return callback.apply(null, arguments);
        }
        nextTask(Object(__WEBPACK_IMPORTED_MODULE_3__internal_slice__["a" /* default */])(arguments, 1));
    }

    nextTask([]);
});


/***/ }),

/***/ 455:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

/* harmony default export */ __webpack_exports__["a"] = (isArray);


/***/ }),

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * This method returns `undefined`.
 *
 * @static
 * @memberOf _
 * @since 2.3.0
 * @category Util
 * @example
 *
 * _.times(2, _.noop);
 * // => [undefined, undefined]
 */
function noop() {
  // No operation performed.
}

/* harmony default export */ __webpack_exports__["a"] = (noop);


/***/ }),

/***/ 457:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = once;
function once(fn) {
    return function () {
        if (fn === null) return;
        var callFn = fn;
        fn = null;
        callFn.apply(this, arguments);
    };
}


/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = onlyOnce;
function onlyOnce(fn) {
    return function() {
        if (fn === null) throw new Error("Callback was already called.");
        var callFn = fn;
        fn = null;
        callFn.apply(this, arguments);
    };
}


/***/ }),

/***/ 459:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export isAsync */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__asyncify__ = __webpack_require__(460);


var supportsSymbol = typeof Symbol === 'function';

function isAsync(fn) {
    return supportsSymbol && fn[Symbol.toStringTag] === 'AsyncFunction';
}

function wrapAsync(asyncFn) {
    return isAsync(asyncFn) ? Object(__WEBPACK_IMPORTED_MODULE_0__asyncify__["a" /* default */])(asyncFn) : asyncFn;
}

/* harmony default export */ __webpack_exports__["a"] = (wrapAsync);




/***/ }),

/***/ 460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = asyncify;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash_es_isObject__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__internal_initialParams__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__internal_setImmediate__ = __webpack_require__(463);




/**
 * Take a sync function and make it async, passing its return value to a
 * callback. This is useful for plugging sync functions into a waterfall,
 * series, or other async functions. Any arguments passed to the generated
 * function will be passed to the wrapped function (except for the final
 * callback argument). Errors thrown will be passed to the callback.
 *
 * If the function passed to `asyncify` returns a Promise, that promises's
 * resolved/rejected state will be used to call the callback, rather than simply
 * the synchronous return value.
 *
 * This also means you can asyncify ES2017 `async` functions.
 *
 * @name asyncify
 * @static
 * @memberOf module:Utils
 * @method
 * @alias wrapSync
 * @category Util
 * @param {Function} func - The synchronous function, or Promise-returning
 * function to convert to an {@link AsyncFunction}.
 * @returns {AsyncFunction} An asynchronous wrapper of the `func`. To be
 * invoked with `(args..., callback)`.
 * @example
 *
 * // passing a regular synchronous function
 * async.waterfall([
 *     async.apply(fs.readFile, filename, "utf8"),
 *     async.asyncify(JSON.parse),
 *     function (data, next) {
 *         // data is the result of parsing the text.
 *         // If there was a parsing error, it would have been caught.
 *     }
 * ], callback);
 *
 * // passing a function returning a promise
 * async.waterfall([
 *     async.apply(fs.readFile, filename, "utf8"),
 *     async.asyncify(function (contents) {
 *         return db.model.create(contents);
 *     }),
 *     function (model, next) {
 *         // `model` is the instantiated model object.
 *         // If there was an error, this function would be skipped.
 *     }
 * ], callback);
 *
 * // es2017 example, though `asyncify` is not needed if your JS environment
 * // supports async functions out of the box
 * var q = async.queue(async.asyncify(async function(file) {
 *     var intermediateStep = await processFile(file);
 *     return await somePromise(intermediateStep)
 * }));
 *
 * q.push(files);
 */
function asyncify(func) {
    return Object(__WEBPACK_IMPORTED_MODULE_1__internal_initialParams__["a" /* default */])(function (args, callback) {
        var result;
        try {
            result = func.apply(this, args);
        } catch (e) {
            return callback(e);
        }
        // if result is Promise object
        if (Object(__WEBPACK_IMPORTED_MODULE_0_lodash_es_isObject__["a" /* default */])(result) && typeof result.then === 'function') {
            result.then(function(value) {
                invokeCallback(callback, null, value);
            }, function(err) {
                invokeCallback(callback, err.message ? err : new Error(err));
            });
        } else {
            callback(null, result);
        }
    });
}

function invokeCallback(callback, error, value) {
    try {
        callback(error, value);
    } catch (e) {
        Object(__WEBPACK_IMPORTED_MODULE_2__internal_setImmediate__["a" /* default */])(rethrow, e);
    }
}

function rethrow(error) {
    throw error;
}


/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

/* harmony default export */ __webpack_exports__["a"] = (isObject);


/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__slice__ = __webpack_require__(448);


/* harmony default export */ __webpack_exports__["a"] = (function (fn) {
    return function (/*...args, callback*/) {
        var args = Object(__WEBPACK_IMPORTED_MODULE_0__slice__["a" /* default */])(arguments);
        var callback = args.pop();
        fn.call(this, args, callback);
    };
});


/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(setImmediate, process) {/* unused harmony export hasSetImmediate */
/* unused harmony export hasNextTick */
/* unused harmony export fallback */
/* unused harmony export wrap */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__slice__ = __webpack_require__(448);




var hasSetImmediate = typeof setImmediate === 'function' && setImmediate;
var hasNextTick = typeof process === 'object' && typeof process.nextTick === 'function';

function fallback(fn) {
    setTimeout(fn, 0);
}

function wrap(defer) {
    return function (fn/*, ...args*/) {
        var args = Object(__WEBPACK_IMPORTED_MODULE_0__slice__["a" /* default */])(arguments, 1);
        defer(function () {
            fn.apply(null, args);
        });
    };
}

var _defer;

if (hasSetImmediate) {
    _defer = setImmediate;
} else if (hasNextTick) {
    _defer = process.nextTick;
} else {
    _defer = fallback;
}

/* harmony default export */ __webpack_exports__["a"] = (wrap(_defer));

/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(464).setImmediate, __webpack_require__(450)))

/***/ }),

/***/ 464:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, window, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, window, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(window, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__(465);
// On some exotic environments, it's not clear which object `setimmeidate` was
// able to install onto.  Search each possibility in the same order as the
// `setimmediate` library.
exports.setImmediate = (typeof self !== "undefined" && self.setImmediate) ||
                       (typeof global !== "undefined" && global.setImmediate) ||
                       (this && this.setImmediate);
exports.clearImmediate = (typeof self !== "undefined" && self.clearImmediate) ||
                         (typeof global !== "undefined" && global.clearImmediate) ||
                         (this && this.clearImmediate);

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(41)))

/***/ }),

/***/ 465:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(41), __webpack_require__(450)))

/***/ }),

/***/ 509:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_listas__ = __webpack_require__(451);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_async_es_waterfall__ = __webpack_require__(454);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
    Generated class for the Lista page.

    See http://ionicframework.com/docs/v2/components/#navigation for more info on
    Ionic pages and navigation.
*/
var ListaPage = (function () {
    function ListaPage(navCtrl, navParams, lp, ap, up, alertCtrl, modalCtrl, toastCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.lp = lp;
        this.ap = ap;
        this.up = up;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.showLoader();
    }
    ListaPage.prototype.ionViewDidLoad = function () {
        console.log("Hello Lista Page");
        this.init();
    };
    ListaPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando...",
        });
        this.loading.present();
    };
    ListaPage.prototype.init = function () {
        var _this = this;
        try {
            var listaParams_1 = this.navParams.get("l");
            Object(__WEBPACK_IMPORTED_MODULE_5_async_es_waterfall__["a" /* default */])([
                function (cb) {
                    // Se o usuário for visitante
                    if (_this.ap.currentUserRoleCheck("guest")) {
                        // E se for uma lista nova
                        if (listaParams_1._id === undefined) {
                            // Query pra contar o número de cardápios do usuário
                            var data = { user_id: _this.ap.getCurrentUser()._id };
                            var options = { count: true };
                            _this.lp.getAll(data, options).then(function (count) {
                                // Se o número de cardápios for maior do que 0, ele deve ser redirecionado para tela de registro, e depois redirecionando para tela de cardápio
                                if (count > 0) {
                                    cb(true);
                                }
                                else {
                                    cb(false);
                                }
                            });
                        }
                        else {
                            cb(false);
                        }
                    }
                    else {
                        cb(false);
                    }
                },
            ], function (needRegister) {
                if (needRegister) {
                    var registerAlert = _this.alertCtrl.create({
                        enableBackdropDismiss: false,
                        title: "Registrar",
                        message: "Para criar mais de uma lista você precisa se registrar.",
                        buttons: [
                            {
                                text: "Voltar",
                                handler: function (data) {
                                    _this.loading.dismiss();
                                    if (_this.navCtrl.canGoBack()) {
                                        _this.navCtrl.pop();
                                    }
                                    else {
                                        _this.navCtrl.setRoot("HomePage");
                                    }
                                }
                            },
                            {
                                text: "Se registrar",
                                handler: function (data) {
                                    _this.loading.dismiss();
                                    _this.navCtrl.setRoot("RegisterPage", { nextPage: "ListaPage" });
                                }
                            }
                        ]
                    });
                    registerAlert.present();
                }
                else {
                    _this.callback = _this.navParams.get("updateListaCB");
                    if (listaParams_1._id === undefined) {
                        _this.lista = listaParams_1;
                        _this.listaClone = _this.up.clone(_this.lista);
                        _this.loading.dismiss();
                    }
                    else {
                        var data = { _id: listaParams_1._id };
                        var options = { populate: [{ field: "cardapio_id", select: "titulo" }, { field: "itens.produto_id", select: "titulo" }] };
                        _this.lp.getOne(data, options).then(function (lista) {
                            _this.lista = lista;
                            _this.lista.itens.forEach(function (item) {
                                if (item.check === undefined) {
                                    item.check = false;
                                }
                            });
                            _this.reorderItensLista().then(function (res) {
                                _this.listaClone = _this.up.clone(_this.lista);
                                console.log(_this.listaClone, _this.lista);
                                _this.loading.dismiss();
                            });
                        });
                    }
                }
            });
        }
        catch (err) {
            console.log(err);
            this.loading.dismiss();
        }
    };
    ListaPage.prototype.adicionarValorItem = function (it) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Valor do produto",
            message: "Informar o valor de " + it.produto_id.titulo + ".",
            inputs: [
                {
                    name: "valor",
                    placeholder: it.valor,
                    type: "number"
                },
            ],
            buttons: [
                {
                    text: "Apagar",
                    handler: function (data) {
                        delete it.valor;
                        _this.showToast("Valor do item apagado");
                    }
                },
                { text: "Cancelar" },
                {
                    text: "OK",
                    handler: function (data) {
                        it.valor = Number(data.valor);
                        _this.showToast("Valor do item alterado");
                    }
                }
            ]
        });
        alert.present();
    };
    ListaPage.prototype.editarValorTotal = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Valor da lista",
            message: "Informe o valor total da lista.",
            inputs: [
                {
                    name: "valorTotal",
                    placeholder: this.valorLista(),
                    type: "number"
                },
            ],
            buttons: [
                {
                    text: "Apagar",
                    handler: function (data) {
                        delete _this.lista.valorTotal;
                        _this.showToast("Valor da lista apagado");
                    }
                },
                { text: "Cancelar" },
                {
                    text: "OK",
                    handler: function (data) {
                        _this.lista.valorTotal = data.valorTotal;
                        _this.showToast("Valor da lista alterado");
                    }
                }
            ]
        });
        alert.present();
    };
    ListaPage.prototype.removerItem = function (it) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Remover item",
            message: "Remover <strong>" + it.produto_id.titulo + "</strong>?",
            buttons: [
                { text: "Não" },
                {
                    text: "Sim",
                    handler: function (data) {
                        _this.lista.itens = _this.lista.itens.filter(function (i) { return i !== it; });
                        _this.showToast("Item removido");
                    }
                }
            ]
        });
        alert.present();
    };
    ListaPage.prototype.adicionarItem = function () {
        var _this = this;
        var modal = this.modalCtrl.create("ListaItemPage", { item: { produto_id: {} } });
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.lista.itens.push(data);
                _this.showToast("Item adicionado à lista");
                _this.reorderItensLista();
            }
        });
        modal.present();
    };
    ListaPage.prototype.editarItem = function (it, i) {
        var _this = this;
        var modal = this.modalCtrl.create("ListaItemPage", { item: it });
        modal.onDidDismiss(function (item) {
            if (item) {
                _this.lista.itens[i] = item;
                _this.showToast("Item alterado");
                _this.reorderItensLista();
            }
        });
        modal.present();
    };
    ListaPage.prototype.reorderItensLista = function () {
        var _this = this;
        return new Promise(function (resolve) {
            setTimeout(function () {
                _this.lista.itens = Object(__WEBPACK_IMPORTED_MODULE_6_lodash__["orderBy"])(_this.lista.itens, [
                    function (it) { return it["check"] || false; },
                    function (it) { return Object(__WEBPACK_IMPORTED_MODULE_6_lodash__["deburr"])(it["produto_id"]["titulo"]); }
                ], ["asc", "asc"]);
                resolve(true);
            }, 500);
        });
    };
    ListaPage.prototype.valorLista = function () {
        if (this.lista) {
            if (this.lista.valorTotal) {
                return this.lista.valorTotal.toString();
            }
            else {
                var soma = 0;
                for (var _i = 0, _a = this.lista.itens; _i < _a.length; _i++) {
                    var it_1 = _a[_i];
                    if (it_1.valor !== undefined) {
                        soma = Number(soma) + Number(it_1.valor);
                    }
                }
                return soma.toString();
            }
        }
    };
    ListaPage.prototype.saveLista = function () {
        var _this = this;
        this.updateLista(false).then(function (l) {
            if (l.upserted) {
                var data = { push: { _id: l.upserted[0]._id, titulo: _this.lista.titulo } };
                _this.callback(data).then(function () { });
            }
            else {
                var data = { update: { _id: _this.lista._id, titulo: _this.lista.titulo } };
                _this.callback(data).then(function () { });
            }
            _this.listaClone = _this.up.clone(_this.lista);
            _this.loading.dismiss();
        });
    };
    ListaPage.prototype.editListaDetalhesModal = function () {
        var modal = this.modalCtrl.create("ListaDetalhesPage", { lista: this.lista });
        modal.onDidDismiss(function (data) { });
        modal.present();
    };
    ListaPage.prototype.updateLista = function (dismiss) {
        var _this = this;
        this.showLoader();
        return new Promise(function (resolve) {
            _this.lp.update(_this.lista, {}).then(function (lista) {
                if (dismiss) {
                    _this.loading.dismiss();
                }
                _this.showToast("As alterações foram salvas");
                resolve(lista);
            });
        });
    };
    ListaPage.prototype.ionViewWillLeave = function () {
        var _this = this;
        if (this.up.isDifferent(this.lista, this.listaClone)) {
            var alert_1 = this.alertCtrl.create({
                title: "Lista alterada",
                message: "Deseja salvar alterações?",
                buttons: [
                    {
                        text: "Não",
                    },
                    {
                        text: "Sim",
                        handler: function (data) {
                            _this.updateLista(true).then(function (l) {
                                // Callback para atualizar a tela listas com os dados da nova lista
                                if (l.upserted) {
                                    var data_1 = { push: { _id: l.upserted[0]._id, titulo: _this.lista.titulo } };
                                    _this.callback(data_1).then(function () { });
                                }
                                else {
                                    var data_2 = { update: { _id: _this.lista._id, titulo: _this.lista.titulo } };
                                    _this.callback(data_2).then(function () { });
                                }
                            });
                        }
                    }
                ]
            });
            alert_1.present();
        }
    };
    ListaPage.prototype.showToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: "bottom",
            showCloseButton: true,
            closeButtonText: "X"
        });
        toast.present();
    };
    ListaPage.prototype.rootHome = function () {
        this.navCtrl.setRoot("HomePage");
    };
    return ListaPage;
}());
ListaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-lista",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/lista/lista.html"*/'<!--\n  Generated template for the Lista page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar hideBackButton>\n		<ion-buttons left>\n	      <button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n	    </ion-buttons>\n    	<ion-title *ngIf="lista">\n    		{{ lista.titulo }}\n    		<ion-icon class="fa fa-exclamation" color="danger" *ngIf="up.isDifferent(lista, listaClone)"></ion-icon>\n    	</ion-title>\n		<ion-buttons end>\n			<button class="edit" ion-button (click)="editListaDetalhesModal()"><ion-icon class="fa fa-pencil" color="light"></ion-icon></button>\n	    	<button class="back" ion-button navPop>\n	    		<img src="assets/img/retornar.png">\n	    	</button>\n	    </ion-buttons>\n  	</ion-navbar>\n</ion-header>\n\n<ion-content padding>\n	<ion-list *ngIf="lista" class="lista" border-custom>\n		<ion-list-header>\n			<ion-label>Lista</ion-label>\n			<button class="save" item-right (click)="saveLista()" ion-button clear [disabled]="up.isDifferent(lista, listaClone) ? null : true">\n				Salvar lista\n				<ion-icon class="fa fa-floppy-o" color="primary"></ion-icon>\n			</button>\n		</ion-list-header>\n		<ion-item *ngFor="let it of lista.itens; let i = index" class="item" (press)="editarItem(it, i)">\n			<ion-checkbox (ionChange)="reorderItensLista($event)" color="primary" [(ngModel)]="it.check"></ion-checkbox>\n			<ion-label>\n				<h2>{{it.produto_id.titulo}}</h2>\n				<p><span>{{it.quantidade}}</span>{{it.unidade}}</p>\n			</ion-label>\n			<button ion-button item-right clear (click)="adicionarValorItem(it)">\n				<span class="valor" *ngIf="it.valor">{{ it.valor | number : \'1.2-2\'}}</span>\n				<ion-icon class="fa fa-2x fa-dollar" color="secondary"></ion-icon>\n			</button>\n			<button ion-button item-right clear (click)="removerItem(it)">\n				<ion-icon class="fa fa-2x fa-times" color="secondary"></ion-icon>\n			</button>\n		</ion-item>\n		<ion-item *ngIf="!lista.itens.length">\n			<p>Nenhum item na lista!</p>			\n		</ion-item>\n	</ion-list>\n	\n</ion-content>\n\n<ion-footer padding-horizontal>\n	<div class="subtotal" *ngIf="lista">\n		<button ion-button (click)="editarValorTotal()" clear>\n			<ion-label color="primary">\n				Subtotal: <span>R$ {{ valorLista() | number : \'1.2-2\' }}</span>\n			</ion-label>		\n			<ion-icon class="fa fa-pencil" color="primary"></ion-icon>\n		</button>\n	</div>\n	<div class="length" *ngIf="lista">\n		<span>{{ lista.itens.length }}</span> \n		<span *ngIf="lista.itens.length > 1 || lista.itens.length == 0">itens</span> \n		<span *ngIf="lista.itens.length == 1">item</span> na lista\n	</div>\n	<ion-fab right bottom>\n		<button ion-fab mini color="primary" (click)="adicionarItem()"><ion-icon class="fa fa-plus"></ion-icon></button>\n	</ion-fab>\n</ion-footer>'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/lista/lista.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_listas__["a" /* ListasProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_listas__["a" /* ListasProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_utils__["a" /* UtilsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */]])
], ListaPage);

//# sourceMappingURL=lista.js.map

/***/ })

});
//# sourceMappingURL=2.main.js.map