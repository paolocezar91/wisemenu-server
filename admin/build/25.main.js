webpackJsonp([25],{

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminHomeModule", function() { return AdminHomeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_home__ = __webpack_require__(487);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AdminHomeModule = (function () {
    function AdminHomeModule() {
    }
    return AdminHomeModule;
}());
AdminHomeModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__admin_home__["a" /* AdminHomePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__admin_home__["a" /* AdminHomePage */]),
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__admin_home__["a" /* AdminHomePage */]
        ]
    })
], AdminHomeModule);

//# sourceMappingURL=admin-home.module.js.map

/***/ }),

/***/ 487:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AdminHome page.
 *
 * See http://ionicframework.com/docs/components/#navig for more info
 * on Ionic pages and navigation.
 */
var AdminHomePage = (function () {
    function AdminHomePage(navCtrl, ap) {
        this.navCtrl = navCtrl;
        this.ap = ap;
    }
    AdminHomePage.prototype.ionViewCanEnter = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Checagem de role do usuário
            _this.ap.currentUserRoleCheck("admin").then(function (res) {
                if (!res) {
                    reject("Não autorizado");
                }
                else {
                    resolve(true);
                }
            });
        }).catch(function (err) { return _this.navCtrl.setRoot("HomePage"); });
    };
    AdminHomePage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad AdminHome");
    };
    AdminHomePage.prototype.returnMenu = function () {
        var _this = this;
        return [
            {
                id: "receitas",
                title: "Receitas",
                callback: function () { return _this.pushPage("AdminReceitasPage"); }
            },
            {
                id: "tags",
                title: "Tags",
                callback: function () { _this.pushPage("AdminTagsPage"); }
            },
            {
                id: "receitas2",
                title: "Receitas2",
                callback: function () { _this.pushPage("AdminReceitas_2Page"); }
            }
        ];
    };
    AdminHomePage.prototype.pushPage = function (p) {
        this.navCtrl.push(p);
    };
    AdminHomePage.prototype.rootHome = function () {
        this.navCtrl.setRoot("HomePage");
    };
    return AdminHomePage;
}());
AdminHomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-admin-home",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/admin-home/admin-home.html"*/'<!--\n  Generated template for the AdminHome page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar hideBackButton>\n    <ion-buttons left>\n      <button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n    </ion-buttons>\n    <ion-title>\n    	Admin\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n	<ion-list>\n    <ion-item *ngFor="let m of returnMenu()" (click)="m.callback()">\n      <ion-label>\n        {{ m.title }}\n      </ion-label>\n    </ion-item>\n  </ion-list>\n\n  <ion-fab class="admin" bottom right>\n    <button ion-fab color="primary" (click)="rootHome()">\n      <ion-icon class="fa fa-home"></ion-icon>\n    </button>\n  </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/admin-home/admin-home.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_auth__["a" /* AuthProvider */]])
], AdminHomePage);

//# sourceMappingURL=admin-home.js.map

/***/ })

});
//# sourceMappingURL=25.main.js.map