webpackJsonp([19],{

/***/ 444:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RefeicaoCopiarModule", function() { return RefeicaoCopiarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__refeicao_copiar__ = __webpack_require__(523);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(320);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var RefeicaoCopiarModule = (function () {
    function RefeicaoCopiarModule() {
    }
    return RefeicaoCopiarModule;
}());
RefeicaoCopiarModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__refeicao_copiar__["a" /* RefeicaoCopiarPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__refeicao_copiar__["a" /* RefeicaoCopiarPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__refeicao_copiar__["a" /* RefeicaoCopiarPage */]
        ]
    })
], RefeicaoCopiarModule);

//# sourceMappingURL=refeicao-copiar.module.js.map

/***/ }),

/***/ 523:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RefeicaoCopiarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_current_cardapios__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
    Generated class for the CopiarDia page.

    See http://ionicframework.com/docs/v2/components/#navigation for more info on
    Ionic pages and navigation.
*/
var RefeicaoCopiarPage = (function () {
    function RefeicaoCopiarPage(viewCtrl, navCtrl, navParams, ccp) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ccp = ccp;
    }
    RefeicaoCopiarPage.prototype.ionViewDidLoad = function () {
        console.log("Hello CopiarRefeicao Page");
        this.init();
    };
    RefeicaoCopiarPage.prototype.init = function () {
        this.cardapio = this.navParams.get("cardapio");
        this.refeicoes = this.clone(this.navParams.get("refeicoes"));
        this.cardapioIndexPara = this.clone(this.navParams.get("cardapioIndex"));
        this.cardapioIndexDe = this.clone(this.cardapioIndexPara) === 0 ? 1 : 0;
        this.modelCardapioDiaPara = this.clone(this.cardapio.dias[this.cardapioIndexPara]);
        this.modelCardapioDiaDe = this.clone(this.cardapio.dias[this.cardapioIndexDe]);
        this.refeicoesEscolhidas = [];
    };
    RefeicaoCopiarPage.prototype.dismiss = function (data) {
        if (data) {
            // clona cada refeicao dos dias referenciados por cardapioIndexClone e cardap
            for (var receitasDiaClonado = void 0, refeicaoClonada = void 0, i = 0; i < this.refeicoesEscolhidas.length; ++i) {
                refeicaoClonada = this.refeicoesEscolhidas[i];
                receitasDiaClonado = this.cardapio.dias[this.cardapioIndexDe].refeicoes[(refeicaoClonada.ordem)].receitas;
                this.cardapio.dias[this.cardapioIndexPara].refeicoes[(refeicaoClonada.ordem)].receitas = this.clone(receitasDiaClonado);
            }
        }
        this.viewCtrl.dismiss();
    };
    // Adiciona a refeição "r" à lista de refeicoesEscolhidas
    RefeicaoCopiarPage.prototype.copiarRefeicao = function (r, i) {
        this.refeicoesEscolhidas.push(r);
        this.refeicoesEscolhidas.sort(function (a, b) {
            return (a.id > b.id ? 1 : 0);
        });
        this.refeicoes.splice(i, 1);
    };
    RefeicaoCopiarPage.prototype.copiarDiaPara = function (event) {
        if ((event.c + event.i) >= this.cardapio.dias.length) {
            this.cardapioIndexPara = 0;
        }
        else if ((event.c + event.i) < 0) {
            this.cardapioIndexPara = this.cardapio.dias.length - 1;
        }
        else {
            this.cardapioIndexPara = event.c + event.i;
        }
        this.modelCardapioDiaPara = this.clone(this.cardapio.dias[this.cardapioIndexPara]);
    };
    RefeicaoCopiarPage.prototype.copiarDiaDe = function (event) {
        if ((event.c + event.i) >= this.cardapio.dias.length) {
            this.cardapioIndexDe = 0;
        }
        else if ((event.c + event.i) < 0) {
            this.cardapioIndexDe = this.cardapio.dias.length - 1;
        }
        else {
            this.cardapioIndexDe = event.c + event.i;
        }
        this.modelCardapioDiaDe = this.clone(this.cardapio.dias[this.cardapioIndexDe]);
    };
    RefeicaoCopiarPage.prototype.clone = function (original) {
        return JSON.parse(JSON.stringify(original));
    };
    return RefeicaoCopiarPage;
}());
RefeicaoCopiarPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-refeicao-copiar",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/refeicao-copiar/refeicao-copiar.html"*/'<!--\n  Generated template for the CopiarDia page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding>\n	<ion-grid>\n		<ion-row>\n			<ion-col col-5>\n				<h1>Copiar de:</h1>\n				<dia-seletor\n				  [modelCardapioDia]="modelCardapioDiaDe" \n				  [cardapioIndex]="cardapioIndexDe" \n				  [cardapio]="cardapio"\n				  (copiarDia)="copiarDiaDe($event)">\n			  </dia-seletor>\n			</ion-col>\n			<ion-col col-2 style="display: flex; justify-content: center;">\n				<ion-icon style="align-self: center;" class="fa fa-3x fa-angle-right"></ion-icon>\n			</ion-col>\n			<ion-col col-5>\n				<h1>Copiar para:</h1>\n				<dia-seletor\n				  [modelCardapioDia]="modelCardapioDiaPara" \n				  [cardapioIndex]="cardapioIndexPara" \n				  [cardapio]="cardapio"\n				  (copiarDia)="copiarDiaPara($event)">\n			  </dia-seletor>\n			</ion-col>\n		</ion-row>\n	</ion-grid>\n\n	<refeicoes-escolhidas \n		[refeicoes]="refeicoes"\n		[refeicoesEscolhidas]="refeicoesEscolhidas">		\n	</refeicoes-escolhidas>\n\n	<ion-list class="refeicoes" border-custom>\n		<ion-item *ngFor="let r of refeicoes; let i = index;" class="refeicao" (click)="copiarRefeicao(r, i)">\n			<h2>{{r.titulo}}</h2>\n			<ion-icon class="fa fa-plus-circle" item-right color="secondary"></ion-icon>\n		</ion-item>		\n	</ion-list>\n\n</ion-content>\n<ion-footer>\n	<ion-grid no-padding>\n    <ion-row no-padding class="actions" no-padding *ngIf="cardapio">\n      <ion-col no-padding col-6 class="action">\n      <button ion-button color="danger" full (click)="dismiss(false)">Cancelar</button>\n      </ion-col>\n      <ion-col no-padding col-6 class="action">\n				<button ion-button color="secondary" full (click)="dismiss(true)">Concluído</button>\n      </ion-col>\n		</ion-row>\n	</ion-grid>\n</ion-footer>'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/refeicao-copiar/refeicao-copiar.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_current_cardapios__["a" /* CurrentCardapiosProvider */]])
], RefeicaoCopiarPage);

//# sourceMappingURL=refeicao-copiar.js.map

/***/ })

});
//# sourceMappingURL=19.main.js.map