webpackJsonp([24],{

/***/ 424:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminReceitas_2PageModule", function() { return AdminReceitas_2PageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_receitas_2__ = __webpack_require__(494);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AdminReceitas_2PageModule = (function () {
    function AdminReceitas_2PageModule() {
    }
    return AdminReceitas_2PageModule;
}());
AdminReceitas_2PageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__admin_receitas_2__["a" /* AdminReceitas_2Page */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__admin_receitas_2__["a" /* AdminReceitas_2Page */]),
        ],
    })
], AdminReceitas_2PageModule);

//# sourceMappingURL=admin-receitas-2.module.js.map

/***/ }),

/***/ 494:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminReceitas_2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AdminReceitas_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdminReceitas_2Page = (function () {
    function AdminReceitas_2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AdminReceitas_2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminReceitas_2Page');
    };
    AdminReceitas_2Page.prototype.rootAdminHome = function () {
        this.navCtrl.setRoot("AdminHomePage");
    };
    AdminReceitas_2Page.prototype.pushPage = function (p, param) {
        this.navCtrl.push(p, { title: param });
    };
    return AdminReceitas_2Page;
}());
AdminReceitas_2Page = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-admin-receitas-2',template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/admin-receitas-2/admin-receitas-2.html"*/'<!--\n  Generated template for the AdminReceitas_2Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar hideBackButton>\n		<ion-buttons left>\n      <button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n    </ion-buttons>\n		<ion-title>\n			Receitas\n		</ion-title>\n		<ion-buttons end>\n			<button class="back" ion-button (click)="rootAdminHome()"><img src="assets/img/retornar.png"></button>\n		</ion-buttons>\n	</ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n	<div padding full style=\'background-color: #113484; color: #fff; text-align: center\'>ORGANIZAR POR</div>\n	<div full style=\'background-color: #113484\'>\n		<button ion-button full (click)="pushPage(\'BuscaReceitasPage\', \'tipo_de_prato\')">TIPO DE PRATO</button>\n		<button ion-button full (click)="pushPage(\'BuscaReceitasPage\', \'nacionalidade\')">NACIONALIDADE</button>\n		<button ion-button full (click)="pushPage(\'BuscaReceitasPage\', \'preparo\')">PREPARO</button>\n		<button ion-button full (click)="pushPage(\'BuscaReceitasPage\', \'tempo\')">TEMPO</button>\n		<button ion-button full (click)="pushPage(\'BuscaReceitasPage\', \'dificuldade\')">DIFICULDADE</button>\n		<button ion-button full (click)="pushPage(\'BuscaReceitasPage\', \'avaliacoes\')">AVALIAÇÕES</button>\n	</div>\n</ion-content>\n'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/admin-receitas-2/admin-receitas-2.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */]])
], AdminReceitas_2Page);

//# sourceMappingURL=admin-receitas-2.js.map

/***/ })

});
//# sourceMappingURL=24.main.js.map