webpackJsonp([14],{

/***/ 428:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvaliacoesModule", function() { return AvaliacoesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__avaliacoes__ = __webpack_require__(499);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AvaliacoesModule = (function () {
    function AvaliacoesModule() {
    }
    return AvaliacoesModule;
}());
AvaliacoesModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__avaliacoes__["a" /* AvaliacoesPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__avaliacoes__["a" /* AvaliacoesPage */]),
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__avaliacoes__["a" /* AvaliacoesPage */]
        ]
    })
], AvaliacoesModule);

//# sourceMappingURL=avaliacoes.module.js.map

/***/ }),

/***/ 452:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
  Generated class for the Favoritos provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var FavoritosProvider = (function () {
    function FavoritosProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
        console.log('Hello Filtros Provider');
    }
    FavoritosProvider.prototype.newFavorito = function (receita_id) {
        return {
            "user_id": this.authProvider.getCurrentUser()._id,
            "receita_id": receita_id
        };
    };
    FavoritosProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Authorization', _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + 'favoritos/one', { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (favorito) {
                _this.favorito = favorito;
                resolve(favorito);
            });
        });
    };
    FavoritosProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Authorization', _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + 'favoritos', { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (favoritos) {
                _this.favoritos = favoritos;
                resolve(favoritos);
            });
        });
    };
    FavoritosProvider.prototype.update = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Authorization', _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + 'favoritos/update', { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (favorito) {
                resolve(favorito);
            });
        });
    };
    return FavoritosProvider;
}());
FavoritosProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], FavoritosProvider);

//# sourceMappingURL=favoritos.js.map

/***/ }),

/***/ 499:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AvaliacoesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_favoritos__ = __webpack_require__(452);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the Avaliacoes page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AvaliacoesPage = (function () {
    function AvaliacoesPage(navCtrl, navParams, favp, ap, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.favp = favp;
        this.ap = ap;
        this.loadingCtrl = loadingCtrl;
        this.segment = "receitas";
        this.showLoader();
    }
    AvaliacoesPage.prototype.ionViewDidLoad = function () {
        console.log("Hello Filtros Page");
        this.init();
    };
    AvaliacoesPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando..."
        });
        this.loading.present();
    };
    AvaliacoesPage.prototype.init = function () {
        var _this = this;
        try {
            if (this.ap.currentUserRoleCheck("guest")) {
                this.loading.dismiss();
            }
            else {
                var data = { user_id: this.ap.getCurrentUser()._id, active: true };
                var options = { select: "receita_id", populate: { path: "receita_id", select: "titulo" } };
                this.favp.getAll(data, options).then(function (receitas) {
                    _this.receitas = receitas;
                    console.log("this.receitas", _this.receitas);
                    _this.loading.dismiss();
                });
            }
        }
        catch (err) {
            console.log(err);
            this.loading.dismiss();
        }
    };
    AvaliacoesPage.prototype.rootHome = function () {
        this.navCtrl.setRoot("HomePage");
    };
    return AvaliacoesPage;
}());
AvaliacoesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-avaliacoes",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/avaliacoes/avaliacoes.html"*/'<!--\n	Generated template for the Avaliacoes page.\n\n	See http://ionicframework.com/docs/components/#navigation for more info on\n	Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar hideBackButton>\n		<ion-buttons left>\n			<button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n		</ion-buttons>\n		<ion-title>Avaliações</ion-title>    \n		<ion-buttons end>\n			<button class="back" ion-button (click)="rootHome()"><img src="assets/img/retornar.png"></button>\n		</ion-buttons>\n	</ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n	<ion-segment mode="md" [(ngModel)]="segment">\n		<ion-segment-button value="receitas">\n			 Receitas\n		</ion-segment-button>\n		<ion-segment-button disabled value="mercados" *ngIf="false">\n			Mercados\n		</ion-segment-button>\n	</ion-segment>\n\n	<div [ngSwitch]="receitas" *ngIf="receitas">\n		<ion-list>\n			<ion-item *ngFor="let r of receitas">\n				{{ r.receita_id.titulo }}\n			</ion-item>\n		</ion-list>\n	</div>\n	<div [ngSwitch]="mercados" *ngIf="mercados">\n		\n	</div>\n\n</ion-content>\n'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/avaliacoes/avaliacoes.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_favoritos__["a" /* FavoritosProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_favoritos__["a" /* FavoritosProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */]])
], AvaliacoesPage);

//# sourceMappingURL=avaliacoes.js.map

/***/ })

});
//# sourceMappingURL=14.main.js.map