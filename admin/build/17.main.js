webpackJsonp([17],{

/***/ 425:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminTagPageModule", function() { return AdminTagPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_tag__ = __webpack_require__(495);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AdminTagPageModule = (function () {
    function AdminTagPageModule() {
    }
    return AdminTagPageModule;
}());
AdminTagPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__admin_tag__["a" /* AdminTagPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__admin_tag__["a" /* AdminTagPage */]),
        ],
    })
], AdminTagPageModule);

//# sourceMappingURL=admin-tag.module.js.map

/***/ }),

/***/ 449:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TagsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Tags provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var TagsProvider = (function () {
    function TagsProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
        console.log("Hello Tags Provider");
    }
    TagsProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.token);
            _this.http.post(_this.ENV.API_URL + "tags", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TagsProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.token);
            _this.http.post(_this.ENV.API_URL + "tags/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TagsProvider.prototype.createOrUpdate = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "tags/createOrUpdate", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    return TagsProvider;
}());
TagsProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], TagsProvider);

//# sourceMappingURL=tags.js.map

/***/ }),

/***/ 495:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminTagPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_tags__ = __webpack_require__(449);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the AdminTagPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AdminTagPage = (function () {
    function AdminTagPage(navCtrl, navParams, tp, ap, loadingCtrl, alertCtrl, toastCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tp = tp;
        this.ap = ap;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.events = events;
    }
    AdminTagPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad AdminReceita");
        this.init();
    };
    AdminTagPage.prototype.ionViewCanEnter = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Checagem de role do usuário
            _this.ap.currentUserRoleCheck("admin").then(function (res) {
                if (!res) {
                    reject("Não autorizado");
                }
                else {
                    resolve(true);
                }
            });
        }).catch(function (err) { return _this.navCtrl.setRoot("HomePage"); });
    };
    AdminTagPage.prototype.ionViewWillLeave = function () {
        var _this = this;
        if (this.isDifferent()) {
            var alert_1 = this.alertCtrl.create({
                title: "Receita alterada",
                message: "Deseja salvar alterações?",
                buttons: [
                    {
                        text: "Não",
                        handler: function (data) { }
                    },
                    {
                        text: "Sim",
                        handler: function (data) {
                            _this.createOrUpdateTag();
                        }
                    }
                ]
            });
            alert_1.present();
        }
        this.events.unsubscribe("reloadPage");
    };
    AdminTagPage.prototype.init = function () {
        try {
            this.getTag();
        }
        catch (err) {
            console.log(err);
        }
    };
    AdminTagPage.prototype.getTag = function () {
        var _this = this;
        if (this.navParams.get("_id") !== "new") {
            var data = { _id: this.navParams.get("_id") }, options = {};
            this.showLoader();
            this.tp.getOne(data, options).then(function (tag) {
                _this.tag = tag;
                _this.tagClone = _this.clone(_this.tag);
                _this.loading.dismiss();
            }).catch(function (err) {
                _this.loading.dismiss();
            });
        }
        else {
            this.tag = {
                titulo: "",
                tipo: ""
            };
            this.tagClone = this.clone(this.tag);
        }
    };
    AdminTagPage.prototype.createOrUpdateTag = function () {
        var _this = this;
        this.showLoader();
        this.tp.createOrUpdate(this.tag, {}).then(function (tag) {
            // if its a create, should reload on back to previous page
            if (!tag["ok"]) {
                _this.events.publish("reloadPage");
                _this.tag = tag;
                _this.tagClone = _this.clone(_this.tag);
            }
            else {
                _this.tagClone = _this.clone(_this.tag);
            }
            _this.showToast("As alterações foram salvas");
            _this.loading.dismiss();
        });
    };
    AdminTagPage.prototype.rootAdminHome = function () {
        this.navCtrl.setRoot("AdminHomePage");
    };
    AdminTagPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando..."
        });
        this.loading.present();
    };
    AdminTagPage.prototype.showToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: "bottom",
            showCloseButton: true,
            closeButtonText: "X"
        });
        toast.present();
    };
    AdminTagPage.prototype.clone = function (original) {
        return JSON.parse(JSON.stringify(original));
    };
    AdminTagPage.prototype.isDifferent = function () {
        return !__WEBPACK_IMPORTED_MODULE_4_lodash___default.a.isEqual(this.tag, this.tagClone);
    };
    return AdminTagPage;
}());
AdminTagPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])({
        segment: "admin-tag/:_id"
    }),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-admin-tag",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/admin-tag/admin-tag.html"*/'<!--\n  Generated template for the AdminTagPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar hideBackButton>\n    <ion-buttons left>\n      <button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n    </ion-buttons>\n    <ion-title *ngIf="tag">\n    	{{tag.titulo}}\n    	<ion-icon class="fa fa-exclamation" color="danger" *ngIf="isDifferent()"></ion-icon>\n    </ion-title>\n  	<ion-buttons end>\n			<button class="back" ion-button navPop><img src="assets/img/retornar.png"></button>\n		</ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n	<div *ngIf="tag">\n		<ion-grid class="tag">\n			<ion-row>\n				<ion-col col-6>\n					<ion-label class="bold" for="titulo">Título</ion-label>\n					<ion-input type="text" [(ngModel)]="tag.titulo"></ion-input>\n				</ion-col>\n			</ion-row>\n			<ion-row>\n				<ion-col col-6>\n					<ion-label class="bold" for="tipo">Tipo</ion-label>\n					<ion-select [(ngModel)]="tag.tipo">\n						<ion-option value="filho">filho</ion-option>\n						<ion-option value="nacionalidade">nacionalidade</ion-option>\n						<ion-option value="normal">normal</ion-option>\n						<ion-option value="porcao_sugerida">porcao_sugerida</ion-option>\n						<ion-option value="refeicao">refeicao</ion-option>\n						<ion-option value="tempo_de_preparo">tempo_de_preparo</ion-option>\n						<ion-option value="tipo_de_preparo">tipo_de_preparo</ion-option>\n						<ion-option value="tipo_de_prato">tipo_de_prato</ion-option>\n						<ion-option value="dificuldade">dificuldade</ion-option>\n						<ion-option value="avaliacoes">avaliacoes</ion-option>\n					</ion-select>\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</div>\n	<ion-footer>\n  <button ion-button color="primary" full (click)="createOrUpdateTag()">\n  	Salvar &nbsp;\n  	<i class="fa fa-floppy-o"></i>\n	</button>\n</ion-footer>\n</ion-content>\n'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/admin-tag/admin-tag.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_tags__["a" /* TagsProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_tags__["a" /* TagsProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */]])
], AdminTagPage);

//# sourceMappingURL=admin-tag.js.map

/***/ })

});
//# sourceMappingURL=17.main.js.map