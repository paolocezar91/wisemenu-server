webpackJsonp([16],{

/***/ 426:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminTagsPageModule", function() { return AdminTagsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_tags__ = __webpack_require__(496);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(320);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AdminTagsPageModule = (function () {
    function AdminTagsPageModule() {
    }
    return AdminTagsPageModule;
}());
AdminTagsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__admin_tags__["a" /* AdminTagsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__admin_tags__["a" /* AdminTagsPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
        ],
    })
], AdminTagsPageModule);

//# sourceMappingURL=admin-tags.module.js.map

/***/ }),

/***/ 449:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TagsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Tags provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var TagsProvider = (function () {
    function TagsProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
        console.log("Hello Tags Provider");
    }
    TagsProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.token);
            _this.http.post(_this.ENV.API_URL + "tags", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TagsProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.token);
            _this.http.post(_this.ENV.API_URL + "tags/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    TagsProvider.prototype.createOrUpdate = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "tags/createOrUpdate", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    return TagsProvider;
}());
TagsProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], TagsProvider);

//# sourceMappingURL=tags.js.map

/***/ }),

/***/ 496:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminTagsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_tags__ = __webpack_require__(449);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AdminTags page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AdminTagsPage = (function () {
    function AdminTagsPage(navCtrl, ap, tp, loadingCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.ap = ap;
        this.tp = tp;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        // este objeto direciona a criação da table-admin com os parametros corretos
        this.options = {
            cols: [
                {
                    label: "Título",
                    attr: "titulo"
                },
                {
                    label: "Tipo",
                    attr: "tipo"
                }
            ],
            page: 1,
            limit: 25,
            buttons: [
                {
                    label: "Novo",
                    color: "primary",
                    icon: "fa-plus",
                    callback: function () { _this.pushAdminTagPage({ _id: "new" }); }
                }
            ]
        };
    }
    AdminTagsPage.prototype.ionViewCanEnter = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Checagem de role do usuário
            _this.ap.currentUserRoleCheck("admin").then(function (res) {
                if (!res) {
                    reject("Não autorizado");
                }
                else {
                    resolve(true);
                }
            });
        }).catch(function (err) { return _this.navCtrl.setRoot("HomePage"); });
    };
    AdminTagsPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad AdminTags");
        this.getTags(this.options.page, this.options.limit);
    };
    AdminTagsPage.prototype.getTags = function (page, limit) {
        var _this = this;
        try {
            var data = {}, options = { "page": page, "limit": limit, select: "titulo tipo", sort: "tipo titulo" };
            this.showLoader();
            this.tp.getAll(data, options).then(function (receitas) {
                _this.items = receitas;
                _this.loading.dismiss();
            });
        }
        catch (err) {
            console.log(err);
            this.loading.dismiss();
        }
    };
    AdminTagsPage.prototype.setOptions = function (options) {
        this.options = options;
        this.getTags(this.options.page, this.options.limit);
    };
    AdminTagsPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando..."
        });
        this.loading.present();
    };
    // importExcel() {
    // 	this.fileInput.nativeElement.click()
    // }
    // uploadXls(fileInput: any) {
    // 	this.filesToUpload = <Array<File>> fileInput.target.files
    // 	const params = {
    // 		attr: fileInput.target.name,
    // 	}
    // 	this.showLoader()
    // 	this.rp.uploadXls(params, this.filesToUpload).then(result => {
    // 		this.loading.dismiss()
    // 		this.getTags(this.options.page, this.options.limit)
    // 	}, (err) => {
    // 		this.loading.dismiss()
    // 	})
    // }
    // removeItem(event) {
    // 	const data = {_id: event._id}, options = {"page": this.options.page, "limit": this.options.limit, select: "titulo"}
    // 	this.showLoader()
    // 	// Recarrega receitas
    // 	this.rp.removeReceita(data, options).then(receitas => {
    // 		this.items = receitas
    // 		this.loading.dismiss()
    // 	})
    // }
    AdminTagsPage.prototype.pushAdminTagPage = function (item) {
        var _this = this;
        this.events.subscribe("reloadPage", function () {
            _this.getTags(_this.options.page, _this.options.limit);
        });
        this.navCtrl.push("AdminTagPage", { _id: item._id });
    };
    AdminTagsPage.prototype.rootAdminHome = function () {
        this.navCtrl.setRoot("AdminHomePage");
    };
    return AdminTagsPage;
}());
AdminTagsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-admin-tags",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/admin-tags/admin-tags.html"*/'<!--\n  Generated template for the AdminTags page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar hideBackButton>\n		<ion-buttons left>\n      <button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n    </ion-buttons>\n		<ion-title>\n			Tags\n		</ion-title>\n		<ion-buttons end>\n			<button class="back" ion-button (click)="rootAdminHome()"><img src="assets/img/retornar.png"></button>\n		</ion-buttons>\n	</ion-navbar>\n</ion-header>\n\n<ion-content padding>\n	<h1>Tags</h1>\n\n	<admin-table *ngIf="items" \n		[items]="items"\n		[options]="options"\n		(setOptions)="setOptions($event)"\n		(editItem)="pushAdminTagPage($event)"\n		(removeItem)="removeItem($event)">\n	</admin-table>\n</ion-content>\n'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/admin-tags/admin-tags.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_tags__["a" /* TagsProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_tags__["a" /* TagsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */]])
], AdminTagsPage);

//# sourceMappingURL=admin-tags.js.map

/***/ })

});
//# sourceMappingURL=16.main.js.map