webpackJsonp([0],{

/***/ 430:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardapioModule", function() { return CardapioModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cardapio__ = __webpack_require__(501);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(320);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CardapioModule = (function () {
    function CardapioModule() {
    }
    return CardapioModule;
}());
CardapioModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__cardapio__["a" /* CardapioPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cardapio__["a" /* CardapioPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__cardapio__["a" /* CardapioPage */]
        ]
    })
], CardapioModule);

//# sourceMappingURL=cardapio.module.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReceitasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var ReceitasProvider = (function () {
    function ReceitasProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
    }
    ReceitasProvider.prototype.getReceitas = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.getReceitasFiltered = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/filtered", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    // Receita única
    ReceitasProvider.prototype.getReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.avaliarReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/avaliar", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.createOrUpdateReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/createOrUpdate", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.removeReceita = function (data, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/remove", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.upload = function (params, files, path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("attr", params.attr);
            for (var i = 0; i < files.length; i++) {
                formData.append("upload", files[i], files[i].name);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.response);
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", _this.ENV.API_URL + path, true);
            xhr.setRequestHeader("Authorization", _this.authProvider.getToken());
            xhr.send(formData);
        });
    };
    ReceitasProvider.prototype.uploadXls = function (params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.upload(params, files, "receitas/uploadXls")
                .then(function (result) {
                resolve(result);
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ReceitasProvider.prototype.uploadImg = function (params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.upload(params, files, "receitas/uploadImage");
        });
    };
    return ReceitasProvider;
}());
ReceitasProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], ReceitasProvider);

//# sourceMappingURL=receitas.js.map

/***/ }),

/***/ 448:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = slice;
function slice(arrayLike, start) {
    start = start|0;
    var newLen = Math.max(arrayLike.length - start, 0);
    var newArr = Array(newLen);
    for(var idx = 0; idx < newLen; idx++)  {
        newArr[idx] = arrayLike[start + idx];
    }
    return newArr;
}


/***/ }),

/***/ 450:
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Listas provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var ListasProvider = (function () {
    function ListasProvider(http, ap, ENV) {
        this.http = http;
        this.ap = ap;
        this.ENV = ENV;
        console.log("Hello Listas Provider");
    }
    ListasProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (listas) {
                _this.listas = listas;
                resolve(_this.listas);
            });
        });
    };
    ListasProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (lista) {
                _this.lista = lista;
                resolve(_this.lista);
            });
        });
    };
    ListasProvider.prototype.create = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/create", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (lista) {
                _this.lista = lista;
                resolve(_this.lista);
            });
        });
    };
    ListasProvider.prototype.update = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/update", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ListasProvider.prototype.delete = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/delete", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ListasProvider.prototype.newLista = function (c) {
        var _this = this;
        return new Promise(function (resolve) {
            var titulo = c.titulo;
            console.log("c.gerado", c.gerado);
            console.log((c.gerado - 1) > 0);
            if (c.gerado - 1 > 0) {
                titulo = titulo + " (" + (c.gerado - 1) + ")";
            }
            var data = {
                titulo: titulo,
                receitas: _this.extractReceitas(c),
                user_id: _this.ap.getCurrentUser()._id,
                cardapio_id: c._id
            };
            resolve(data);
        });
    };
    ListasProvider.prototype.extractReceitas = function (c) {
        var allReceitas = [];
        for (var _i = 0, _a = c.dias; _i < _a.length; _i++) {
            var d = _a[_i];
            for (var _b = 0, _c = d.refeicoes; _b < _c.length; _b++) {
                var r = _c[_b];
                for (var _d = 0, _e = r.receitas; _d < _e.length; _d++) {
                    var re = _e[_d];
                    allReceitas.push({ _id: re._id, porcoes: re.porcoes !== undefined ? re.porcoes : c.porcoes });
                }
            }
        }
        return allReceitas;
    };
    return ListasProvider;
}());
ListasProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], ListasProvider);

//# sourceMappingURL=listas.js.map

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardapiosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Cardapios provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var CardapiosProvider = (function () {
    function CardapiosProvider(http, ap, ENV) {
        this.http = http;
        this.ap = ap;
        this.ENV = ENV;
        console.log("Hello Cardapios Provider");
    }
    CardapiosProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.create = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/create", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.update = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/update", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.delete = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/delete", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    return CardapiosProvider;
}());
CardapiosProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], CardapiosProvider);

//# sourceMappingURL=cardapios.js.map

/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash_es_isArray__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_es_noop__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__internal_once__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__internal_slice__ = __webpack_require__(448);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__internal_onlyOnce__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__internal_wrapAsync__ = __webpack_require__(459);








/**
 * Runs the `tasks` array of functions in series, each passing their results to
 * the next in the array. However, if any of the `tasks` pass an error to their
 * own callback, the next function is not executed, and the main `callback` is
 * immediately called with the error.
 *
 * @name waterfall
 * @static
 * @memberOf module:ControlFlow
 * @method
 * @category Control Flow
 * @param {Array} tasks - An array of [async functions]{@link AsyncFunction}
 * to run.
 * Each function should complete with any number of `result` values.
 * The `result` values will be passed as arguments, in order, to the next task.
 * @param {Function} [callback] - An optional callback to run once all the
 * functions have completed. This will be passed the results of the last task's
 * callback. Invoked with (err, [results]).
 * @returns undefined
 * @example
 *
 * async.waterfall([
 *     function(callback) {
 *         callback(null, 'one', 'two');
 *     },
 *     function(arg1, arg2, callback) {
 *         // arg1 now equals 'one' and arg2 now equals 'two'
 *         callback(null, 'three');
 *     },
 *     function(arg1, callback) {
 *         // arg1 now equals 'three'
 *         callback(null, 'done');
 *     }
 * ], function (err, result) {
 *     // result now equals 'done'
 * });
 *
 * // Or, with named functions:
 * async.waterfall([
 *     myFirstFunction,
 *     mySecondFunction,
 *     myLastFunction,
 * ], function (err, result) {
 *     // result now equals 'done'
 * });
 * function myFirstFunction(callback) {
 *     callback(null, 'one', 'two');
 * }
 * function mySecondFunction(arg1, arg2, callback) {
 *     // arg1 now equals 'one' and arg2 now equals 'two'
 *     callback(null, 'three');
 * }
 * function myLastFunction(arg1, callback) {
 *     // arg1 now equals 'three'
 *     callback(null, 'done');
 * }
 */
/* harmony default export */ __webpack_exports__["a"] = (function(tasks, callback) {
    callback = Object(__WEBPACK_IMPORTED_MODULE_2__internal_once__["a" /* default */])(callback || __WEBPACK_IMPORTED_MODULE_1_lodash_es_noop__["a" /* default */]);
    if (!Object(__WEBPACK_IMPORTED_MODULE_0_lodash_es_isArray__["a" /* default */])(tasks)) return callback(new Error('First argument to waterfall must be an array of functions'));
    if (!tasks.length) return callback();
    var taskIndex = 0;

    function nextTask(args) {
        var task = Object(__WEBPACK_IMPORTED_MODULE_5__internal_wrapAsync__["a" /* default */])(tasks[taskIndex++]);
        args.push(Object(__WEBPACK_IMPORTED_MODULE_4__internal_onlyOnce__["a" /* default */])(next));
        task.apply(null, args);
    }

    function next(err/*, ...args*/) {
        if (err || taskIndex === tasks.length) {
            return callback.apply(null, arguments);
        }
        nextTask(Object(__WEBPACK_IMPORTED_MODULE_3__internal_slice__["a" /* default */])(arguments, 1));
    }

    nextTask([]);
});


/***/ }),

/***/ 455:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

/* harmony default export */ __webpack_exports__["a"] = (isArray);


/***/ }),

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * This method returns `undefined`.
 *
 * @static
 * @memberOf _
 * @since 2.3.0
 * @category Util
 * @example
 *
 * _.times(2, _.noop);
 * // => [undefined, undefined]
 */
function noop() {
  // No operation performed.
}

/* harmony default export */ __webpack_exports__["a"] = (noop);


/***/ }),

/***/ 457:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = once;
function once(fn) {
    return function () {
        if (fn === null) return;
        var callFn = fn;
        fn = null;
        callFn.apply(this, arguments);
    };
}


/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = onlyOnce;
function onlyOnce(fn) {
    return function() {
        if (fn === null) throw new Error("Callback was already called.");
        var callFn = fn;
        fn = null;
        callFn.apply(this, arguments);
    };
}


/***/ }),

/***/ 459:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export isAsync */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__asyncify__ = __webpack_require__(460);


var supportsSymbol = typeof Symbol === 'function';

function isAsync(fn) {
    return supportsSymbol && fn[Symbol.toStringTag] === 'AsyncFunction';
}

function wrapAsync(asyncFn) {
    return isAsync(asyncFn) ? Object(__WEBPACK_IMPORTED_MODULE_0__asyncify__["a" /* default */])(asyncFn) : asyncFn;
}

/* harmony default export */ __webpack_exports__["a"] = (wrapAsync);




/***/ }),

/***/ 460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = asyncify;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash_es_isObject__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__internal_initialParams__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__internal_setImmediate__ = __webpack_require__(463);




/**
 * Take a sync function and make it async, passing its return value to a
 * callback. This is useful for plugging sync functions into a waterfall,
 * series, or other async functions. Any arguments passed to the generated
 * function will be passed to the wrapped function (except for the final
 * callback argument). Errors thrown will be passed to the callback.
 *
 * If the function passed to `asyncify` returns a Promise, that promises's
 * resolved/rejected state will be used to call the callback, rather than simply
 * the synchronous return value.
 *
 * This also means you can asyncify ES2017 `async` functions.
 *
 * @name asyncify
 * @static
 * @memberOf module:Utils
 * @method
 * @alias wrapSync
 * @category Util
 * @param {Function} func - The synchronous function, or Promise-returning
 * function to convert to an {@link AsyncFunction}.
 * @returns {AsyncFunction} An asynchronous wrapper of the `func`. To be
 * invoked with `(args..., callback)`.
 * @example
 *
 * // passing a regular synchronous function
 * async.waterfall([
 *     async.apply(fs.readFile, filename, "utf8"),
 *     async.asyncify(JSON.parse),
 *     function (data, next) {
 *         // data is the result of parsing the text.
 *         // If there was a parsing error, it would have been caught.
 *     }
 * ], callback);
 *
 * // passing a function returning a promise
 * async.waterfall([
 *     async.apply(fs.readFile, filename, "utf8"),
 *     async.asyncify(function (contents) {
 *         return db.model.create(contents);
 *     }),
 *     function (model, next) {
 *         // `model` is the instantiated model object.
 *         // If there was an error, this function would be skipped.
 *     }
 * ], callback);
 *
 * // es2017 example, though `asyncify` is not needed if your JS environment
 * // supports async functions out of the box
 * var q = async.queue(async.asyncify(async function(file) {
 *     var intermediateStep = await processFile(file);
 *     return await somePromise(intermediateStep)
 * }));
 *
 * q.push(files);
 */
function asyncify(func) {
    return Object(__WEBPACK_IMPORTED_MODULE_1__internal_initialParams__["a" /* default */])(function (args, callback) {
        var result;
        try {
            result = func.apply(this, args);
        } catch (e) {
            return callback(e);
        }
        // if result is Promise object
        if (Object(__WEBPACK_IMPORTED_MODULE_0_lodash_es_isObject__["a" /* default */])(result) && typeof result.then === 'function') {
            result.then(function(value) {
                invokeCallback(callback, null, value);
            }, function(err) {
                invokeCallback(callback, err.message ? err : new Error(err));
            });
        } else {
            callback(null, result);
        }
    });
}

function invokeCallback(callback, error, value) {
    try {
        callback(error, value);
    } catch (e) {
        Object(__WEBPACK_IMPORTED_MODULE_2__internal_setImmediate__["a" /* default */])(rethrow, e);
    }
}

function rethrow(error) {
    throw error;
}


/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

/* harmony default export */ __webpack_exports__["a"] = (isObject);


/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__slice__ = __webpack_require__(448);


/* harmony default export */ __webpack_exports__["a"] = (function (fn) {
    return function (/*...args, callback*/) {
        var args = Object(__WEBPACK_IMPORTED_MODULE_0__slice__["a" /* default */])(arguments);
        var callback = args.pop();
        fn.call(this, args, callback);
    };
});


/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(setImmediate, process) {/* unused harmony export hasSetImmediate */
/* unused harmony export hasNextTick */
/* unused harmony export fallback */
/* unused harmony export wrap */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__slice__ = __webpack_require__(448);




var hasSetImmediate = typeof setImmediate === 'function' && setImmediate;
var hasNextTick = typeof process === 'object' && typeof process.nextTick === 'function';

function fallback(fn) {
    setTimeout(fn, 0);
}

function wrap(defer) {
    return function (fn/*, ...args*/) {
        var args = Object(__WEBPACK_IMPORTED_MODULE_0__slice__["a" /* default */])(arguments, 1);
        defer(function () {
            fn.apply(null, args);
        });
    };
}

var _defer;

if (hasSetImmediate) {
    _defer = setImmediate;
} else if (hasNextTick) {
    _defer = process.nextTick;
} else {
    _defer = fallback;
}

/* harmony default export */ __webpack_exports__["a"] = (wrap(_defer));

/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(464).setImmediate, __webpack_require__(450)))

/***/ }),

/***/ 464:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, window, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, window, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(window, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__(465);
// On some exotic environments, it's not clear which object `setimmeidate` was
// able to install onto.  Search each possibility in the same order as the
// `setimmediate` library.
exports.setImmediate = (typeof self !== "undefined" && self.setImmediate) ||
                       (typeof global !== "undefined" && global.setImmediate) ||
                       (this && this.setImmediate);
exports.clearImmediate = (typeof self !== "undefined" && self.clearImmediate) ||
                         (typeof global !== "undefined" && global.clearImmediate) ||
                         (this && this.clearImmediate);

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(41)))

/***/ }),

/***/ 465:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(41), __webpack_require__(450)))

/***/ }),

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RefeicoesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Refeicoes provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var RefeicoesProvider = (function () {
    function RefeicoesProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
        console.log("Hello Refeicoes Provider");
    }
    // options serão queries para o banco de dados
    RefeicoesProvider.prototype.getRefeicoes = function (options) {
        return [
            {
                ordem: 0,
                icone: "cafe.png",
                titulo: "Café-da-manhã",
                tag: "58f9076457a4bd2cfca9e85e"
            },
            {
                ordem: 1,
                icone: "lanchemanha.png",
                titulo: "Lanche da manhã",
                tag: "58f9076457a4bd2cfca9e862"
            },
            {
                ordem: 2,
                icone: "almoco.png",
                titulo: "Almoço",
                tag: "58f9076457a4bd2cfca9e85f"
            },
            {
                ordem: 3,
                icone: "lanchetarde.png",
                titulo: "Lanche da tarde",
                tag: "58f9076457a4bd2cfca9e862"
            },
            {
                ordem: 4,
                icone: "jantar.png",
                titulo: "Jantar",
                tag: "58f9076457a4bd2cfca9e860"
            },
            {
                ordem: 5,
                icone: "ceia.png",
                titulo: "Ceia",
                tag: "58f9076457a4bd2cfca9e861"
            },
        ];
    };
    // Refeicao única
    RefeicoesProvider.prototype.getRefeicao = function (receita_id) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.token);
            _this.http.get(_this.ENV.API_URL + "refeicoes/" + receita_id, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    return RefeicoesProvider;
}());
RefeicoesProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__auth__["a" /* AuthProvider */], Object])
], RefeicoesProvider);

//# sourceMappingURL=refeicoes.js.map

/***/ }),

/***/ 501:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardapioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_current_cardapios__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_receitas__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_cardapios__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_refeicoes__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_listas__ = __webpack_require__(451);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_utils__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_async_es_waterfall__ = __webpack_require__(454);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/*
    Generated class for the Cardapio page.

    See http://ionicframework.com/docs/v2/components/#navigation for more info on
    Ionic pages and navigation.
*/
var CardapioPage = (function () {
    function CardapioPage(navCtrl, alertCtrl, modalCtrl, navParams, ccp, cp, lp, rp, recP, ap, up, loadingCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.ccp = ccp;
        this.cp = cp;
        this.lp = lp;
        this.rp = rp;
        this.recP = recP;
        this.ap = ap;
        this.up = up;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.autosave = true;
        this.searchToggle = false;
    }
    CardapioPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad Cardapio Page");
        this.init();
    };
    CardapioPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando..."
        });
        this.loading.present();
    };
    CardapioPage.prototype.init = function () {
        var _this = this;
        // Se o usuário for visitante
        var cardapioParams = this.navParams.get("c");
        Object(__WEBPACK_IMPORTED_MODULE_9_async_es_waterfall__["a" /* default */])([
            function (cb) {
                _this.showLoader();
                _this.ap.currentUserRoleCheck("guest")
                    .then(function (res) {
                    _this.loading.dismiss();
                    if (!res) {
                        cb(false);
                        // Se o usuário for visitante
                    }
                    else {
                        // E for um cardápio novo
                        if (!cardapioParams) {
                            var data = { user_id: _this.ap.getCurrentUser()._id };
                            var options = { count: true };
                            // Query pra contar o número de cardápios do usuário
                            _this.showLoader();
                            _this.cp.getAll(data, options).then(function (count) {
                                _this.loading.dismiss();
                                // Se o número de cardápios for maior do que 0, ele deve ser redirecionado para tela de registro, e depois redirecionando para tela de cardápio
                                if (count > 0) {
                                    cb(true);
                                }
                                else {
                                    cb(false);
                                }
                            });
                        }
                        else {
                            cb(false);
                        }
                    }
                })
                    .catch(function (err) { return cb(false); });
            }
        ], function (needRegister) {
            _this.needRegister = needRegister;
            if (needRegister) {
                var registerAlert = _this.alertCtrl.create({
                    enableBackdropDismiss: false,
                    title: "Registrar",
                    message: "Para prosseguir com a criação de outros cardápios, é necessário o registro de perfil.",
                    buttons: [
                        {
                            text: "Voltar",
                            handler: function (data) {
                                _this.navCtrl.setRoot("HomePage");
                            }
                        },
                        {
                            text: "Registrar",
                            handler: function (data) {
                                _this.navCtrl.setRoot("RegisterPage", { nextPage: "CardapioPage" });
                            }
                        }
                    ]
                });
                registerAlert.present();
            }
            else {
                // Se o cardapio já existe, já seta os modelos.
                if (cardapioParams) {
                    var data = { _id: cardapioParams._id };
                    var options = {};
                    _this.cardapioIndex = _this.ccp.setCardapioIndex(0);
                    _this.showLoader();
                    _this.cp.getOne(data, options).then(function (cardapio) {
                        _this.cardapio = _this.ccp.setCardapio(cardapio);
                        _this.modelCardapioDia = _this.ccp.setModelCardapioDia(_this.cardapio.dias[_this.cardapioIndex]);
                        _this.modelCardapioDiaClone = _this.up.clone(_this.modelCardapioDia);
                        _this.refeicoes = _this.rp.getRefeicoes({});
                        _this.refeicaoIndex = _this.ccp.setRefeicaoIndex(0);
                        _this.modelRefeicao = _this.refeicoes[_this.refeicaoIndex];
                        _this.cardapioClone = _this.up.clone(_this.cardapio);
                        _this.loading.dismiss();
                        _this.orderByReceitas("normal");
                    });
                }
                else {
                    _this.cardapioDetalhesModal();
                }
            }
        });
    };
    CardapioPage.prototype.refeicaoPage = function (event) {
        this.refeicaoIndex = this.ccp.setRefeicaoIndex(event.i);
        this.navCtrl.push("RefeicaoPage", {
            refeicao: event.r,
            refeicaoIndex: this.refeicaoIndex,
            refeicoes: this.refeicoes
        });
    };
    CardapioPage.prototype.gerarCardapio = function () {
        var _this = this;
        var refeicoesBranco = [];
        var touched = true;
        // Checar se há refeição em dias que não foram preenchidos
        for (var i = 0; i < this.cardapio.dias.length; ++i) {
            var d = this.cardapio.dias[i];
            refeicoesBranco.push({ dia: d.data, refeicao: [] });
            for (var j = 0; j < d.refeicoes.length; ++j) {
                var r = d.refeicoes[j];
                if (!r.touched) {
                    refeicoesBranco[i].refeicao.push(r.titulo);
                    touched = false;
                }
            }
        }
        // caso sim, cria um alert informando isso
        if (!touched) {
            var alert_1 = this.alertCtrl.create({
                title: "Refeições em branco",
                message: "Você ainda tem refeições em branco. Gerar lista mesmo assim?",
                buttons: [
                    { text: "Não" },
                    {
                        text: "Gerar a lista de compras",
                        handler: function (data) {
                            if (_this.cardapio._id) {
                                _this.updateCardapio();
                            }
                            else {
                                _this.createCardapio();
                            }
                        }
                    }
                ]
            });
            alert_1.present();
            // se não, ele simplsmente atualiza o cardapio e cria a lista
        }
        else {
            if (this.cardapio._id) {
                this.updateCardapio();
            }
            else {
                this.createCardapio();
            }
        }
    };
    CardapioPage.prototype.createCardapio = function () {
        var _this = this;
        try {
            this.showLoader();
            Object(__WEBPACK_IMPORTED_MODULE_9_async_es_waterfall__["a" /* default */])([
                function (cb) {
                    // contador de geraçõs do cardápio
                    _this.cardapio.gerado++;
                    _this.cp.create(_this.cardapio, {}).then(function (cardapio) {
                        cb(undefined, cardapio);
                    });
                },
                function (cardapio, cb) {
                    _this.lp.newLista(cardapio).then(function (newLista) {
                        cb(undefined, newLista);
                    });
                },
                function (lista, cb) {
                    _this.lp.create(lista, {}).then(function (lista) {
                        cb(lista);
                    });
                }
            ], function (lista) {
                _this.autosave = false;
                _this.navCtrl.setPages([
                    { page: "HomePage" },
                    { page: "ListasPage" },
                    { page: "ListaPage", params: { l: lista } }
                ]);
                _this.loading.dismiss();
            });
        }
        catch (err) {
            console.log(err);
            this.loading.dismiss();
        }
    };
    CardapioPage.prototype.updateCardapio = function () {
        var _this = this;
        try {
            this.showLoader();
            Object(__WEBPACK_IMPORTED_MODULE_9_async_es_waterfall__["a" /* default */])([
                function (cb) {
                    // contador de geraçõs do cardápio
                    _this.cardapio.gerado++;
                    _this.cp.update(_this.cardapio, {}).then(function (cardapio) {
                        cb(undefined, _this.cardapio);
                    });
                },
                function (cardapio, cb) {
                    _this.lp.newLista(cardapio).then(function (newLista) {
                        cb(undefined, newLista);
                    });
                },
                function (lista, cb) {
                    _this.lp.create(lista, {}).then(function (lista) {
                        cb(lista);
                    });
                }
            ], function (lista) {
                _this.showToast("Cardápio salvo e lista criada");
                _this.autosave = false;
                _this.navCtrl.setPages([
                    { page: "HomePage" },
                    { page: "ListasPage" },
                    { page: "ListaPage", params: { l: lista } }
                ]);
                _this.loading.dismiss();
            });
        }
        catch (err) {
            console.log(err);
            this.loading.dismiss();
        }
    };
    CardapioPage.prototype.cardapioDetalhesModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create("CardapioDetalhesPage", { cardapio: this.cardapio }, { enableBackdropDismiss: false });
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.cardapio = _this.ccp.newCardapio(data);
                _this.cardapioIndex = _this.ccp.setCardapioIndex(0);
                _this.modelCardapioDia = _this.ccp.setModelCardapioDia(_this.cardapio.dias[_this.cardapioIndex]);
                _this.modelCardapioDiaClone = _this.up.clone(_this.modelCardapioDia);
                _this.refeicoes = _this.rp.getRefeicoes({});
                _this.refeicaoIndex = _this.ccp.setRefeicaoIndex(0);
                _this.modelRefeicao = _this.refeicoes[_this.refeicaoIndex];
                _this.cardapioClone = _this.up.clone(_this.cardapio);
                _this.orderByReceitas("normal");
            }
        });
        modal.present();
    };
    CardapioPage.prototype.copiarDia = function (event) {
        if ((event.c + event.i) >= this.cardapio.dias.length) {
            this.cardapioIndex = this.ccp.setCardapioIndex(0);
        }
        else if ((event.c + event.i) < 0) {
            this.cardapioIndex = this.ccp.setCardapioIndex(this.cardapio.dias.length - 1);
        }
        else {
            this.cardapioIndex = this.ccp.setCardapioIndex(event.c + event.i);
        }
        this.modelCardapioDia = this.ccp.setModelCardapioDia(this.cardapio.dias[this.cardapioIndex]);
    };
    CardapioPage.prototype.modalCopiarDia = function () {
        var modal = this.modalCtrl.create("RefeicaoCopiarPage", {
            cardapio: this.cardapio,
            refeicoes: this.refeicoes,
            cardapioIndex: this.cardapioIndex
        }, {
            enableBackdropDismiss: false
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
        });
        modal.present();
    };
    CardapioPage.prototype.limparCardapio = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Limpar",
            message: "Deseja limpar o cardápio?"
        });
        alert.addInput({
            type: "radio",
            label: "Hoje",
            value: "hoje",
        });
        alert.addInput({
            type: "radio",
            label: "Todos os dias",
            value: "todos_os_dias",
        });
        alert.addButton({
            text: "OK",
            handler: function (data) {
                if (data === "hoje") {
                    _this.cardapio.dias[_this.cardapioIndex].refeicoes = _this.ccp.resetRefeicao(_this.cardapioIndex);
                    _this.modelCardapioDia = _this.ccp.setModelCardapioDia(_this.cardapio.dias[_this.cardapioIndex]);
                    _this.modelCardapioDiaClone = _this.up.clone(_this.modelCardapioDia);
                }
                else {
                    for (var i = _this.cardapio.dias.length - 1; i >= 0; i--) {
                        _this.cardapio.dias[i].refeicoes = _this.ccp.resetRefeicao(i);
                    }
                    _this.cardapioClone = _this.up.clone(_this.cardapio);
                }
            }
        });
        alert.present();
    };
    CardapioPage.prototype.alterarFiltros = function () {
        this.filtro.open();
    };
    CardapioPage.prototype.alterarRefeicao = function (event) {
        if ((event.r.ordem + event.i) >= this.refeicoes.length) {
            this.refeicaoIndex = this.ccp.setRefeicaoIndex(0);
        }
        else if ((event.r.ordem + event.i) < 0) {
            this.refeicaoIndex = this.ccp.setRefeicaoIndex(this.refeicoes.length - 1);
        }
        else {
            this.refeicaoIndex = this.ccp.setRefeicaoIndex(event.r.ordem + event.i);
        }
        this.modelRefeicao = this.refeicoes[this.refeicaoIndex];
        this.orderByReceitas("normal");
    };
    CardapioPage.prototype.orderByReceitas = function (orderBy) {
        this.orderBy = orderBy;
        this.getReceitas({ search: this.search, orderBy: orderBy });
    };
    CardapioPage.prototype.searchReceitas = function (ev) {
        this.search = ev.target.value;
        if (this.search && this.search.trim() !== "") {
            this.getReceitas({ search: this.search, orderBy: this.orderBy });
        }
    };
    // Função para pegar receitas
    CardapioPage.prototype.getReceitas = function (params) {
        var _this = this;
        var data = __assign({ user_id: this.ap.getCurrentUser()._id, tag_refeicao: this.modelRefeicao.tag }, params);
        var orderBy = function (ar, currentField, orderType) {
            return ar.sort(function (a, b) {
                if (orderType === "ASC") {
                    if (a[currentField] < b[currentField])
                        return -1;
                    if (a[currentField] > b[currentField])
                        return 1;
                    return 0;
                }
                else {
                    if (a[currentField] < b[currentField])
                        return 1;
                    if (a[currentField] > b[currentField])
                        return -1;
                    return 0;
                }
            });
        };
        try {
            this.showLoader();
            this.recP.getReceitasFiltered(data, {}).then(function (receitas) {
                _this.receitas = receitas;
                var parentes = [];
                var filhos = [];
                var orderType = "DESC";
                var currentField = "score";
                // Separa quais tags são tipo "parente" e tipo "filho"
                for (var r = void 0, i = _this.receitas.length - 1; i >= 0; i--) {
                    r = _this.receitas[i];
                    if (r.tags.tipo === "parente") {
                        parentes.push({ i: i, _id: r.tags._id });
                    } // Nas tags parente, guarda o índice que a tag está no array de receitas, e o _id para comparar com o parent_id
                    if (r.tags.tipo === "filho") {
                        filhos.push(r);
                    } // Nas tags filho, guarda o parent_id e ele
                    r.receitas = orderBy(r.receitas, currentField, orderType);
                }
                // Construindo a hierarquia de pai e filho
                // Para cada parente
                for (var p = void 0, j = parentes.length - 1; j >= 0; j--) {
                    p = parentes[j];
                    // Seta receitas deste parente como um array vazio
                    _this.receitas[p.i].receitas = [];
                    // Para cada filho
                    for (var f = void 0, k = filhos.length - 1; k >= 0; k--) {
                        f = filhos[k];
                        // Se f for filho de p, adiciona na array de receitas
                        if (f.tags.parent_id === p._id) {
                            _this.receitas[p.i].receitas.push(f);
                            filhos.splice(k, 1);
                        }
                    }
                    _this.receitas[p.i].receitas = orderBy(_this.receitas[p.i].receitas, currentField, orderType);
                    _this.receitas[p.i].receitas.sort(function (a, b) { return a.tags.titulo > b.tags.titulo; });
                }
                _this.receitas = _this.receitas.filter(function (r) {
                    if (r.tags.tipo === "filho") {
                        return false;
                    }
                    if (r.tags.tipo === "refeicao") {
                        return false;
                    }
                    return true;
                });
                _this.loading.dismiss();
            });
        }
        catch (err) {
            console.error(err);
            this.loading.dismiss();
        }
    };
    CardapioPage.prototype.receitaModal = function (r) {
        var _this = this;
        var modal = this.modalCtrl.create("ReceitaPage", {
            receita: r,
        });
        modal.onDidDismiss(function (adicionar) {
            if (adicionar) {
                _this.adicionarReceita(r);
            }
        });
        modal.present();
    };
    CardapioPage.prototype.adicionarReceita = function (r) {
        this.modelCardapioDia = this.ccp.getModelCardapioDia();
        this.modelCardapioDia.refeicoes[this.refeicaoIndex].touched = true;
        this.modelCardapioDia.refeicoes[this.refeicaoIndex].receitas.push(this.up.clone(r));
        this.showToast("Receita adicionada");
    };
    CardapioPage.prototype.toggleSearch = function () {
        this.searchToggle = !this.searchToggle;
    };
    CardapioPage.prototype.ionViewWillUnload = function () {
        var _this = this;
        if (this.autosave) {
            if (this.up.isDifferent(this.cardapio, this.cardapioClone)) {
                var alert_2 = this.alertCtrl.create({
                    title: "Cardápio alterado",
                    message: "Deseja salvar alterações?",
                    buttons: [
                        { text: "Não" },
                        {
                            text: "Sim",
                            handler: function (data) {
                                _this.cp.create(_this.cardapio, {}).then(function (cardapio) {
                                    _this.showToast("Cardápio salvo");
                                });
                            }
                        }
                    ]
                });
                alert_2.present();
            }
        }
    };
    CardapioPage.prototype.showToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: "bottom",
            showCloseButton: true,
            closeButtonText: "X"
        });
        toast.present();
    };
    CardapioPage.prototype.pop = function () {
        if (this.navCtrl.canGoBack()) {
            this.navCtrl.pop();
        }
        else {
            this.rootHome();
        }
    };
    CardapioPage.prototype.rootHome = function () {
        this.navCtrl.setRoot("HomePage");
    };
    return CardapioPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("filtro"),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* Select */])
], CardapioPage.prototype, "filtro", void 0);
CardapioPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-cardapio",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/cardapio/cardapio.html"*/'<!--\n	Generated template for the Cardapio page.\n\n	See http://ionicframework.com/docs/v2/components/#navigation for more info on\n	Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar hideBackButton>\n		<ion-buttons left>\n			<button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n		</ion-buttons>\n		<ion-title *ngIf="cardapio">\n			{{ cardapio.titulo }}\n			<ion-icon class="fa fa-exclamation" color="danger" *ngIf="up.isDifferent(cardapio, cardapioClone)"></ion-icon>\n		</ion-title>\n		<ion-buttons end>\n			<button class="edit" ion-button (click)="cardapioDetalhesModal()"><ion-icon class="fa fa-pencil" color="light"></ion-icon></button>\n			<button class="back" ion-button (click)="pop()"><img src="assets/img/retornar.png"></button>\n		</ion-buttons>\n	</ion-navbar>\n</ion-header>\n\n<ion-content>\n	<ion-searchbar\n		(ionInput)="searchReceitas($event)"\n		placeholder="Busca"\n		autocomplete="on"\n		debounce="500"\n	>\n	</ion-searchbar>\n\n	<ion-grid padding-horizontal>    \n		<ion-row class="seletores">\n			<ion-col>\n				<dia-seletor *ngIf="modelRefeicao && refeicoes"\n					[modelCardapioDia]="ccp.getModelCardapioDia()"\n					[cardapio]="ccp.getCardapio()"\n					[cardapioIndex]="ccp.getCardapioIndex()"\n					(copiarDia)="copiarDia($event)">\n				</dia-seletor>        \n			</ion-col>\n			<ion-col>\n				<refeicao-seletor *ngIf="modelRefeicao && refeicoes"\n					[refeicoes]="refeicoes"\n					[refeicao]="modelRefeicao"\n					[refeicaoIndex]="ccp.getRefeicaoIndex()" \n					(alterarRefeicao)="alterarRefeicao($event)">\n				</refeicao-seletor>       \n			</ion-col>      \n		</ion-row>\n	</ion-grid>\n\n	<receitas-escolhidas *ngIf="cardapio" \n		[refeicaoIndex]="ccp.getRefeicaoIndex()" \n		[modelCardapioDia]="ccp.getModelCardapioDia()" \n		[cardapio]="cardapio">\n	</receitas-escolhidas>\n\n	<ion-item [class.hide]="true">\n		<ion-label class="bold" for="tipo">Ordernar por:</ion-label>\n		<ion-select (ionChange)="orderByReceitas($event)" #filtro>\n			<!-- <ion-option value="filho">Filho</ion-option> -->\n			<ion-option value="normal" checked>Tipo de Alimento</ion-option>\n			<ion-option value="tipo_de_preparo">Tipo de Preparo</ion-option>\n			<ion-option value="nacionalidade">Nacionalidade</ion-option>\n			<ion-option value="tempo_de_preparo">Tempo de Preparo</ion-option>\n			<ion-option value="porcao_sugerida">Porção Sugerida</ion-option>\n		</ion-select>\n	</ion-item>\n	\n	<div class="filter-wrapper" *ngIf="receitas && modelRefeicao">\n		<div class="receitas" *ngFor="let r of receitas" [ngSwitch]="r.tags.tipo">      \n			<!-- Para tags parentes -->\n			<div class="parente" *ngSwitchCase="\'parente\'">\n				<ion-list *ngIf="r.receitas.length" padding border-custom>\n					<ion-list-header>\n						{{r.tags.titulo}}\n					</ion-list-header>\n					<ion-item-group *ngFor="let rr of r.receitas">\n						<ion-item class="filho" (click)="rr.collapse = !rr.collapse" >\n							<ion-label>{{ rr.tags.titulo }} <span>({{rr.receitas.length}})</span></ion-label>\n							<button ion-button item-right clear>\n								<ion-icon class="fa" [class.fa-chevron-right]="!rr.collapse" [class.fa-chevron-down]="rr.collapse" color="secondary"></ion-icon>\n							</button>\n						</ion-item>\n						<div *ngIf="rr.collapse">\n							<ion-item *ngFor="let re of rr.receitas" class="receita">\n								<ion-label (click)="adicionarReceita(re)"><h2 class="titulo">{{re.titulo}}</h2> </ion-label>\n								<button class="receitaFav" (click)="updateFav(re)" item-right ion-button clear>\n									<ion-icon class="fa" [class.fa-star-o]="!re.favorito" [class.fa-star]="re.favorito" color="secondary"></ion-icon>\n								</button>\n								<button class="receitaModal" (click)="receitaModal(re)" item-right ion-button color="secondary" round>\n									<ion-icon class="fa fa-ellipsis-h" color="light"></ion-icon>\n								</button>\n							</ion-item>\n						</div>\n					</ion-item-group>\n				</ion-list>\n			</div>\n			\n			<div class="normal" *ngSwitchDefault>\n				<ion-list *ngIf="r.receitas.length" padding border-custom>\n					<ion-list-header (click)="r.collapse = !r.collapse">\n						<ion-label>\n							{{r.tags.titulo}} <span>({{r.receitas.length}})</span>\n						</ion-label>\n						<button ion-button item-right clear>\n							<ion-icon class="fa" [class.fa-chevron-right]="!r.collapse" [class.fa-chevron-down]="r.collapse" color="secondary"></ion-icon>\n						</button>\n					</ion-list-header>\n					<ion-item-group *ngIf="r.collapse">\n						<ion-item *ngFor="let re of r.receitas" class="receita">\n							<ion-label (press)="receitaModal(re)" (click)="adicionarReceita(re)"><h2 class="titulo">{{re.titulo}}</h2> </ion-label>\n							<button class="receitaFav" (click)="updateFav(re)" item-right ion-button clear>\n								<ion-icon class="fa" [class.fa-star-o]="!re.favorito" [class.fa-star]="re.favorito" color="secondary"></ion-icon>\n							</button>\n							<button class="receitaModal" (click)="receitaModal(re)" item-right ion-button color="secondary" round>\n								<ion-icon class="fa fa-ellipsis-h" color="light"></ion-icon>\n							</button>\n						</ion-item>\n					</ion-item-group>\n				</ion-list>\n			</div>\n		</div>\n		<div padding *ngIf="receitas.length === 0">Nenhuma receita encontrada.</div>\n	</div>\n	\n	\n</ion-content>\n<ion-footer>\n	<ion-grid no-padding>\n		<ion-row no-padding class="actions" no-padding>\n			<ion-col no-padding class="action" *ngIf="cardapio">\n				<button ion-button icon-left id="copiar" (click)="modalCopiarDia()" [attr.disabled]="cardapio.diasCounter <= 1 ? true : null">\n					<ion-icon color="light" class="fa fa-copy"></ion-icon>\n					<ion-label>Copiar<br>refeições</ion-label>\n				</button>\n			</ion-col>\n			<ion-col class="action">\n				<button ion-button icon-left color="danger" id="limpar" (click)="limparCardapio()">\n					<ion-icon color="light" class="fa fa-eraser"></ion-icon>\n					<ion-label>Limpar<br>receitas</ion-label>\n				</button>\n			</ion-col>\n			<ion-col no-padding class="action">\n				<button ion-button icon-left color="verde" id="resumo" (click)="alterarFiltros()">\n					<ion-icon color="light" class="fa fa-filter"></ion-icon>        \n					<ion-label>Alterar<br>Filtros</ion-label>\n				</button>\n			</ion-col>\n		</ion-row>\n		<ion-row no-padding>\n			<ion-col no-padding col-12 class="action">\n				<button ion-button icon-left color="secondary" full (click)="gerarCardapio()">\n					<ion-icon class="fa fa-shopping-cart"></ion-icon>\n					<ion-label>Gerar lista de compras</ion-label>\n				</button>      \n			</ion-col>\n		</ion-row>\n	</ion-grid>\n</ion-footer>'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/cardapio/cardapio.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_4__providers_cardapios__["a" /* CardapiosProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_refeicoes__["a" /* RefeicoesProvider */], __WEBPACK_IMPORTED_MODULE_6__providers_listas__["a" /* ListasProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_receitas__["a" /* ReceitasProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_current_cardapios__["a" /* CurrentCardapiosProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_cardapios__["a" /* CardapiosProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_listas__["a" /* ListasProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_refeicoes__["a" /* RefeicoesProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_receitas__["a" /* ReceitasProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_8__providers_utils__["a" /* UtilsProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */]])
], CardapioPage);

//# sourceMappingURL=cardapio.js.map

/***/ })

});
//# sourceMappingURL=0.main.js.map