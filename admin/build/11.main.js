webpackJsonp([11],{

/***/ 439:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListasModule", function() { return ListasModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__listas__ = __webpack_require__(513);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(321);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ListasModule = (function () {
    function ListasModule() {
    }
    return ListasModule;
}());
ListasModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__listas__["a" /* ListasPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__listas__["a" /* ListasPage */]),
            __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__listas__["a" /* ListasPage */]
        ]
    })
], ListasModule);

//# sourceMappingURL=listas.module.js.map

/***/ }),

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Listas provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var ListasProvider = (function () {
    function ListasProvider(http, ap, ENV) {
        this.http = http;
        this.ap = ap;
        this.ENV = ENV;
        console.log("Hello Listas Provider");
    }
    ListasProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (listas) {
                _this.listas = listas;
                resolve(_this.listas);
            });
        });
    };
    ListasProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (lista) {
                _this.lista = lista;
                resolve(_this.lista);
            });
        });
    };
    ListasProvider.prototype.create = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/create", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (lista) {
                _this.lista = lista;
                resolve(_this.lista);
            });
        });
    };
    ListasProvider.prototype.update = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/update", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ListasProvider.prototype.delete = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/delete", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ListasProvider.prototype.newLista = function (c) {
        var _this = this;
        return new Promise(function (resolve) {
            var titulo = c.titulo;
            console.log("c.gerado", c.gerado);
            console.log((c.gerado - 1) > 0);
            if (c.gerado - 1 > 0) {
                titulo = titulo + " (" + (c.gerado - 1) + ")";
            }
            var data = {
                titulo: titulo,
                receitas: _this.extractReceitas(c),
                user_id: _this.ap.getCurrentUser()._id,
                cardapio_id: c._id
            };
            resolve(data);
        });
    };
    ListasProvider.prototype.extractReceitas = function (c) {
        var allReceitas = [];
        for (var _i = 0, _a = c.dias; _i < _a.length; _i++) {
            var d = _a[_i];
            for (var _b = 0, _c = d.refeicoes; _b < _c.length; _b++) {
                var r = _c[_b];
                for (var _d = 0, _e = r.receitas; _d < _e.length; _d++) {
                    var re = _e[_d];
                    allReceitas.push({ _id: re._id, porcoes: re.porcoes !== undefined ? re.porcoes : c.porcoes });
                }
            }
        }
        return allReceitas;
    };
    return ListasProvider;
}());
ListasProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], ListasProvider);

//# sourceMappingURL=listas.js.map

/***/ }),

/***/ 513:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_listas__ = __webpack_require__(451);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
    Generated class for the Listas page.

    See http://ionicframework.com/docs/v2/components/#navigation for more info on
    Ionic pages and navigation.
*/
var ListasPage = (function () {
    function ListasPage(navCtrl, alertCtrl, lp, ap, modalCtrl, loadingCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.lp = lp;
        this.ap = ap;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.guest = false;
        this._ = __WEBPACK_IMPORTED_MODULE_4_lodash___default.a;
        this.showLoader();
    }
    ListasPage.prototype.ionViewDidLoad = function () {
        console.log("Hello Listas Page");
        this.init();
    };
    ListasPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando..."
        });
        this.loading.present();
    };
    ListasPage.prototype.init = function () {
        var _this = this;
        try {
            var data = { user_id: this.ap.getCurrentUser()._id };
            var options = { select: "titulo createdAt cardapio_id", populate: { field: "cardapio_id", select: "titulo" } };
            this.lp.getAll(data, options).then(function (listas) {
                _this.listas = listas;
                _this.loading.dismiss();
            });
        }
        catch (err) {
            console.log(err);
            this.loading.dismiss();
        }
    };
    ListasPage.prototype.pushListaPage = function (l) {
        var _this = this;
        var updateListaCB = function (_params) {
            return new Promise(function (resolve, reject) {
                if (_params.push) {
                    _this.listas.push(_params.push);
                }
                else if (_params.update) {
                    var find = _this.listas.find(function (l) { return l._id === _params.update._id; });
                    find.titulo = _params.update.titulo;
                }
                resolve();
            });
        };
        this.navCtrl.push("ListaPage", { l: l, updateListaCB: updateListaCB });
    };
    ListasPage.prototype.adicionarLista = function () {
        var novaLista = {
            itens: [],
            titulo: "Lista nova",
            user_id: this.ap.getCurrentUser()._id
        };
        this.pushListaPage(novaLista);
    };
    ListasPage.prototype.removeLista = function (l) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Remover lista?",
            message: "Remover <strong>" + l.titulo + "</strong>?",
            buttons: [
                { text: "Não" },
                {
                    text: "Sim",
                    handler: function (data) {
                        _this.lp.delete({ user_id: _this.ap.getCurrentUser()._id, _id: l._id }).then(function (resolve) {
                            _this.listas = _this.listas.filter(function (i) { return i !== l; });
                            _this.showToast("Lista removida");
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    ListasPage.prototype.showToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: "bottom",
            showCloseButton: true,
            closeButtonText: "X"
        });
        toast.present();
    };
    ListasPage.prototype.editListaDetalhesModal = function (l) {
        var modal = this.modalCtrl.create("ListaDetalhesPage", { lista: l });
        modal.onDidDismiss(function (data) { });
        modal.present();
    };
    ListasPage.prototype.rootHome = function () {
        this.navCtrl.setRoot("HomePage");
    };
    ListasPage.prototype.registerPage = function () {
        this.navCtrl.setRoot("RegisterPage", { nextPage: "CardapiosPage" });
    };
    ListasPage.prototype.isGuest = function () {
        var _this = this;
        this.ap.currentUserRoleCheck("guest")
            .then(function (res) {
            _this.guest = res;
        })
            .catch(function (err) {
            _this.guest = false;
        });
    };
    return ListasPage;
}());
ListasPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-listas",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/listas/listas.html"*/'<!--\n	Generated template for the Listas page.\n\n	See http://ionicframework.com/docs/v2/components/#navigation for more info on\n	Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar hideBackButton>\n		<ion-buttons left>\n			<button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n		</ion-buttons>\n		<ion-title>Listas</ion-title>\n		<ion-buttons end>\n				<button class="back" ion-button (click)="rootHome()"><img src="assets/img/retornar.png"></button>\n			</ion-buttons>\n		</ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n	<ion-list no-padding border-custom *ngIf="listas">\n		<ion-list-header>\n			Listas ({{ listas.length }})\n			<button class="add" item-right (click)="adicionarLista()" ion-button clear *ngIf="false">\n				Nova lista\n				<ion-icon class="fa fa-plus-circle" color="primary"></ion-icon>\n			</button>\n		</ion-list-header>\n		<ion-item *ngFor="let l of listas">\n			<ion-label (click)="pushListaPage(l)">\n				<h2>{{l.titulo}}</h2>\n			</ion-label>\n			<button *ngIf="false" class="edit" item-right ion-button (click)="editListaDetalhesModal(l)" clear>\n				<ion-icon class="fa fa-pencil" color="primary"></ion-icon>\n			</button>\n			<button class="remove" item-right ion-button (click)="removeLista(l)" clear>\n				<ion-icon class="fa fa-times" color="secondary"></ion-icon>\n			</button>\n		</ion-item>\n		<ion-item *ngIf="!listas.length">\n			Nenhuma lista criada!\n		</ion-item>\n		<ion-item *ngIf="guest && listas.length == 1" (click)="registerPage()" tappable>\n			<ion-label>\n				Visitantes só podem ter uma lista.\n				<p>Cadastre-se para ter quantas listas quiser!</p>\n			</ion-label>\n		</ion-item>\n	</ion-list>\n	<ion-fab right bottom>\n		<button ion-fab mini color="primary" (click)="adicionarLista()"><ion-icon class="fa fa-plus"></ion-icon></button>\n	</ion-fab>\n</ion-content>\n'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/listas/listas.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_listas__["a" /* ListasProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_listas__["a" /* ListasProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */]])
], ListasPage);

//# sourceMappingURL=listas.js.map

/***/ })

});
//# sourceMappingURL=11.main.js.map