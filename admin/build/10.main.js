webpackJsonp([10],{

/***/ 441:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilModule", function() { return PerfilModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__perfil__ = __webpack_require__(515);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PerfilModule = (function () {
    function PerfilModule() {
    }
    return PerfilModule;
}());
PerfilModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */]),
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */]
        ]
    })
], PerfilModule);

//# sourceMappingURL=perfil.module.js.map

/***/ }),

/***/ 515:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__validators_password__ = __webpack_require__(516);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
    Generated class for the Perfil page.

    See http://ionicframework.com/docs/v2/components/#navigation for more info on
    Ionic pages and navigation.
*/
var PerfilPage = (function () {
    function PerfilPage(navCtrl, loadingCtrl, ap, formBuilder) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.ap = ap;
        this.formBuilder = formBuilder;
        this.showLoader();
    }
    PerfilPage.prototype.ionViewDidLoad = function () {
        console.log("Hello Perfil Page");
        this.init();
    };
    PerfilPage.prototype.init = function () {
        try {
            this.submitAttempt = false;
            this.resetPasswordForm = this.formBuilder.group({
                password: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
                newPassword: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
                repeatNewPassword: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            }, {
                validator: Object(__WEBPACK_IMPORTED_MODULE_4__validators_password__["a" /* matchingPasswords */])("newPassword", "repeatNewPassword")
            });
            this.user = this.ap.getCurrentUser();
            this.loading.dismiss();
        }
        catch (err) {
            console.log(err);
            this.loading.dismiss();
        }
    };
    PerfilPage.prototype.resetPassword = function (value) {
        var _this = this;
        this.submitAttempt = true;
        this.showLoader();
        this.ap.resetPassword(value).then(function (result) {
            _this.loading.dismiss();
            _this.resetPasswordMessage = result;
        });
    };
    PerfilPage.prototype.rootHome = function () {
        this.navCtrl.setRoot("HomePage");
    };
    PerfilPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando..."
        });
        this.loading.present();
    };
    return PerfilPage;
}());
PerfilPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-perfil",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/perfil/perfil.html"*/'<!--\n	Generated template for the Filtros page.\n\n	See http://ionicframework.com/docs/v2/components/#navigation for more info on\n	Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar hideBackButton>\n		<ion-buttons left>\n				<button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n			</ion-buttons>\n			<ion-title>Perfil</ion-title>    \n			<ion-buttons end>\n				<button class="back" ion-button (click)="rootHome()"><img src="assets/img/retornar.png"></button>\n			</ion-buttons>\n		</ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n	<ion-list border-custom no-padding *ngIf="user">\n		<ion-list-header>\n			Dados\n		</ion-list-header>\n		<ion-item>\n			<ion-label stacked>Nome</ion-label>\n			<ion-input name="nome" value="{{ user.name }}" disabled></ion-input>\n		</ion-item>\n		<ion-item>\n			<ion-label stacked>E-mail</ion-label>\n			<ion-input name="email" disabled value="{{ user.email }}"></ion-input>\n		</ion-item>\n	</ion-list>\n	<form *ngIf="resetPasswordForm" [formGroup]="resetPasswordForm" (ngSubmit)="resetPassword(resetPasswordForm.value)">\n		<ion-list  no-padding>\n			<ion-list-header>\n				Alterar senha\n			</ion-list-header>\n			<ion-item>\n				<ion-label stacked>Senha atual</ion-label>\n				<ion-input formControlName="password" type="password"></ion-input>\n			</ion-item>\n			<ion-item>\n				<ion-label stacked>Nova senha</ion-label>\n				<ion-input formControlName="newPassword" type="password"></ion-input>\n			</ion-item>\n			<ion-item>\n				<ion-label stacked>Repetir senha</ion-label>\n				<ion-input formControlName="repeatNewPassword" type="password"></ion-input>\n			</ion-item>\n		</ion-list>\n		<button ion-button color="secondary" type="submit" [disabled]="!resetPasswordForm.valid">Salvar</button>\n	</form>	\n</ion-content>\n'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/perfil/perfil.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"]])
], PerfilPage);

//# sourceMappingURL=perfil.js.map

/***/ }),

/***/ 516:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = matchingPasswords;
// export class PasswordValidator {
function matchingPasswords(passwordKey, confirmPasswordKey) {
    return function (group) {
        var password = group.controls[passwordKey];
        var confirmPassword = group.controls[confirmPasswordKey];
        if (password.value !== confirmPassword.value) {
            return {
                mismatchedPasswords: true
            };
        }
    };
}
//# sourceMappingURL=password.js.map

/***/ })

});
//# sourceMappingURL=10.main.js.map