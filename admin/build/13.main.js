webpackJsonp([13],{

/***/ 432:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardapiosModule", function() { return CardapiosModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cardapios__ = __webpack_require__(504);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(321);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CardapiosModule = (function () {
    function CardapiosModule() {
    }
    return CardapiosModule;
}());
CardapiosModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__cardapios__["a" /* CardapiosPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cardapios__["a" /* CardapiosPage */]),
            __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__cardapios__["a" /* CardapiosPage */]
        ]
    })
], CardapiosModule);

//# sourceMappingURL=cardapios.module.js.map

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardapiosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Cardapios provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var CardapiosProvider = (function () {
    function CardapiosProvider(http, ap, ENV) {
        this.http = http;
        this.ap = ap;
        this.ENV = ENV;
        console.log("Hello Cardapios Provider");
    }
    CardapiosProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.create = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/create", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.update = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/update", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.delete = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/delete", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    return CardapiosProvider;
}());
CardapiosProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], CardapiosProvider);

//# sourceMappingURL=cardapios.js.map

/***/ }),

/***/ 504:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardapiosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cardapios__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_current_cardapios__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
    Generated class for the Cardapios page.

    See http://ionicframework.com/docs/v2/components/#navigation for more info on
    Ionic pages and navigation.
*/
var CardapiosPage = (function () {
    function CardapiosPage(navCtrl, modalCtrl, alertCtrl, cp, ccp, ap, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.cp = cp;
        this.ccp = ccp;
        this.ap = ap;
        this.loadingCtrl = loadingCtrl;
        this.guest = false;
        this._ = __WEBPACK_IMPORTED_MODULE_5_lodash___default.a;
        this.showLoader();
        this.user = this.ap.getCurrentUser();
    }
    CardapiosPage.prototype.ionViewDidLoad = function () {
        this.init();
        console.log("Hello Cardapios Page");
    };
    CardapiosPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando..."
        });
        this.loading.present();
    };
    CardapiosPage.prototype.init = function () {
        var _this = this;
        var data = { user_id: this.user._id };
        var options = { select: "titulo dias.data de ate porcoes diasCounter" };
        this.cp.getAll(data, options).then(function (cardapios) {
            _this.cardapios = _this.ccp.setCardapio(cardapios);
            _this.loading.dismiss();
        });
    };
    CardapiosPage.prototype.pushCardapioPage = function (c) {
        this.navCtrl.push("CardapioPage", { c: c });
    };
    CardapiosPage.prototype.rootHome = function () {
        this.navCtrl.setRoot("HomePage");
    };
    CardapiosPage.prototype.editCardapioDetalhesModal = function (c) {
        var _this = this;
        var modal = this.modalCtrl.create("CardapioDetalhesPage", { cardapio: c });
        modal.onDidDismiss(function (data) {
            if (data === undefined) {
                _this.rootHome();
            }
        });
        modal.present();
    };
    CardapiosPage.prototype.removeCardapio = function (c) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Remover cardápio?",
            message: "Remover <strong>" + c.titulo + "</strong>?",
            buttons: [
                { text: "Não" },
                {
                    text: "Sim",
                    handler: function (data) {
                        _this.cp.delete({ user_id: _this.user._id, _id: c._id }).then(function (resolve) {
                            _this.cardapios = _this.cardapios.filter(function (i) { return i !== c; });
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    CardapiosPage.prototype.adicionarCardapio = function () {
        this.navCtrl.push("CardapioPage");
    };
    CardapiosPage.prototype.isGuest = function () {
        var _this = this;
        this.ap.currentUserRoleCheck("guest")
            .then(function (res) {
            _this.guest = res;
        })
            .catch(function (err) {
            _this.guest = false;
        });
    };
    CardapiosPage.prototype.registerPage = function () {
        this.navCtrl.setRoot("RegisterPage", { nextPage: "CardapiosPage" });
    };
    return CardapiosPage;
}());
CardapiosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-cardapios",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/cardapios/cardapios.html"*/'<!--\n	Generated template for the Cardapios page.\n\n	See http://ionicframework.com/docs/v2/components/#navigation for more info on\n	Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar hideBackButton>\n		<ion-buttons left>\n			<button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n		</ion-buttons>\n		<ion-title>Meus Cardapios</ion-title>    \n		<ion-buttons end>\n			<button class="back" ion-button (click)="rootHome()"><img src="assets/img/retornar.png"></button>\n		</ion-buttons>\n	</ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<ion-list border-custom no-padding *ngIf="cardapios">\n		<ion-list-header>\n			Cardapios ({{ cardapios.length }})\n			<button class="add" item-right (click)="adicionarCardapio()" ion-button clear *ngIf="false">\n				Novo cardápio\n				<ion-icon class="fa fa-plus-circle" color="primary"></ion-icon>\n			</button>			\n		</ion-list-header>\n		<ion-item *ngFor="let c of cardapios">\n			<ion-label (click)="pushCardapioPage(c)">\n				<h2>{{c.titulo}}</h2>\n			</ion-label>\n			<button *ngIf="false" class="edit" item-right ion-button (click)="editCardapioDetalhesModal(c)" clear>\n				<ion-icon class="fa fa-pencil" color="primary"></ion-icon>\n			</button>\n			<button class="remove" item-right ion-button (click)="removeCardapio(c)" clear>\n				<ion-icon class="fa fa-times" color="secondary"></ion-icon>\n			</button>\n		</ion-item>\n		<ion-item *ngIf="!cardapios.length">\n			Nenhum cardapio criado!\n		</ion-item>\n		<ion-item *ngIf="guest && cardapios.length == 1" (click)="registerPage()" tappable>\n			<ion-label>\n			Visitantes só podem ter um cardápio.\n			<p>Cadastre-se para ter quantos cardápios quiser!</p>\n		</ion-label>\n		</ion-item>\n	</ion-list>\n\n	<ion-fab right bottom>\n		<button ion-fab mini color="primary" (click)="adicionarCardapio()"><ion-icon class="fa fa-plus"></ion-icon></button>\n	</ion-fab>\n</ion-content>'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/cardapios/cardapios.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_cardapios__["a" /* CardapiosProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_cardapios__["a" /* CardapiosProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_current_cardapios__["a" /* CurrentCardapiosProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */]])
], CardapiosPage);

//# sourceMappingURL=cardapios.js.map

/***/ })

});
//# sourceMappingURL=13.main.js.map