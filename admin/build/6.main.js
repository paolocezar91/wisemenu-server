webpackJsonp([6],{

/***/ 443:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RefeicaoModule", function() { return RefeicaoModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__refeicao__ = __webpack_require__(522);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(320);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var RefeicaoModule = (function () {
    function RefeicaoModule() {
    }
    return RefeicaoModule;
}());
RefeicaoModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__refeicao__["a" /* RefeicaoPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__refeicao__["a" /* RefeicaoPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__refeicao__["a" /* RefeicaoPage */]
        ]
    })
], RefeicaoModule);

//# sourceMappingURL=refeicao.module.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReceitasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var ReceitasProvider = (function () {
    function ReceitasProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
    }
    ReceitasProvider.prototype.getReceitas = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.getReceitasFiltered = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/filtered", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    // Receita única
    ReceitasProvider.prototype.getReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.avaliarReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/avaliar", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.createOrUpdateReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/createOrUpdate", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.removeReceita = function (data, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/remove", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.upload = function (params, files, path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("attr", params.attr);
            for (var i = 0; i < files.length; i++) {
                formData.append("upload", files[i], files[i].name);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.response);
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", _this.ENV.API_URL + path, true);
            xhr.setRequestHeader("Authorization", _this.authProvider.getToken());
            xhr.send(formData);
        });
    };
    ReceitasProvider.prototype.uploadXls = function (params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.upload(params, files, "receitas/uploadXls")
                .then(function (result) {
                resolve(result);
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ReceitasProvider.prototype.uploadImg = function (params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.upload(params, files, "receitas/uploadImage");
        });
    };
    return ReceitasProvider;
}());
ReceitasProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], ReceitasProvider);

//# sourceMappingURL=receitas.js.map

/***/ }),

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Listas provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var ListasProvider = (function () {
    function ListasProvider(http, ap, ENV) {
        this.http = http;
        this.ap = ap;
        this.ENV = ENV;
        console.log("Hello Listas Provider");
    }
    ListasProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (listas) {
                _this.listas = listas;
                resolve(_this.listas);
            });
        });
    };
    ListasProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (lista) {
                _this.lista = lista;
                resolve(_this.lista);
            });
        });
    };
    ListasProvider.prototype.create = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/create", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (lista) {
                _this.lista = lista;
                resolve(_this.lista);
            });
        });
    };
    ListasProvider.prototype.update = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/update", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ListasProvider.prototype.delete = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "listas/delete", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ListasProvider.prototype.newLista = function (c) {
        var _this = this;
        return new Promise(function (resolve) {
            var titulo = c.titulo;
            console.log("c.gerado", c.gerado);
            console.log((c.gerado - 1) > 0);
            if (c.gerado - 1 > 0) {
                titulo = titulo + " (" + (c.gerado - 1) + ")";
            }
            var data = {
                titulo: titulo,
                receitas: _this.extractReceitas(c),
                user_id: _this.ap.getCurrentUser()._id,
                cardapio_id: c._id
            };
            resolve(data);
        });
    };
    ListasProvider.prototype.extractReceitas = function (c) {
        var allReceitas = [];
        for (var _i = 0, _a = c.dias; _i < _a.length; _i++) {
            var d = _a[_i];
            for (var _b = 0, _c = d.refeicoes; _b < _c.length; _b++) {
                var r = _c[_b];
                for (var _d = 0, _e = r.receitas; _d < _e.length; _d++) {
                    var re = _e[_d];
                    allReceitas.push({ _id: re._id, porcoes: re.porcoes !== undefined ? re.porcoes : c.porcoes });
                }
            }
        }
        return allReceitas;
    };
    return ListasProvider;
}());
ListasProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], ListasProvider);

//# sourceMappingURL=listas.js.map

/***/ }),

/***/ 452:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
  Generated class for the Favoritos provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var FavoritosProvider = (function () {
    function FavoritosProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
        console.log('Hello Filtros Provider');
    }
    FavoritosProvider.prototype.newFavorito = function (receita_id) {
        return {
            "user_id": this.authProvider.getCurrentUser()._id,
            "receita_id": receita_id
        };
    };
    FavoritosProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Authorization', _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + 'favoritos/one', { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (favorito) {
                _this.favorito = favorito;
                resolve(favorito);
            });
        });
    };
    FavoritosProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Authorization', _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + 'favoritos', { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (favoritos) {
                _this.favoritos = favoritos;
                resolve(favoritos);
            });
        });
    };
    FavoritosProvider.prototype.update = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Authorization', _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + 'favoritos/update', { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (favorito) {
                resolve(favorito);
            });
        });
    };
    return FavoritosProvider;
}());
FavoritosProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], FavoritosProvider);

//# sourceMappingURL=favoritos.js.map

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardapiosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Cardapios provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var CardapiosProvider = (function () {
    function CardapiosProvider(http, ap, ENV) {
        this.http = http;
        this.ap = ap;
        this.ENV = ENV;
        console.log("Hello Cardapios Provider");
    }
    CardapiosProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.create = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/create", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.update = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/update", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    CardapiosProvider.prototype.delete = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "cardapios/delete", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    return CardapiosProvider;
}());
CardapiosProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], CardapiosProvider);

//# sourceMappingURL=cardapios.js.map

/***/ }),

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RefeicoesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Refeicoes provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var RefeicoesProvider = (function () {
    function RefeicoesProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
        console.log("Hello Refeicoes Provider");
    }
    // options serão queries para o banco de dados
    RefeicoesProvider.prototype.getRefeicoes = function (options) {
        return [
            {
                ordem: 0,
                icone: "cafe.png",
                titulo: "Café-da-manhã",
                tag: "58f9076457a4bd2cfca9e85e"
            },
            {
                ordem: 1,
                icone: "lanchemanha.png",
                titulo: "Lanche da manhã",
                tag: "58f9076457a4bd2cfca9e862"
            },
            {
                ordem: 2,
                icone: "almoco.png",
                titulo: "Almoço",
                tag: "58f9076457a4bd2cfca9e85f"
            },
            {
                ordem: 3,
                icone: "lanchetarde.png",
                titulo: "Lanche da tarde",
                tag: "58f9076457a4bd2cfca9e862"
            },
            {
                ordem: 4,
                icone: "jantar.png",
                titulo: "Jantar",
                tag: "58f9076457a4bd2cfca9e860"
            },
            {
                ordem: 5,
                icone: "ceia.png",
                titulo: "Ceia",
                tag: "58f9076457a4bd2cfca9e861"
            },
        ];
    };
    // Refeicao única
    RefeicoesProvider.prototype.getRefeicao = function (receita_id) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.token);
            _this.http.get(_this.ENV.API_URL + "refeicoes/" + receita_id, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    return RefeicoesProvider;
}());
RefeicoesProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__auth__["a" /* AuthProvider */], Object])
], RefeicoesProvider);

//# sourceMappingURL=refeicoes.js.map

/***/ }),

/***/ 522:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RefeicaoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_receitas__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_favoritos__ = __webpack_require__(452);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_listas__ = __webpack_require__(451);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_cardapios__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_refeicoes__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_current_cardapios__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_lodash__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/*
    Generated class for the Refeicao page.

    See http://ionicframework.com/docs/v2/components/#navigation for more info on
    Ionic pages and navigation.
*/
var RefeicaoPage = (function () {
    function RefeicaoPage(nav, navParams, alertCtrl, modalCtrl, ccp, recP, rp, lp, ap, favsp, loadingCtrl, toastCtrl) {
        this.nav = nav;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.ccp = ccp;
        this.recP = recP;
        this.rp = rp;
        this.lp = lp;
        this.ap = ap;
        this.favsp = favsp;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.autosave = true;
        this.showLoader();
    }
    RefeicaoPage.prototype.ionViewDidLoad = function () {
        console.log("Hello Refeicao Page");
        this.init();
    };
    RefeicaoPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando..."
        });
        this.loading.present();
    };
    RefeicaoPage.prototype.init = function () {
        try {
            var cardapioParams = this.navParams.get("c");
            this.cardapio = this.ccp.getCardapio();
            this.cardapioIndex = this.ccp.getCardapioIndex();
            this.modelCardapioDia = this.ccp.getModelCardapioDia();
            this.modelCardapioDiaClone = this.clone(this.modelCardapioDia);
            this.refeicao = this.navParams.get("refeicao");
            this.refeicoes = this.navParams.get("refeicoes");
            this.refeicaoIndex = this.ccp.getRefeicaoIndex();
            this.getReceitas();
        }
        catch (err) {
            console.log(err);
            this.loading.dismiss();
        }
    };
    RefeicaoPage.prototype.adicionarReceita = function (r) {
        this.modelCardapioDia = this.ccp.getModelCardapioDia();
        this.modelCardapioDia.refeicoes[this.refeicaoIndex].touched = true;
        this.modelCardapioDia.refeicoes[this.refeicaoIndex].receitas.push(this.clone(r));
        this.showToast("Receita adicionada");
    };
    RefeicaoPage.prototype.getReceitas = function (ev) {
        var _this = this;
        var data = { user_id: this.ap.getCurrentUser()._id, tag_refeicao: this.refeicao.tag };
        if (ev) {
            var val = ev.target.value;
            if (val && val.trim() !== "") {
                data.search = val;
            }
        }
        try {
            this.recP.getReceitasFiltered(data, {}).then(function (receitas) {
                _this.receitas = receitas;
                var parentes = [];
                var filhos = [];
                var orderType = "DESC";
                var currentField = "score";
                // Separa quais tags são tipo "parente" e tipo "filho"
                for (var r = void 0, i = _this.receitas.length - 1; i >= 0; i--) {
                    r = _this.receitas[i];
                    if (r.tags.tipo === "parente") {
                        parentes.push({ i: i, _id: r.tags._id });
                    } // Nas tags parente, guarda o índice que a tag está no array de receitas, e o _id para comparar com o parent_id
                    if (r.tags.tipo === "filho") {
                        filhos.push(r);
                    } // Nas tags filho, guarda o parent_id e ele
                    r.receitas = _this.orderBy(r.receitas, currentField, orderType);
                }
                // Construindo a hierarquia de pai e filho
                // Para cada parente
                for (var p = void 0, j = parentes.length - 1; j >= 0; j--) {
                    p = parentes[j];
                    // Seta receitas deste parente como um array vazio
                    _this.receitas[p.i].receitas = [];
                    // Para cada filho
                    for (var f = void 0, k = filhos.length - 1; k >= 0; k--) {
                        f = filhos[k];
                        // Se f for filho de p, adiciona na array de receitas
                        if (f.tags.parent_id === p._id) {
                            _this.receitas[p.i].receitas.push(f);
                            filhos.splice(k, 1);
                        }
                    }
                    _this.receitas[p.i].receitas = _this.orderBy(_this.receitas[p.i].receitas, currentField, orderType);
                    _this.receitas[p.i].receitas.sort(function (a, b) { return a.tags.titulo > b.tags.titulo; });
                }
                _this.receitas = _this.receitas.filter(function (r) {
                    if (r.tags.tipo === "filho") {
                        return false;
                    }
                    if (r.tags.tipo === "refeicao") {
                        return false;
                    }
                    return true;
                });
                // var orderType = "DESC"
                // var currentField = "receitas"
                // this.receitas.sort(function(a,b) {
                // 	if (a[currentField].length < b[currentField].length) return 1
                // 	if (a[currentField].length > b[currentField].length) return -1
                // 	return 0
                // })
                _this.loading.dismiss();
            });
        }
        catch (err) {
            console.error(err);
            this.loading.dismiss();
        }
    };
    RefeicaoPage.prototype.orderBy = function (ar, currentField, orderType) {
        return ar.sort(function (a, b) {
            if (orderType === "ASC") {
                if (a[currentField] < b[currentField])
                    return -1;
                if (a[currentField] > b[currentField])
                    return 1;
                return 0;
            }
            else {
                if (a[currentField] < b[currentField])
                    return 1;
                if (a[currentField] > b[currentField])
                    return -1;
                return 0;
            }
        });
    };
    RefeicaoPage.prototype.showToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: "bottom",
            showCloseButton: true,
            closeButtonText: "X"
        });
        toast.present();
    };
    RefeicaoPage.prototype.popView = function () {
        this.nav.pop();
    };
    RefeicaoPage.prototype.rootHome = function () {
        this.nav.setRoot("HomePage");
    };
    RefeicaoPage.prototype.receitaModal = function (r) {
        var _this = this;
        var modal = this.modalCtrl.create("ReceitaPage", {
            receita: r,
        });
        modal.onDidDismiss(function (adicionar) {
            if (adicionar) {
                _this.adicionarReceita(r);
            }
        });
        modal.present();
    };
    RefeicaoPage.prototype.updateFav = function (re) {
        var _this = this;
        this.showLoader();
        var _favorito = {
            user_id: this.ap.getCurrentUser()._id,
            receita_id: re._id,
            active: !re.favorito
        };
        re.favorito = !re.favorito;
        this.favsp.update(_favorito, {}).then(function (favorito) {
            var favoritos = _this.receitas.find(function (r) {
                return r.tags.tipo === "favoritos";
            });
            if (re.favorito) {
                favoritos.receitas.push(re);
                _this.showToast("Receita adicionada aos favoritos");
            }
            else {
                favoritos.receitas = favoritos.receitas.filter(function (r) {
                    return r._id !== re._id;
                });
                _this.showToast("Receita removida dos favoritos");
            }
            _this.loading.dismiss();
        });
    };
    RefeicaoPage.prototype.alterarRefeicao = function (event) {
        if ((event.r.ordem + event.i) >= this.refeicoes.length) {
            this.refeicaoIndex = this.ccp.setRefeicaoIndex(0);
        }
        else if ((event.r.ordem + event.i) < 0) {
            this.refeicaoIndex = this.ccp.setRefeicaoIndex(this.refeicoes.length - 1);
        }
        else {
            this.refeicaoIndex = this.ccp.setRefeicaoIndex(event.r.ordem + event.i);
        }
        this.refeicao = this.refeicoes[this.refeicaoIndex];
        this.getReceitas();
    };
    RefeicaoPage.prototype.copiarDia = function (dia) {
        if (this.cardapio.dias.length !== 1) {
            if ((this.ccp.getCardapioIndex() + dia) >= this.cardapio.dias.length) {
                this.cardapioIndex = this.ccp.setCardapioIndex(0);
            }
            else if ((this.ccp.getCardapioIndex() + dia) < 0) {
                this.cardapioIndex = this.ccp.setCardapioIndex(this.cardapio.dias.length - 1);
            }
            else {
                this.cardapioIndex = this.ccp.setCardapioIndex(this.ccp.getCardapioIndex() + dia);
            }
            this.modelCardapioDia = this.ccp.setModelCardapioDia(this.cardapio.dias[this.cardapioIndex]);
            // Resetar collapse de tags de receitas
            this.resetCollapse();
        }
    };
    RefeicaoPage.prototype.resetCollapse = function () {
        for (var r = void 0, i = 0; i < this.receitas.length; ++i) {
            r = this.receitas[i];
            if (r.collapse) {
                r.collapse = !r.collapse;
            }
            if (r.receitas) {
                for (var rr = void 0, j = 0; j < r.receitas.length; ++j) {
                    rr = r.receitas[j];
                    if (rr.collapse) {
                        rr.collapse = !rr.collapse;
                    }
                }
            }
        }
    };
    RefeicaoPage.prototype.scrollTo = function (scrollHeight) {
        var _this = this;
        setTimeout(function () {
            _this.content.scrollTo(0, scrollHeight);
        }, 500);
    };
    RefeicaoPage.prototype.clone = function (original) {
        return JSON.parse(JSON.stringify(original));
    };
    RefeicaoPage.prototype.isDifferent = function () {
        return !__WEBPACK_IMPORTED_MODULE_9_lodash___default.a.isEqual(this.modelCardapioDia, this.modelCardapioDiaClone);
    };
    return RefeicaoPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Content */])
], RefeicaoPage.prototype, "content", void 0);
RefeicaoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-refeicao",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/refeicao/refeicao.html"*/'<!--\n	Generated template for the Refeicao page.\n\n	See http://ionicframework.com/docs/v2/components/#navigation for more info on\n	Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar hideBackButton>\n		<ion-buttons left>\n			<button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n		</ion-buttons>\n		<ion-title *ngIf="cardapio">\n			{{ cardapio.titulo }}\n			<ion-icon class="fa fa-exclamation" color="danger" *ngIf="isDifferent()"></ion-icon>\n		</ion-title>\n		<ion-buttons end>\n			<button class="back" ion-button navPop><img src="assets/img/retornar.png"></button>\n		</ion-buttons>\n	</ion-navbar>\n</ion-header>\n\n<ion-content class="refeicoes">\n	\n	<ion-grid padding>\n		<ion-row class="actions" *ngIf="false">\n			<ion-col col-4 class="action">\n				<button ion-button clear id="copiar">\n					<ion-label>Copiar<br>refeições</ion-label>\n					<ion-icon color="secondary" class="fa fa-copy"></ion-icon>\n				</button>\n			</ion-col>\n			<ion-col col-4 class="action">\n				<button ion-button clear id="limpar">\n					<ion-label>Limpar<br>receitas</ion-label>\n					<ion-icon color="secondary" class="fa fa-eraser"></ion-icon>\n				</button>\n			</ion-col>\n			<ion-col col-4 class="action">\n				<button ion-button clear id="resumo">\n					<ion-label>Resumo</ion-label>\n					<ion-icon class="fa fa-bars"></ion-icon>				\n				</button>\n			</ion-col>\n		</ion-row>\n		<ion-row class="seletores">\n			<ion-col>\n				<dia-seletor\n					[modelCardapioDia]="ccp.getModelCardapioDia()" \n					[cardapioIndex]="ccp.getCardapioIndex()" \n					(copiarDia)="copiarDia($event)">\n				</dia-seletor>				\n			</ion-col>\n			<ion-col>\n				<refeicao-seletor *ngIf="refeicao && refeicoes"\n					[refeicoes]="refeicoes"\n					[refeicao]="refeicao" \n					[refeicaoIndex]="ccp.getRefeicaoIndex()" \n					(alterarRefeicao)="alterarRefeicao($event)">\n				</refeicao-seletor>				\n			</ion-col>			\n		</ion-row>\n	</ion-grid>\n\n	<receitas-escolhidas *ngIf="cardapio" \n		[refeicaoIndex]="ccp.getRefeicaoIndex()" \n		[modelCardapioDia]="ccp.getModelCardapioDia()" \n		[cardapio]="cardapio">\n	</receitas-escolhidas>\n	\n	<ion-row  padding-horizontal *ngIf="false">\n		<ion-col>\n			<ion-searchbar\n				(ionFocus)="scrollTo(500)" \n				(ionInput)="getReceitas($event)" \n				placeholder="Busca" \n				autocomplete="on" \n				autocorrect="on" \n				animated="false">					\n			</ion-searchbar>\n		</ion-col>\n	</ion-row>\n	\n	<div class="filter-wrapper" *ngIf="receitas && refeicao">\n		<ion-searchbar\n			(ionInput)="getReceitas($event)"\n			placeholder="Busca"\n			autocomplete="on"\n			debounce="500"\n		>\n		</ion-searchbar>\n\n		<div class="receitas" *ngFor="let r of receitas" [ngSwitch]="r.tags.tipo">\n			\n			<!-- Para tags parentes -->\n			<div class="parente" *ngSwitchCase="\'parente\'">\n				<ion-list *ngIf="r.receitas.length" padding border-custom>\n					<ion-list-header>\n						{{r.tags.titulo}}\n					</ion-list-header>\n					<ion-item-group *ngFor="let rr of r.receitas">\n						<ion-item class="filho" (click)="rr.collapse = !rr.collapse" >\n							<ion-label>{{ rr.tags.titulo }} <span>({{rr.receitas.length}})</span></ion-label>\n							<button ion-button item-right clear>\n								<ion-icon class="fa" [class.fa-chevron-right]="!rr.collapse" [class.fa-chevron-down]="rr.collapse" color="secondary"></ion-icon>\n							</button>\n						</ion-item>\n						<div *ngIf="rr.collapse">\n							<ion-item *ngFor="let re of rr.receitas" class="receita">\n								<ion-label (click)="adicionarReceita(re)"><h2 class="titulo">{{re.titulo}}</h2> </ion-label>\n								<button class="receitaFav" (click)="updateFav(re)" item-right ion-button clear>\n									<ion-icon class="fa" [class.fa-star-o]="!re.favorito" [class.fa-star]="re.favorito" color="secondary"></ion-icon>\n								</button>\n								<button class="receitaModal" (click)="receitaModal(re)" item-right ion-button color="secondary" round>\n									<ion-icon class="fa fa-ellipsis-h" color="light"></ion-icon>\n								</button>\n							</ion-item>\n						</div>\n					</ion-item-group>\n				</ion-list>\n			</div>\n			\n			<div class="normal" *ngSwitchDefault>\n				<ion-list *ngIf="r.receitas.length" padding border-custom>\n					<ion-list-header (click)="r.collapse = !r.collapse">\n						<ion-label>\n							{{r.tags.titulo}} <span>({{r.receitas.length}})</span>\n						</ion-label>\n						<button ion-button item-right clear>\n							<ion-icon class="fa" [class.fa-chevron-right]="!r.collapse" [class.fa-chevron-down]="r.collapse" color="secondary"></ion-icon>\n						</button>\n					</ion-list-header>\n					<ion-item-group *ngIf="r.collapse">\n						<ion-item *ngFor="let re of r.receitas" class="receita">\n							<ion-label (press)="receitaModal(re)" (click)="adicionarReceita(re)"><h2 class="titulo">{{re.titulo}}</h2> </ion-label>\n							<button class="receitaFav" (click)="updateFav(re)" item-right ion-button clear>\n								<ion-icon class="fa" [class.fa-star-o]="!re.favorito" [class.fa-star]="re.favorito" color="secondary"></ion-icon>\n							</button>\n							<button class="receitaModal" (click)="receitaModal(re)" item-right ion-button color="secondary" round>\n								<ion-icon class="fa fa-ellipsis-h" color="light"></ion-icon>\n							</button>\n						</ion-item>\n					</ion-item-group>\n				</ion-list>\n			</div>\n		</div>\n	</div>\n\n</ion-content>'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/refeicao/refeicao.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_5__providers_cardapios__["a" /* CardapiosProvider */], __WEBPACK_IMPORTED_MODULE_6__providers_refeicoes__["a" /* RefeicoesProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_receitas__["a" /* ReceitasProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_favoritos__["a" /* FavoritosProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_listas__["a" /* ListasProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_8__providers_current_cardapios__["a" /* CurrentCardapiosProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_receitas__["a" /* ReceitasProvider */],
        __WEBPACK_IMPORTED_MODULE_6__providers_refeicoes__["a" /* RefeicoesProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_listas__["a" /* ListasProvider */],
        __WEBPACK_IMPORTED_MODULE_7__providers_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_favoritos__["a" /* FavoritosProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */]])
], RefeicaoPage);

//# sourceMappingURL=refeicao.js.map

/***/ })

});
//# sourceMappingURL=6.main.js.map