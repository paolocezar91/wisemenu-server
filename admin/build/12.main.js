webpackJsonp([12],{

/***/ 438:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaNovoItemModule", function() { return ListaNovoItemModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lista_item__ = __webpack_require__(511);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(321);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ListaNovoItemModule = (function () {
    function ListaNovoItemModule() {
    }
    return ListaNovoItemModule;
}());
ListaNovoItemModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__lista_item__["a" /* ListaItemPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__lista_item__["a" /* ListaItemPage */]),
            __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__lista_item__["a" /* ListaItemPage */]
        ]
    })
], ListaNovoItemModule);

//# sourceMappingURL=lista-item.module.js.map

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListaItemPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_produtos__ = __webpack_require__(512);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ListaNovoItem page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ListaItemPage = (function () {
    function ListaItemPage(navCtrl, navParams, viewCtrl, alertCtrl, loadingCtrl, pp, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.pp = pp;
        this.toastCtrl = toastCtrl;
        this.searching = false;
        this.searchDisabled = false;
        this.filterResults = [];
    }
    ListaItemPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ListaItemPage");
        this.init();
    };
    ListaItemPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando..."
        });
        this.loading.present();
    };
    ListaItemPage.prototype.init = function () {
        this.item = this.navParams.get("item");
        this.searchDisabled = this.item.produto_id._id ? true : false;
        this.itemForm = {
            _id: this.item.produto_id._id || "",
            titulo: this.item.produto_id.titulo || "",
            valor: this.item.valor || "",
            quantidade: this.item.quantidade || "",
            unidade: this.item.unidade || "",
            check: this.item.check || false
        };
        console.log(this.itemForm);
        this.searchText = this.item.produto_id.titulo || "";
    };
    ListaItemPage.prototype.add = function (itemForm) {
        var _this = this;
        this.showLoader();
        this.errorMessage = "";
        // Se há erros de validação do formulário
        if (!itemForm) {
            if (itemForm.titulo) {
                this.errorMessage += "Você deve digitar o nome do produto. <br>";
            }
            if (itemForm.quantidade) {
                this.errorMessage += "Você deve digitar uma quantidade para o produto. <br>";
            }
            console.log(itemForm.controls);
            this.loading.dismiss();
            this.showError(this.errorMessage);
        }
        else {
            var item_1 = {
                produto_id: {
                    _id: itemForm._id || false,
                    titulo: itemForm.titulo
                },
                valor: itemForm.valor,
                quantidade: itemForm.quantidade,
                unidade: itemForm.unidade,
                check: itemForm.check || false,
            };
            // Se o produto não tem _id, não existe no banco e deve ser criado.
            if (!item_1.produto_id._id) {
                var produto = {
                    titulo: item_1.produto_id.titulo,
                    sinonimos: [],
                    unidades_domesticas: [],
                    unidades_comerciais: [],
                    marcas_id: []
                };
                this.pp.create(produto).then(function (produto) {
                    item_1.produto_id._id = produto._id;
                    _this.loading.dismiss();
                    _this.dismiss(item_1);
                });
            }
            else {
                this.loading.dismiss();
                this.dismiss(item_1);
            }
        }
    };
    ListaItemPage.prototype.search = function (e) {
        var _this = this;
        this.searching = true;
        this.searchText = this.trim(e.srcElement.value);
        if (this.searchText) {
            if (this.searchText !== "") {
                setTimeout(function () {
                    _this.pp.getResults(_this.searchText).then(function (filterResults) {
                        _this.filterResults = filterResults;
                        _this.searching = false;
                    });
                    _this.itemForm.controls["titulo"].setValue(_this.searchText);
                }, 500);
            }
        }
        else {
            this.clear();
            this.searching = false;
        }
    };
    ListaItemPage.prototype.clear = function () {
        this.searchText = "";
        this.itemForm.controls["titulo"].setValue("");
        this.itemForm.controls["_id"].setValue("");
        this.filterResults = [];
    };
    ListaItemPage.prototype.blur = function () {
        var _this = this;
        setTimeout(function () {
            _this.filterResults = [];
        }, 300);
    };
    ListaItemPage.prototype.selectItem = function (e) {
        this.searchText = e.titulo;
        this.itemForm.controls["titulo"].setValue(e.titulo);
        this.itemForm.controls["_id"].setValue(e._id);
        this.filterResults = [];
    };
    ListaItemPage.prototype.trim = function (value) {
        if (value) {
            return value.replace(/^\s+/gi, "") // removes leading and trailing spaces
                .replace(/[ ]{2,}/gi, " ") // replaces multiple spaces with one space
                .replace(/\n +/, "\n"); // Removes spaces after newlines
        }
        else {
            return undefined;
        }
    };
    ListaItemPage.prototype.dismiss = function (data) {
        if (data !== undefined) {
            this.viewCtrl.dismiss(data);
        }
        else {
            this.viewCtrl.dismiss();
        }
    };
    ListaItemPage.prototype.showToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: "bottom",
            showCloseButton: true,
            closeButtonText: "X"
        });
        toast.present();
    };
    ListaItemPage.prototype.showError = function (errorMessage) {
        var alert = this.alertCtrl.create({
            title: "Erro",
            message: errorMessage,
            buttons: [
                { text: "Ok" }
            ]
        });
        alert.present();
    };
    return ListaItemPage;
}());
ListaItemPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-lista-item",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/lista-item/lista-item.html"*/'<!--\n	Generated template for the ListaNovoItem page.\n\n	See http://ionicframework.com/docs/components/#navigation for more info on\n	Ionic pages and navigation.\n-->\n<ion-header>\n\n	<ion-navbar>\n		<ion-title>Adicionar item a lista</ion-title>\n		<ion-buttons end>\n			<button class="close" ion-button (click)="dismiss()"><ion-icon color="light" class="fa fa-times"></ion-icon></button>\n		</ion-buttons>    \n	</ion-navbar>\n\n</ion-header>\n\n<ion-content *ngIf="itemForm">\n	<div ion-item class="autocomplete" padding-horizontal>\n		<ion-label stacked>\n			Produto <ion-icon *ngIf="!itemForm.titulo" class="fa fa-asterisk" color="danger"></ion-icon>\n		</ion-label>\n		<ion-input\n			[(ngModel)]="searchText"\n			class="searchBar"\n			placeholder="ex: arroz"\n			disabled="{{searchDisabled}}"\n			clearInput\n			(blur)="blur()"\n		  	(input)="search($event)"\n		  	>\n		</ion-input>\n		<ion-icon item-right class="fa fa-search"></ion-icon>\n		<ion-spinner item-right class="spinner" *ngIf="searching"></ion-spinner>\n	</div>\n	<ion-list border-custom class="filterResults" *ngIf="filterResults.length > 0" >\n		<ion-item *ngFor="let r of filterResults" (click)="selectItem(r)">\n			<span [innerHTML]="r.titulo | boldPrefix:searchText"></span>\n		</ion-item>\n	</ion-list>	\n	<ion-list border-custom *ngIf="itemForm" padding-horizontal>\n		<ion-item>\n			<ion-label stacked>\n				Quantidade\n			</ion-label>\n			<ion-input placeholder="ex: 200" type="number" [(ngModel)]="itemForm.quantidade"></ion-input>\n		</ion-item>\n		<ion-item>\n			<ion-label stacked>\n				Unidade	\n			</ion-label>\n			<ion-select placeholder="ex: g" [(ngModel)]="itemForm.unidade">\n				<ion-option value="kg">quilogramas (kg)</ion-option>\n					<ion-option value="g">gramas (g)</ion-option>\n					<ion-option value="ml">mililitros (ml)</ion-option>\n					<ion-option value="l">litros (l)</ion-option>\n					<ion-option value="unidade">unidade(s)</ion-option>\n			</ion-select>\n		</ion-item>\n		<ion-item>\n			<ion-label stacked>\n				Valor	\n			</ion-label>\n			<ion-input type="tel" placeholder="15,00" [(ngModel)]="itemForm.valor"></ion-input>\n		</ion-item>\n		<ion-item>\n			<ion-checkbox [(ngModel)]="itemForm.check"></ion-checkbox>\n			<ion-label>\n				Marcar item?\n			</ion-label>\n		</ion-item>\n	</ion-list>\n</ion-content>\n<ion-footer>\n	<button (click)="add(itemForm)" type="submit" ion-button full color="secondary">\n		<ion-label>OK</ion-label>\n	</button>\n</ion-footer>'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/lista-item/lista-item.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_produtos__["a" /* ProdutosProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_produtos__["a" /* ProdutosProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */]])
], ListaItemPage);

//# sourceMappingURL=lista-item.js.map

/***/ }),

/***/ 512:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProdutosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var ProdutosProvider = (function () {
    function ProdutosProvider(http, ap, ENV) {
        this.http = http;
        this.ap = ap;
        this.ENV = ENV;
    }
    ProdutosProvider.prototype.getResults = function (search) {
        var _this = this;
        var data = {};
        var options = { select: "titulo" };
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "produtos", { data: data, options: options }, { headers: headers })
                .map(function (res) {
                return res.json().filter(function (item) {
                    if (item) {
                        return item.titulo.toLowerCase().startsWith(search.toLowerCase());
                    }
                });
            })
                .subscribe(function (data) {
                var filter = data.filter(function (item) {
                    if (item) {
                        return item.titulo.toLowerCase().startsWith(search.toLowerCase());
                    }
                });
                resolve(filter);
            });
        });
    };
    ProdutosProvider.prototype.create = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.ap.getToken());
            _this.http.post(_this.ENV.API_URL + "produtos/create", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (produto) {
                _this.produto = produto;
                resolve(_this.produto);
            });
        });
    };
    return ProdutosProvider;
}());
ProdutosProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], ProdutosProvider);

//# sourceMappingURL=produtos.js.map

/***/ })

});
//# sourceMappingURL=12.main.js.map