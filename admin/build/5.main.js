webpackJsonp([5],{

/***/ 442:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReceitaModule", function() { return ReceitaModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__receita__ = __webpack_require__(517);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_gradacao_gradacao__ = __webpack_require__(519);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic2_rating__ = __webpack_require__(520);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ReceitaModule = (function () {
    function ReceitaModule() {
    }
    return ReceitaModule;
}());
ReceitaModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__receita__["a" /* ReceitaPage */],
            __WEBPACK_IMPORTED_MODULE_3__components_gradacao_gradacao__["a" /* Gradacao */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__receita__["a" /* ReceitaPage */]),
            __WEBPACK_IMPORTED_MODULE_4_ionic2_rating__["a" /* Ionic2RatingModule */]
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__receita__["a" /* ReceitaPage */]
        ]
    })
], ReceitaModule);

//# sourceMappingURL=receita.module.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReceitasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var ReceitasProvider = (function () {
    function ReceitasProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
    }
    ReceitasProvider.prototype.getReceitas = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.getReceitasFiltered = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/filtered", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    // Receita única
    ReceitasProvider.prototype.getReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.avaliarReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/avaliar", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.createOrUpdateReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/createOrUpdate", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.removeReceita = function (data, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/remove", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.upload = function (params, files, path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("attr", params.attr);
            for (var i = 0; i < files.length; i++) {
                formData.append("upload", files[i], files[i].name);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.response);
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", _this.ENV.API_URL + path, true);
            xhr.setRequestHeader("Authorization", _this.authProvider.getToken());
            xhr.send(formData);
        });
    };
    ReceitasProvider.prototype.uploadXls = function (params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.upload(params, files, "receitas/uploadXls")
                .then(function (result) {
                resolve(result);
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ReceitasProvider.prototype.uploadImg = function (params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.upload(params, files, "receitas/uploadImage");
        });
    };
    return ReceitasProvider;
}());
ReceitasProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], ReceitasProvider);

//# sourceMappingURL=receitas.js.map

/***/ }),

/***/ 452:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
  Generated class for the Favoritos provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var FavoritosProvider = (function () {
    function FavoritosProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
        console.log('Hello Filtros Provider');
    }
    FavoritosProvider.prototype.newFavorito = function (receita_id) {
        return {
            "user_id": this.authProvider.getCurrentUser()._id,
            "receita_id": receita_id
        };
    };
    FavoritosProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Authorization', _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + 'favoritos/one', { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (favorito) {
                _this.favorito = favorito;
                resolve(favorito);
            });
        });
    };
    FavoritosProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Authorization', _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + 'favoritos', { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (favoritos) {
                _this.favoritos = favoritos;
                resolve(favoritos);
            });
        });
    };
    FavoritosProvider.prototype.update = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append('Authorization', _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + 'favoritos/update', { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (favorito) {
                resolve(favorito);
            });
        });
    };
    return FavoritosProvider;
}());
FavoritosProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], FavoritosProvider);

//# sourceMappingURL=favoritos.js.map

/***/ }),

/***/ 486:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export RATING_CONTROL_VALUE_ACCESSOR */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Ionic2Rating; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(13);


var noop = function () {
};
var RATING_CONTROL_VALUE_ACCESSOR = {
    provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["NG_VALUE_ACCESSOR"],
    useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["forwardRef"])(function () { return Ionic2Rating; }),
    multi: true
};
var Ionic2Rating = (function () {
    function Ionic2Rating() {
        this._max = 5;
        this._readOnly = false;
        this._emptyStarIconName = 'star-outline';
        this._halfStarIconName = 'star-half';
        this._starIconName = 'star';
        this._nullable = false;
        this.onChangeCallback = noop;
    }
    Object.defineProperty(Ionic2Rating.prototype, "max", {
        get: function () {
            return this._max;
        },
        set: function (val) {
            this._max = this.getNumberPropertyValue(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ionic2Rating.prototype, "readOnly", {
        get: function () {
            return this._readOnly;
        },
        set: function (val) {
            this._readOnly = this.isTrueProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ionic2Rating.prototype, "emptyStarIconName", {
        get: function () {
            return this._emptyStarIconName;
        },
        set: function (val) {
            this._emptyStarIconName = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ionic2Rating.prototype, "halfStarIconName", {
        get: function () {
            return this._halfStarIconName;
        },
        set: function (val) {
            this._halfStarIconName = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ionic2Rating.prototype, "starIconName", {
        get: function () {
            return this._starIconName;
        },
        set: function (val) {
            this._starIconName = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ionic2Rating.prototype, "nullable", {
        get: function () {
            return this._nullable;
        },
        set: function (val) {
            this._nullable = this.isTrueProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Ionic2Rating.prototype.ngOnInit = function () {
        // ngFor needs an array
        this.starIndexes = Array(this.max).fill(1).map(function (x, i) { return i; });
    };
    Ionic2Rating.prototype.getStarIconName = function (starIndex) {
        if (this.value === undefined) {
            return this.emptyStarIconName;
        }
        if (this.value > starIndex) {
            if (this.value < starIndex + 1) {
                return this.halfStarIconName;
            }
            else {
                return this.starIconName;
            }
        }
        else {
            return this.emptyStarIconName;
        }
    };
    Object.defineProperty(Ionic2Rating.prototype, "value", {
        get: function () {
            return this.innerValue;
        },
        set: function (value) {
            if (value !== this.innerValue) {
                this.innerValue = value;
                this.onChangeCallback(value);
            }
        },
        enumerable: true,
        configurable: true
    });
    Ionic2Rating.prototype.writeValue = function (value) {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    };
    Ionic2Rating.prototype.registerOnChange = function (fn) {
        this.onChangeCallback = fn;
    };
    Ionic2Rating.prototype.registerOnTouched = function (fn) {
    };
    Ionic2Rating.prototype.onKeyDown = function (event) {
        if (/(37|38|39|40)/.test(event.which)) {
            event.preventDefault();
            event.stopPropagation();
            var newValue = this.value + ((event.which == 38 || event.which == 39) ? 1 : -1);
            return this.rate(newValue);
        }
    };
    Ionic2Rating.prototype.rate = function (value) {
        if (this.readOnly || value < 0 || value > this.max) {
            return;
        }
        if (value === this.value && this.nullable) {
            value = null;
        }
        this.value = value;
    };
    Ionic2Rating.prototype.isTrueProperty = function (val) {
        if (typeof val === 'string') {
            val = val.toLowerCase().trim();
            return (val === 'true' || val === 'on');
        }
        return !!val;
    };
    Ionic2Rating.prototype.getNumberPropertyValue = function (val) {
        if (typeof val === 'string') {
            return parseInt(val.trim());
        }
        return val;
    };
    Ionic2Rating.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"], args: [{
                    selector: 'rating',
                    styles: ["\n    ul.rating li {\n      display: inline;\n      border: 0px;\n      background: none;\n      padding: 5px 10px;\n    }\n    ul.rating li i {\n      font-size: 30px;\n    }\n  "],
                    template: "\n    <ul class=\"rating\" (keydown)=\"onKeyDown($event)\">\n      <li *ngFor=\"let starIndex of starIndexes\" tappable (click)=\"rate(starIndex + 1)\">\n        <ion-icon [name]=\"getStarIconName(starIndex)\">\n        </ion-icon>\n      </li>\n    </ul>",
                    providers: [RATING_CONTROL_VALUE_ACCESSOR]
                },] },
    ];
    /** @nocollapse */
    Ionic2Rating.ctorParameters = [];
    Ionic2Rating.propDecorators = {
        'max': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'readOnly': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'emptyStarIconName': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'halfStarIconName': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'starIconName': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
        'nullable': [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"] },],
    };
    return Ionic2Rating;
}());
//# sourceMappingURL=ionic2-rating.js.map

/***/ }),

/***/ 517:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReceitaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_receitas__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_favoritos__ = __webpack_require__(452);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_likes__ = __webpack_require__(518);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};







/*
    Generated class for the Receita page.

    See http://ionicframework.com/docs/v2/components/#navigation for more info on
    Ionic pages and navigation.
*/
var ReceitaPage = (function () {
    function ReceitaPage(viewCtrl, navParams, rp, favsp, lp, ap, loadingCtrl, toastCtrl, ENV) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.rp = rp;
        this.favsp = favsp;
        this.lp = lp;
        this.ap = ap;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.ENV = ENV;
        this.favorito = { active: false };
        this.like = { active: false };
        this.showLoader();
    }
    ReceitaPage.prototype.ionViewDidLoad = function () {
        console.log("Hello Receita Page");
        this.init();
    };
    ReceitaPage.prototype.init = function () {
        var _this = this;
        var data = { _id: this.navParams.get("receita")._id };
        var options = {};
        this.rp.getReceita(data, options).then(function (receita) {
            _this.receita = receita;
            var fav_data = { user_id: _this.ap.getCurrentUser()._id, receita_id: _this.receita._id };
            _this.favsp.getOne(fav_data, {}).then(function (favorito) {
                _this.favorito = favorito;
                _this.receita.favorito = true;
            });
            var like_data = { user_id: _this.ap.getCurrentUser()._id, receita_id: _this.receita._id };
            _this.lp.getOne(like_data, {}).then(function (like) {
                _this.like = like;
            });
            _this.loading.dismiss();
        });
    };
    ReceitaPage.prototype.updateFav = function () {
        var _this = this;
        this.showLoader();
        this.favorito.user_id = this.ap.getCurrentUser()._id;
        this.favorito.receita_id = this.receita._id;
        this.favorito.active = !this.favorito.active;
        this.favsp.update(this.favorito, {}).then(function (favorito) {
            var _f = favorito;
            if (_this.favorito.active) {
                _this.showToast("Receita adicionada aos favoritos");
            }
            else {
                _this.showToast("Receita removida dos favoritos");
            }
            if (_this.favorito._id === undefined) {
                _this.favorito._id = _f.upserted[0]._id;
            }
            _this.loading.dismiss();
        });
    };
    ReceitaPage.prototype.updateLike = function () {
        var _this = this;
        this.showLoader();
        this.like.likeData.user_id = this.ap.getCurrentUser()._id;
        this.like.likeData.receita_id = this.receita._id;
        this.like.likeData.active = !this.like.likeData.active;
        this.lp.update(this.like.likeData, {}).then(function (like) {
            var _f = like;
            if (_this.like.likeData.active) {
                _this.like.count++;
                _this.showToast("Receita curtida");
            }
            else {
                _this.showToast("Receita descurtida");
                _this.like.count--;
            }
            if (_this.like.likeData._id === undefined) {
                _this.like.likeData._id = _f.upserted[0]._id;
            }
            _this.loading.dismiss();
        });
    };
    ReceitaPage.prototype.dismiss = function (data) {
        this.viewCtrl.dismiss(data);
    };
    ReceitaPage.prototype.avaliar = function (v) {
        var _this = this;
        var data = { _id: this.receita._id, user_id: this.ap.getCurrentUser()._id, valor: v };
        var options = {};
        this.rp.avaliarReceita(data, options).then(function (result) {
            _this.showToast("Receita avaliada");
        });
    };
    ReceitaPage.prototype.showToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: "bottom",
            showCloseButton: true,
            closeButtonText: "X"
        });
        toast.present();
    };
    ReceitaPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando..."
        });
        this.loading.present();
    };
    ReceitaPage.prototype.sharePage = function () {
        alert("Compartilhar");
    };
    return ReceitaPage;
}());
ReceitaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-receita",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/receita/receita.html"*/'<!--\n  Generated template for the Receita page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title *ngIf="receita">{{receita.titulo}}</ion-title>\n    <ion-buttons end>\n      <button class="close" ion-button (click)="dismiss()"><ion-icon color="light" class="fa fa-times"></ion-icon></button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding *ngIf="receita">\n	<div class="receita">\n		<ion-card *ngIf="receita.icone">\n			<img src="{{ENV.DOWNLOAD_PATH + receita.icone}}" alt="{{receita.titulo}}" />\n		</ion-card>\n		<ion-grid id="informacao" padding-horizontal>\n			<ion-row>\n				<ion-col col-4 id="adicionar" class="action">\n					<button ion-button clear class="adicionar" (click)="dismiss(true)">\n						<ion-label>Adicionar <br>ao cardápio</ion-label>\n						<ion-icon color="secondary" class="fa fa-plus-circle active"></ion-icon>\n					</button>\n				</ion-col>\n				<ion-col col-4 id="favoritos" class="action">\n					<button ion-button clear class="action" (click)="updateFav()">\n						<ion-label *ngIf="!favorito.active">Adicionar <br>aos favoritos</ion-label>\n						<ion-label *ngIf="favorito.active">Remover <br>dos favoritos</ion-label>\n						<ion-icon class="fa" [class.fa-star-o]="!favorito.active" [class.fa-star]="favorito.active" [class.active]="favorito.active"></ion-icon>\n					</button>\n				</ion-col>\n				<ion-col col-4 id="like" class="action">\n					<button ion-button clear class="action" (click)="updateLike()" *ngIf="like.likeData">\n						<ion-label *ngIf="!like.likeData.active">Curtir</ion-label>\n						<ion-label *ngIf="like.likeData.active">Descurtir</ion-label>\n						<ion-label>\n							<ion-icon class="fa fa-thumbs-up" [class.active]="like.likeData.active"></ion-icon>\n							{{like.count}}\n						</ion-label>\n					</button>\n				</ion-col>\n			</ion-row>\n			<ion-row>\n				<ion-col col-4 id="grau_de_dificuldade" class="info">\n					<div *ngIf="receita.dificuldade">\n						<ion-label>Dificuldade</ion-label>\n						<gradacao [grau]="receita.dificuldade" [image]="\'dificuldade\'"></gradacao>\n					</div>\n				</ion-col>\n				<ion-col col-4 id="tempo_de_preparo" class="info">\n					<div *ngIf="receita.tempo_de_preparo">\n						<ion-label>Tempo<br>de preparo</ion-label>\n						<p>{{ receita.tempo_de_preparo }}</p>\n					</div>	\n				</ion-col>\n				<ion-col col-4 id="rendimento" class="info">\n					<div *ngIf="receita.rendimento">\n						<ion-label>Porções<br>sugeridas</ion-label>\n						<gradacao [grau]="receita.rendimento" [image]="\'rendimento\'"></gradacao>\n					</div>\n				</ion-col>\n			</ion-row>\n			<ion-row *ngIf="receita.ingredientes" id="ingredientes" class="main-info">\n				<ion-col>\n					<h1>Ingredientes</h1>\n					<ion-list>\n						<ion-item *ngFor="let i of receita.ingredientes"> \n							<h3>{{ i.ingrediente }}</h3>\n							<p>{{ i.porcao }} {{ i.unidade }} </p>\n						</ion-item>\n					</ion-list>\n				</ion-col>\n			</ion-row>\n			<ion-row *ngIf="receita.modo_de_preparo" id="modo_de_preparo" class="main-info">\n				<ion-col>\n					<h1>Modo de preparo</h1>\n					<p [innerHTML]="receita.modo_de_preparo"></p>\n				</ion-col>\n			</ion-row>\n			<ion-row *ngIf="receita.informacoes_nutricionais" id="informacoes_nutricionais" class="main-info">\n				<ion-col>\n					<h1>{{ receita.informacoes_nutricionais.titulo }}</h1>\n					<p [innerHTML]="receita.informacoes_nutricionais"></p>				\n				</ion-col>\n			</ion-row>\n		</ion-grid>\n	</div>\n</ion-content>'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/receita/receita.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_receitas__["a" /* ReceitasProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_favoritos__["a" /* FavoritosProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_likes__["a" /* LikesProvider */]]
    }),
    __param(8, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_6__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_receitas__["a" /* ReceitasProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_favoritos__["a" /* FavoritosProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_likes__["a" /* LikesProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* ToastController */], Object])
], ReceitaPage);

//# sourceMappingURL=receita.js.map

/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LikesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/*
    Generated class for the Likes provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular 2 DI.
*/
var LikesProvider = (function () {
    function LikesProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
        console.log("Hello Filtros Provider");
    }
    LikesProvider.prototype.newLike = function (receita_id) {
        return {
            "user_id": this.authProvider.getCurrentUser()._id,
            "receita_id": receita_id
        };
    };
    LikesProvider.prototype.getOne = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "likes/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (like) {
                resolve(like);
            });
        });
    };
    LikesProvider.prototype.getAll = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "likes", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (likes) {
                resolve(likes);
            });
        });
    };
    LikesProvider.prototype.update = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "likes/update", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (like) {
                resolve(like);
            });
        });
    };
    return LikesProvider;
}());
LikesProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], LikesProvider);

//# sourceMappingURL=likes.js.map

/***/ }),

/***/ 519:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Gradacao; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Gradacao = (function () {
    function Gradacao() {
        this.range = [1, 2, 3, 4, 5];
        this.arr = Array;
    }
    return Gradacao;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Number)
], Gradacao.prototype, "grau", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], Gradacao.prototype, "icone", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], Gradacao.prototype, "image", void 0);
Gradacao = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "gradacao",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/components/gradacao/gradacao.html"*/'<p tabindex="0">\n	<span *ngFor="let i of arr(grau).fill(1)">\n  		<ion-icon *ngIf="icone" class="fa {{ icone }}"></ion-icon>\n  		<img class="{{image}}" *ngIf="image" src="assets/img/{{image}}.png" />\n  	</span>\n</p>'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/components/gradacao/gradacao.html"*/,
    }),
    __metadata("design:paramtypes", [])
], Gradacao);

//# sourceMappingURL=gradacao.js.map

/***/ }),

/***/ 520:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic2_rating_module__ = __webpack_require__(521);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__ionic2_rating_module__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic2_rating__ = __webpack_require__(486);
/* unused harmony reexport Ionic2Rating */


//# sourceMappingURL=index.js.map

/***/ }),

/***/ 521:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Ionic2RatingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic2_rating__ = __webpack_require__(486);




var Ionic2RatingModule = (function () {
    function Ionic2RatingModule() {
    }
    Ionic2RatingModule.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                    declarations: [
                        __WEBPACK_IMPORTED_MODULE_3__ionic2_rating__["a" /* Ionic2Rating */]
                    ],
                    exports: [
                        __WEBPACK_IMPORTED_MODULE_3__ionic2_rating__["a" /* Ionic2Rating */]
                    ],
                    imports: [
                        __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicModule */]
                    ],
                    schemas: [
                        __WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]
                    ]
                },] },
    ];
    /** @nocollapse */
    Ionic2RatingModule.ctorParameters = [];
    return Ionic2RatingModule;
}());
//# sourceMappingURL=ionic2-rating.module.js.map

/***/ })

});
//# sourceMappingURL=5.main.js.map