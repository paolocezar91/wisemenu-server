webpackJsonp([22],{

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(508);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomeModule = (function () {
    function HomeModule() {
    }
    return HomeModule;
}());
HomeModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]
        ]
    })
], HomeModule);

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 508:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
    Generated class for the Index page.

    See http://ionicframework.com/docs/v2/components/#navigation for more info on
    Ionic pages and navigation.
*/
var HomePage = (function () {
    function HomePage(navCtrl, ap) {
        this.navCtrl = navCtrl;
        this.ap = ap;
    }
    HomePage.prototype.ionViewDidLoad = function () {
        console.log("Hello Home Page");
        this.init();
    };
    HomePage.prototype.init = function () {
        var _this = this;
        this.menu = [{
                id: "montar-cardapio",
                icon: "fa-file-o",
                title: "Montar cardápio",
                callback: function () { return _this.pushPage("CardapioPage"); }
            }, {
                id: "lista-de-compras",
                icon: "fa-align-left",
                title: "Minhas listas",
                callback: function () { return _this.pushPage("ListasPage"); }
            }];
    };
    HomePage.prototype.pushPage = function (p) {
        this.navCtrl.push(p);
    };
    HomePage.prototype.logout = function () {
        var _this = this;
        this.ap.logout().then(function (result) {
            if (result) {
                _this.navCtrl.setRoot("LoginPage");
            }
        });
    };
    HomePage.prototype.pushAdmin = function () {
        this.navCtrl.setRoot("AdminHomePage");
    };
    HomePage.prototype.roleCheck = function (role) {
        this.ap.currentUserRoleCheck(role)
            .then(function (check) {
            return check;
        });
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-home",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/home/home.html"*/'<!--\n  Generated template for the Index page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar hideBackButton>\n    <ion-buttons left>\n      <button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n    </ion-buttons>\n    <ion-title>\n    	<img src="assets/img/wisemenu-logo-menu.png" alt="Wisemenu">\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n	<ion-list class="main-menu" padding>\n		<button id="{{m.id}}" clear item ion-button full (click)="m.callback()" *ngFor="let m of menu">\n		    <ion-label>{{ m.title }}</ion-label>\n		</button>\n    <button id="publicidade" clear item ion-button full>\n        <ion-label>Publicidade</ion-label>\n    </button>\n	</ion-list>\n</ion-content>'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_auth__["a" /* AuthProvider */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=22.main.js.map