webpackJsonp([18],{

/***/ 423:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminReceitasModule", function() { return AdminReceitasModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__admin_receitas__ = __webpack_require__(493);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(320);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AdminReceitasModule = (function () {
    function AdminReceitasModule() {
    }
    return AdminReceitasModule;
}());
AdminReceitasModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__admin_receitas__["a" /* AdminReceitasPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__admin_receitas__["a" /* AdminReceitasPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__admin_receitas__["a" /* AdminReceitasPage */]
        ]
    })
], AdminReceitasModule);

//# sourceMappingURL=admin-receitas.module.js.map

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReceitasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var ReceitasProvider = (function () {
    function ReceitasProvider(http, authProvider, ENV) {
        this.http = http;
        this.authProvider = authProvider;
        this.ENV = ENV;
    }
    ReceitasProvider.prototype.getReceitas = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.getReceitasFiltered = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/filtered", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    // Receita única
    ReceitasProvider.prototype.getReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/one", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.avaliarReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/avaliar", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.createOrUpdateReceita = function (data, options) {
        var _this = this;
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/createOrUpdate", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.removeReceita = function (data, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        return new Promise(function (resolve) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            headers.append("Authorization", _this.authProvider.getToken());
            _this.http.post(_this.ENV.API_URL + "receitas/remove", { data: data, options: options }, { headers: headers })
                .map(function (res) { return res.json(); })
                .subscribe(function (data) {
                resolve(data);
            });
        });
    };
    ReceitasProvider.prototype.upload = function (params, files, path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var formData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append("attr", params.attr);
            for (var i = 0; i < files.length; i++) {
                formData.append("upload", files[i], files[i].name);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.response);
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open("POST", _this.ENV.API_URL + path, true);
            xhr.setRequestHeader("Authorization", _this.authProvider.getToken());
            xhr.send(formData);
        });
    };
    ReceitasProvider.prototype.uploadXls = function (params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.upload(params, files, "receitas/uploadXls")
                .then(function (result) {
                resolve(result);
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ReceitasProvider.prototype.uploadImg = function (params, files) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            return _this.upload(params, files, "receitas/uploadImage");
        });
    };
    return ReceitasProvider;
}());
ReceitasProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__auth__["a" /* AuthProvider */], Object])
], ReceitasProvider);

//# sourceMappingURL=receitas.js.map

/***/ }),

/***/ 493:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminReceitasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_receitas__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





/**
 * Generated class for the AdminReceitas page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AdminReceitasPage = (function () {
    function AdminReceitasPage(navCtrl, ap, rp, loadingCtrl, events, ENV) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.ap = ap;
        this.rp = rp;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.ENV = ENV;
        // este objeto direciona a criação da table-admin com os parametros corretos
        this.options = {
            cols: [
                {
                    label: "Título",
                    attr: "titulo"
                }
            ],
            page: 1,
            limit: 10,
            buttons: [
                {
                    label: "Novo",
                    color: "primary",
                    icon: "fa-plus",
                    callback: function () { _this.pushAdminReceitaPage({ _id: "new" }); }
                },
                {
                    label: "Importar .xlsx",
                    color: "verde",
                    icon: "fa-file-excel-o",
                    callback: function () { _this.importExcel(); }
                }
            ]
        };
    }
    AdminReceitasPage.prototype.ionViewCanEnter = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // Checagem de role do usuário
            _this.ap.currentUserRoleCheck("admin").then(function (res) {
                if (!res) {
                    reject("Não autorizado");
                }
                else {
                    resolve(true);
                }
            });
        }).catch(function (err) { return _this.navCtrl.setRoot("HomePage"); });
    };
    AdminReceitasPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad AdminReceitas");
        this.getReceitas(this.options.page, this.options.limit);
    };
    AdminReceitasPage.prototype.getReceitas = function (page, limit) {
        var _this = this;
        try {
            var data = {}, options = { "page": page, "limit": limit, select: "titulo" };
            this.showLoader();
            this.rp.getReceitas(data, options).then(function (receitas) {
                _this.items = receitas;
                _this.loading.dismiss();
            });
        }
        catch (err) {
            console.log(err);
            this.loading.dismiss();
        }
    };
    AdminReceitasPage.prototype.setOptions = function (options) {
        this.options = options;
        this.getReceitas(this.options.page, this.options.limit);
    };
    AdminReceitasPage.prototype.showLoader = function () {
        this.loading = this.loadingCtrl.create({
            content: "Carregando..."
        });
        this.loading.present();
    };
    AdminReceitasPage.prototype.importExcel = function () {
        this.fileInput.nativeElement.click();
    };
    AdminReceitasPage.prototype.uploadXls = function (fileInput) {
        var _this = this;
        this.filesToUpload = fileInput.target.files;
        var params = {
            attr: fileInput.target.name,
        };
        this.showLoader();
        this.rp.uploadXls(params, this.filesToUpload).then(function (result) {
            _this.loading.dismiss();
            _this.getReceitas(_this.options.page, _this.options.limit);
        }, function (err) {
            _this.loading.dismiss();
        });
    };
    AdminReceitasPage.prototype.removeItem = function (event) {
        var _this = this;
        var data = { _id: event._id }, options = { "page": this.options.page, "limit": this.options.limit, select: "titulo" };
        this.showLoader();
        // Recarrega receitas
        this.rp.removeReceita(data, options).then(function (receitas) {
            _this.items = receitas;
            _this.loading.dismiss();
        });
    };
    AdminReceitasPage.prototype.pushAdminReceitaPage = function (item) {
        var _this = this;
        this.navCtrl.push("AdminReceita", { _id: item._id });
        this.events.subscribe("reloadPage", function () {
            _this.getReceitas(_this.options.page, _this.options.limit);
        });
    };
    AdminReceitasPage.prototype.rootAdminHome = function () {
        this.navCtrl.setRoot("AdminHomePage");
    };
    return AdminReceitasPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fileInput"),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], AdminReceitasPage.prototype, "fileInput", void 0);
AdminReceitasPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-admin-receitas",template:/*ion-inline-start:"/home/betoncios/Documents/wisemenu-ionic/src/pages/admin-receitas/admin-receitas.html"*/'<!--\n	Generated template for the AdminReceitas page.\n\n	See http://ionicframework.com/docs/components/#navigation for more info on\n	Ionic pages and navigation.\n-->\n<ion-header>\n	<ion-navbar hideBackButton>\n		<ion-buttons left>\n      <button ion-button menuToggle><ion-icon class="fa fa-bars" color="light"></ion-icon></button>\n    </ion-buttons>\n		<ion-title>\n			Receitas\n		</ion-title>\n		<ion-buttons end>\n			<button class="back" ion-button (click)="rootAdminHome()"><img src="assets/img/retornar.png"></button>\n		</ion-buttons>\n	</ion-navbar>\n</ion-header>\n\n<ion-content padding>\n	<h1>Receitas</h1>\n\n	<admin-table *ngIf="items" \n		[items]="items"\n		[options]="options"\n		(setOptions)="setOptions($event)"\n		(editItem)="pushAdminReceitaPage($event)"\n		(removeItem)="removeItem($event)">\n	</admin-table>\n	<input #fileInput type="file" (change)="uploadXls($event)" />\n</ion-content>\n'/*ion-inline-end:"/home/betoncios/Documents/wisemenu-ionic/src/pages/admin-receitas/admin-receitas.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_receitas__["a" /* ReceitasProvider */]]
    }),
    __param(5, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_4__env_environment_variables_token__["a" /* ENV */])),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_receitas__["a" /* ReceitasProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Events */], Object])
], AdminReceitasPage);

//# sourceMappingURL=admin-receitas.js.map

/***/ })

});
//# sourceMappingURL=18.main.js.map