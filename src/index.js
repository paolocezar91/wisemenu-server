var express  = require("express"),
	app = express(), // create our app w/ express
	https = require("https"),
	http = require("http"),
	mongoose = require("mongoose"),
	databaseConfig = require("./config/database"),
	morgan = require("morgan"), // log requests to the console (express4)
	bodyParser = require("body-parser"), // pull information from HTML POST (express4)
	methodOverride = require("method-override"), // simulate DELETE and PUT (express4)
	cors = require("cors"),
	path = require("path"),
	session = require("express-session"),
	fs = require("fs"),
	debug = require("debug")("dev")

debug("** Wisemenu API **")
debug("enviroment=dev\n")

app.use(morgan("dev")) // log every request to the console
app.use(bodyParser.urlencoded({"extended":"true"})) // parse application/x-www-form-urlencoded
app.use(bodyParser.json()) // parse application/json
app.use(bodyParser.json({ type: "application/vnd.api+json" })) // parse application/vnd.api+json as json
app.use(methodOverride())
app.use(cors())

app.use(session({
	secret: "wisemenu",
	resave: false,
	saveUninitialized: true
}))

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*")
	res.header("Access-Control-Allow-Methods", "GET,HEAD,PUT,PATCH,POST,DELETE")
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")

	next()
})

var	routes = require("./http/routes"),
	filters = require("./http/filters"),
	router = require("./http/router")

// binds router to main app 
app.use("/api", router(routes, filters) )
app.use("/uploads", express.static(path.join(__dirname, "/../", "uploads")))
app.use("/admin", express.static(path.join(__dirname, "/../", "admin")))

// listen (start app with node server.js)
if(process.env.ENV == "prod"){

	debug("ENV=prod")
	mongoose.connect("mongodb://127.0.0.1:2727/" + databaseConfig.db_name , {
		user: databaseConfig.user,
		pass: databaseConfig.password,
		server: { socketOptions: { keepAlive: 1 } },
		auth: { authSource: databaseConfig.authSource }
	})
	var options = {
		key: fs.readFileSync("tech4archi.key", "utf-8"),
		cert: fs.readFileSync("tech4archi.crt", "utf-8")
	}
	https.createServer(options, app).listen(8443)
	debug("https server listening on port 8443")
	http.createServer(function (req, res) {
		res.writeHead(301, { "Location": "https://" + req.headers["host"] + req.url })
		res.end()
	}).listen(8080)
	debug("http server redirecting to https on port 8080")

} else if (process.env.ENV == "dev") {

	debug("ENV=dev")
	debug("mongodb://localhost/" + databaseConfig.db_name)
	mongoose.connect("mongodb://localhost/" + databaseConfig.db_name)
	http.createServer(app).listen(8080)
	debug("http server listening on port 8080")

} else {

	debug("ENV=test")
	mongoose.connect("mongodb://127.0.0.1:27017/" + databaseConfig.db_name , {
		user: databaseConfig.user,
		pass: databaseConfig.password,
		server: { socketOptions: { keepAlive: 1 } },
		auth: { authSource: databaseConfig.authSource }
	})
	http.createServer(app).listen(8080)
	debug("http server listening on port 8080")
	
}
