require("../config/passport")

const passport = require("passport"),
	requireAuth = passport.authenticate("jwt", {session: false}),
	requireLogin = passport.authenticate("local", {session: false, badRequestMessage: "Digite as credenciais"}),
	authService = require("../services/auth.js"),

	multer = require("multer"),
	storageProperties = multer.diskStorage({
		destination: function (req, file, callback) {
			callback(null, "./uploads") //file destination
		},
		filename: function (req, file, callback) {
			callback(null, Date.now() + "_" + file.originalname )
		}
	}),
	upload = multer({ storage: storageProperties }),
	
	filters = {
		login: requireLogin,
		auth: requireAuth,
		user: authService.roleAuthorization( "user" ),
		admin: authService.roleAuthorization( "admin" ),
		upload: upload.single("upload")
	}

module.exports = filters