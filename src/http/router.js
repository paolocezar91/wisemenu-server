// Dependencies
const express = require("express"),
	debug = require("debug")("dev")

module.exports = (routes, filters) => {
	const mainRouter = express.Router()
	let subRouter

	//Building routes: uses routes.js as object to create routes
	routes.forEach((route) => {
		debug(route.path)
		subRouter = express.Router()
		
		route.subroutes.forEach((subroute) => {
			debug(subroute.method + ": " + subroute.path + " :: " + subroute.filters + " :: " + subroute.ctrl + "@" + subroute.callback)
			if(subroute.ctrl){
				// building middleware: pushing each function on filter.js as required by each route
				// example: middleware = [authCtrl.roleAuthorization( "user" ), authCtrl.roleAuthorization( "admin" )]
				const middleware = []

				subroute.filters.forEach((f) => {
					middleware.push(filters[f])
				})


				// getting controller required by route
				// example: ctrl = require("./api/users.js")
				const ctrl = require("../api/" + subroute.ctrl + ".js")

				// subRouter["get"]("/", ctrl["getAll"])
				subRouter[subroute.method](subroute.path, middleware, ctrl[subroute.callback])
			}
		})
		mainRouter.use(route.path, subRouter)
		
	})

	return mainRouter
}
