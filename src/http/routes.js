module.exports = [
	{
		path: "/auth",
		subroutes:[
			{
				method: "post",
				filters: [],
				path: "/register",
				ctrl: "auth",
				callback: "register"
			},
			{
				method: "post",
				filters: ["login"],
				path: "/login",
				ctrl: "auth",
				callback: "login"
			},
			{
				method: "post",
				filters: [],
				path: "/guest",
				ctrl: "auth",
				callback: "guest"
			},
			{
				method: "post",
				filters: [],
				path: "/facebook",
				ctrl: "auth",
				callback: "facebook"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/logout",
				ctrl: "auth",
				callback: "logout"
			},
			{
				method: "post",
				filters: ["login"],
				path: "/resetPassword",
				ctrl: "auth",
				callback: "resetPassword"
			},
			{
				method: "get",
				filters: ["auth"],
				path: "/protected",
				ctrl: "auth",
				callback: "protected"
			},
			{
				method: "get",
				filters: [],
				path: "/error",
				ctrl: "auth",
				callback: "error"
			},
		]
	},
	{
		path: "/user",
		subroutes:[
			{
				method: "get",
				filters: ["auth"],
				path: "/",
				ctrl: "user",
				callback: "getAll"
			},
			{
				method: "get",
				filters: ["auth"],
				path: "/:user_id",
				ctrl: "user",
				callback: "getOne"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/create",
				ctrl: "user",
				callback: "create"
			},
		]
	},
	{
		path: "/receitas",
		subroutes:[
			{
				method: "post",
				filters: ["auth"],
				path: "/",
				ctrl: "receitas",
				callback: "getAll"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/search",
				ctrl: "receitas",
				callback: "searchAll"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/filtered",
				ctrl: "receitas",
				callback: "getAllFiltered"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/one",
				ctrl: "receitas",
				callback: "getOne"
			},
			{
				method: "post",
				filters: ["auth", "admin"],
				path: "/createOrUpdate",
				ctrl: "receitas",
				callback: "createOrUpdate"
			},
			{
				method: "post",
				filters: ["auth", "admin"],
				path: "/remove",
				ctrl: "receitas",
				callback: "remove"
			},
			{
				method: "post",
				filters: ["auth", "admin", "upload"],
				path: "/uploadImage",
				ctrl: "receitas",
				callback: "uploadImage"
			},
			{
				method: "post",
				filters: ["auth", "admin", "upload"],
				path: "/uploadXls",
				ctrl: "receitas",
				callback: "uploadXls"
			},
		]
	},
	{
		path: "/listas",
		subroutes:[
			{
				method: "post",
				filters: ["auth"],
				path: "/",
				ctrl: "listas",
				callback: "getAll"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/one",
				ctrl: "listas",
				callback: "getOne"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/create",
				ctrl: "listas",
				callback: "create"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/update",
				ctrl: "listas",
				callback: "update"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/delete",
				ctrl: "listas",
				callback: "delete"
			},
		]
	},
	{
		path: "/tags",
		subroutes:[
			{
				method: "post",
				filters: ["auth"],
				path: "/",
				ctrl: "tags",
				callback: "getAll"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/one",
				ctrl: "tags",
				callback: "getOne"
			},
			{
				method: "post",
				filters: ["auth", "admin"],
				path: "/create",
				ctrl: "tags",
				callback: "create"
			},
			{
				method: "post",
				filters: ["auth", "admin"],
				path: "/createOrUpdate",
				ctrl: "tags",
				callback: "createOrUpdate"
			},
			{
				method: "post",
				filters: ["auth", "admin", "upload"],
				path: "/import",
				ctrl: "tags",
				callback: "import"
			},
		]
	},
	{
		path: "/filtros",
		subroutes:[
			{
				method: "post",
				filters: ["auth"],
				path: "/",
				ctrl: "filtros",
				callback: "getAll"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/one",
				ctrl: "filtros",
				callback: "getOne"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/create",
				ctrl: "filtros",
				callback: "createOrUpdate"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/update",
				ctrl: "filtros",
				callback: "createOrUpdate"
			},
		]
	},
	{
		path: "/produtos",
		subroutes:[
			{
				method: "post",
				filters: ["auth"],
				path: "/",
				ctrl: "produtos",
				callback: "getAll"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/one",
				ctrl: "produtos",
				callback: "getOne"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/create",
				ctrl: "produtos",
				callback: "create"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/update",
				ctrl: "produtos",
				callback: "createOrUpdate"
			},
		]
	},
	{
		path: "/favoritos",
		subroutes:[
			{
				method: "post",
				filters: ["auth"],
				path: "/",
				ctrl: "favoritos",
				callback: "getAll"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/one",
				ctrl: "favoritos",
				callback: "getOne"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/create",
				ctrl: "favoritos",
				callback: "createOrUpdate"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/update",
				ctrl: "favoritos",
				callback: "createOrUpdate"
			},
		]
	},
	{
		path: "/likes",
		subroutes:[
			{
				method: "post",
				filters: ["auth"],
				path: "/",
				ctrl: "likes",
				callback: "getAll"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/one",
				ctrl: "likes",
				callback: "getOne"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/create",
				ctrl: "likes",
				callback: "createOrUpdate"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/update",
				ctrl: "likes",
				callback: "createOrUpdate"
			},
		]
	},
	{
		path: "/cardapios",
		subroutes:[
			{
				method: "post",
				filters: ["auth"],
				path: "/",
				ctrl: "cardapios",
				callback: "getAll"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/one",
				ctrl: "cardapios",
				callback: "getOne"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/create",
				ctrl: "cardapios",
				callback: "create"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/delete",
				ctrl: "cardapios",
				callback: "delete"
			},
			{
				method: "post",
				filters: ["auth"],
				path: "/update",
				ctrl: "cardapios",
				callback: "update"
			},
		]
	},
	{
		path: "/ajudas",
		subroutes:[
			{
				method: "post",
				filters: ["auth"],
				path: "/",
				ctrl: "ajudas",
				callback: "getAll"
			},
		]
	},
]
