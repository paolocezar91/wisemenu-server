var Cardapio = require('../models/cardapio.js');

exports.getAll = function(req, res) {
  var options = req.body.options;

  var cardapios = Cardapio.find(req.body.data);

  if(options.select) {cardapios.select(options.select)};

  if(options.count){cardapios.count()}

  cardapios.exec(function(err, data) {
    if (err){
      res.status(500).send(err);
    } else {
      res.status(200).json(data);
    }
  });
}

exports.getOne = function(req, res) {
  var options = req.body.options;

  var cardapio = Cardapio.findOne(req.body.data);
  
  cardapio.exec(function(err, data) {
    if (err){
      res.status(500).send(err);
    } else {
      res.status(200).json(data);
    }
  });
}

exports.create = function(req, res){

  Cardapio.create(req.body.data, function(err, data){
    if (err){
      res.status(500).send(err);
    } else {
      res.status(200).send(data);
    }
  });
 
}

exports.delete = function(req, res){
  var options = req.body.options;
  var data = req.body.data;

  Cardapio.remove(data, function(err) {
    if (err){
      res.status(500).send(err);
    } else {
      res.status(200).json("Removido com sucesso");
    }
  });
}

exports.update = function(req, res){
  var options = req.body.options;
  var data = req.body.data;

  Cardapio.findByIdAndUpdate(data._id, data, function(err, cardapio){
    if (err){
      res.status(500).send(err);
    } else {
      res.status(200).send(cardapio);
    }
  });
 
}