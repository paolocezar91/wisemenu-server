var Ajuda = require('../models/ajuda');

exports.getAll = function(req, res) {
	var  data = req.body.data;
	var options = req.body.options;
  
  var ajudas = Ajuda.find(data);
  
  if (options.sort) {ajudas.sort(options.sort);}
  ajudas.exec(function(err, data) {
    if (err){
      res.status(500).send(err);
    } else {
      res.send(data);
    }
  });
}