const mongoose = require("mongoose"),
	Receita = require("../models/receita.js"),
	Filtro = require("../models/filtro.js"),
	Tag = require("../models/tag.js"),
	Favorito = require("../models/favorito.js"),
	XLSX = require("xlsx"),
	async = require("async"),
	sizeOf = require("image-size"),
	fs = require("fs"),
	_ = require("lodash"),
	debug = require("debug")("dev")

exports.getAll = function(req, res) {
	var data = req.body.data
	var options = req.body.options

	Receita.paginate(data, options, function(err, data) {
		if (err){
			res.status(500).send(err)
		} else {
			res.status(200).json(data)
		}
	})
}

exports.searchAll = function(req, res) {
	var data = req.body.data

	Receita.find({titulo: new RegExp(data.titulo, 'i'), tags: {$in: data.tags}}, function(err, docs){
		if (err){
			res.status(500).send(err)
		} else {
			res.status(200).json(docs)
		}
	});
}

exports.getAllFiltered = function(req, res){
	var data = req.body.data

	/* Pega o filtro do usuário */
	async.waterfall([
		function(callback){

			Filtro.findOne({user_id: data.user_id})
				.select("preferencias restricoes.tags")
				.exec(function(err, _f){
					if (err){
						res.status(500).send(err)
					} else {
						if (_f) {
							_f = _f.toObject()
						}
						callback(null, _f)
					}
				})
		},
		function (filtro, callback){
			if(data.orderBy){
				var tags_orderBy = Tag.find({tipo: data.orderBy})
				tags_orderBy.exec((err, results) => {
					if(err) {
						res.status(500).send(err)
					} else {
						results = results.map((t) => {
							return mongoose.Types.ObjectId(t._id)
						})
						callback(null, filtro, results)
					}
				})
			} else {
				callback(null, filtro, false)
			}
		},
		function(filtro, tags_orderBy, callback){
			var _r = Receita.aggregate()
			// filtro de busca
			if (data.search) {
				_r.match(
					{ $text: { $search: data.search } },
					{ score: { $meta: "textScore" } }
				)
				_r.match({ "tags": { $in : tags_orderBy } })
				_r.sort( { score: { $meta: "textScore" } } )
				_r.unwind("tags")
				_r.group({
					_id: "$tags",
					receitas: {
						$push: {
							_id: "$_id",
							titulo: "$titulo",
							favorito: {
								$setIsSubset: [
									[mongoose.Types.ObjectId(data.user_id)],
									"$favoritos"
								]
							},
							score: { $meta: "textScore" }
						}
					}
				})
			} else {
				// filtros de restrições
				if (filtro) {
					var match
					var $nin = filtro.restricoes.tags
					// filtrando tags de restrição e refeição
					if (filtro.preferencias.refeicao && data.tag_refeicao) {
						const $in = []
						$in.push(mongoose.Types.ObjectId(data.tag_refeicao))
						match = {
							$and: [{
								"tags": { $nin }
							},{
								"tags": { $in }
							}]
						}
					// filtrando tags de restrição e tipo
					} else if(tags_orderBy) {
						const $in = tags_orderBy

						match = {
							$and: [{
								"tags": { $nin }
							},{
								"tags": { $in }
							}]
						}
					} else {
						// filtrar apenas tags de restrição
						match = {
							"tags": { $nin }
						}
					}
				} else {
					// se não houver filtro do usuário (usuário visitante), deve retornar apenas as receitas daquela refeição
					match = {
						"tags": { $in : [mongoose.Types.ObjectId(data.tag_refeicao)] }
					}
				}
				_r.match(match)
				// explodindo campo tags
				// agrupando o _id como tags e receitas daquelas tags
				_r.unwind("tags")
				_r.group({
					_id: "$tags",
					receitas: {
						$push: {
							_id: "$_id",
							titulo: "$titulo",
							favorito: {
								$setIsSubset: [
									[mongoose.Types.ObjectId(data.user_id)],
									"$favoritos"
								]
							},
						}
					}
				})
			}
			// altera o nome de _id para tags novamente 
			_r.project({_id: 0, tags: "$_id", receitas: "$receitas"})
			_r.exec(function(err, _r){
				if (err){
					res.status(500).send(err)
				} else {
					callback(null, _r, tags_orderBy)
				}
			})
		},
		function(receitas, tags_orderBy, callback){
			if(tags_orderBy){
				receitas = receitas.filter(function(r) {
					let find = tags_orderBy.findIndex(function(t){
						return t.toString() === r.tags.toString()
					})
					return find >= 0
				})
			}
			// Popula o conteudo das tags para obter o nome
			Receita.populate(receitas, {
				path: "tags",
				model: "Tag"
			}, function(err, receitas){
				if (err){
					res.status(500).send(err)
				} else {
					receitas = receitas.sort((a, b) => {
						var nameA = a.tags.toObject().titulo // ignore upper and lowercase
						var nameB = b.tags.toObject().titulo // ignore upper and lowercase

						if (nameA < nameB) {
							return -1
						}
						if (nameA > nameB) {
							return 1
						}
						// names must be equal
						return 0
					})
					callback(null, receitas)
				}
			})
		},
		function(receitas, callback){
			Favorito.find({"user_id": data.user_id, active: true})
				.select({ _id: 0, receita_id: 1 })
				.populate({
					path: "receita_id",
					select: {
						titulo: 1,
						favoritos: 1
					}
				})
				.exec(function(err, _f){
					if (err){
						res.status(500).send(err)
					} else {
						if (_f.length > 0) {
							var favoritos = { receitas: [], tags: { titulo: "favoritos", tipo: "favoritos" } }
							for (var i = 0; i < _f.length; i++) {
								favoritos.receitas.push(JSON.parse(JSON.stringify(_f[i].receita_id)))
								if (favoritos.receitas.length) {
									var fav = favoritos.receitas[i].favoritos.some(
										function(user_id){
											return user_id.toString() === data.user_id
										})
									delete favoritos.receitas[i].favoritos
									favoritos.receitas[i].favorito = fav
								}
							}
							receitas.unshift(favoritos)
						}


						callback(receitas)
					}
				})
		}
	], function(receitas){
		res.status(200).json(receitas)
	})
}

exports.createOrUpdate = function (req, res) {
	var data = req.body.data
	if (data._id === undefined) {
		data._id = mongoose.Types.ObjectId()
		var r = new Receita(data)
		r.save(function(err, receita){
			if (err) {
				res.status(500).send(err)
			} else {
				res.status(200).send(receita)
			}
		})
	} else {
		Receita.update({_id: data._id}, data)
			.exec(function (err, receita) {
				if (err) {
					res.status(500).send(err)
				} else {
					res.status(200).send(receita)
				}
			})
	}
}

exports.remove = function(req, res, paginate = true){
	var data = req.body.data, options = req.body.options
	if(paginate){
		Receita.remove({_id: data._id})
			.exec(function(){
				Receita.paginate({}, options, function(err, data) {
					if (err){
						res.status(500).send(err)
					} else {
						res.status(200).json(data)
					}
				})
			})
	} else {
		Receita.remove({_id: data._id});
	}
}

exports.getOne = function(req, res) {
	const data = req.body.data,
		options = req.body.options

	const receita = Receita.findOne(data)
	if (options.populate) {receita.populate(options.populate)}
	receita.exec(function(err, data) {
		if (err){
			res.status(500).send(err)
		} else {
			res.status(200).json(data)
		}
	})
}

exports.uploadImage = function (req, res) {
	var dimensions = sizeOf(req.file.path)
	var w, h = -1

	if(req.body.attr === "icone") {
		w = 600
		h = 375
	}

	if (dimensions.width > w || dimensions.height > h ){
		fs.unlink(req.file.path, function () {
			res.status(500).send({err: "Imagem grande demais. Ela deve ter " + w + " de largura e " + h + " de altura."})
		})
	} else {
		res.status(200).send(req.file.filename)
	}
}

/* this function reads a xls file and import*/
exports.uploadXls = function(req, res){
	//exporting workbook
	console.log("req");
	var wb = XLSX.readFile(req.file.path)

	// export 1st worksheet
	var ws = wb.Sheets[wb.SheetNames[0]]
	var json =  XLSX.utils.sheet_to_json(ws)

	var save = false

	var receitaBody = {}
	var receitaBodyArray = []

	async.waterfall([

		function(callback){

			_.forEach(json, function(j) {

				// Titulo e informações que não são um array
				if (j.titulo !== undefined) {
					if (save) {
						debug("before", receitaBody.tags)
						receitaBody.tags = receitaBody.tags.filter(n => n !== undefined)
						debug("after", receitaBody.tags)
						receitaBodyArray.push(receitaBody)
					}
					receitaBody = {
						ingredientes: [],
						tags: [j.tag_id_tempo, j.tag_id_porcao],
						informacoes_nutricionais: [],
						dicas: [],
						titulo: j.titulo,
						modo_de_preparo: j.modo_de_preparo,
						icone: "",
					}
					save = false
				// Outras informações que precisam ser inseridas uma a uma
				} else {
					receitaBody.ingredientes.push({
						ingrediente: j.ingrediente,
						porcao: j.quantidade_comercial,
						unidade: j.unidade_comercial,
						quantidade_domestica: j.unidade_domestica,
						porcao_domestica: j.quantidade_domestica
					})

					if (j.tags !== undefined) { receitaBody.tags.push(j.tag_id) }
					if (j.dicas !== undefined) { receitaBody.dicas.push(j.dicas) }
					if (j.informacoes_nutricionais !== undefined) { receitaBody.informacoes_nutricionais.push(j.informacoes_nutricionais) }
					save = true
				}
			})
			callback(null, receitaBodyArray)
		},
		function(receitaBodyArray, callback){
			async.each(receitaBodyArray, function(r, next){
				Receita.create(r, function(err){
					if (err){
						callback(err)
					} else {
						next()
					}
				})
			}, function(err){
				if(err){
					callback(err)
				} else {
					callback(null, receitaBodyArray)
				}
			})
		}
	],
	function(err){
		if (err){
			res.status(500).send(err)
		} else {
			res.status(200).json(true)
		}
	})
}
