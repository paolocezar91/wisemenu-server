var mongoose = require('mongoose');
var Filtro = require('../models/filtro.js');

exports.getAll = function(req, res) {
	var data = req.body.data;
	var options = req.body.options;
	var filtros = Filtro.find(data);

	filtros.exec(function(err, data) {
		if (err){
			res.status(500).send(err);
		} else {
			res.status(200).send(data);
		}
	});
}

exports.getOne = function(req, res) {
	var data = req.body.data;
	var options = req.body.options;

	if (options.aggregate) {

	} else {
		var filtro = Filtro.findOne(data);
	
		if (options.populate) {filtro.populate(options.populate)}

		filtro.exec(function(err, data) {
			if (err){
				res.status(500).send(err);
			} else {
				if (data != null) {
					res.status(200).send(data);
				} else {
					res.status(200).send(false);
				}
			}
		});
	}
}

exports.create = function(req, res, next){

	

	var filtro = new Filtro(req.body);

	filtro.markModified('anything');
	filtro.save(function(err, data){
		if (err){
			res.status(500).send(err);
		} else {
			res.status(200).send(data);
		}
	});
 
}

exports.createOrUpdate = function(req, res){
	// Criando um id novo, se não houver um.
	var data = req.body.data;

	if(data._id == null){
		data._id = new mongoose.Types.ObjectId();
	}

	Filtro.update({_id: data._id}, 
		data,
		{upsert: true},
		function(err, data){
			if(err)
				res.status(500).send(err);
			else
				res.status(200).json(data);
		}
	);
}