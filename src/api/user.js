var User = require("../models/user")

exports.getAll = function(req, res) {
	User.find({}, function(err, data) {
		if (err){
			res.status(500).send(err)
		} else {
			res.send(data)
		}
	})
}

exports.getOne = function(req, res) {
	// Não pegar perfil
	User.findOne({_id: req.params.user_id}, function(err, data) {
		if (err){
			res.status(500).send(err)
		} else {
			res.send(data)
		}
	})
}

exports.getPerfil = function(req, res) {
	// Fazer select para pegar apenas perfil
	User.findOne({_id: req.params.user_id}, function(err, data) {
		if (err){
			res.status(500).send(err)
		} else {
			res.send(data)
		}
	})
}

exports.create = function(req, res, next){
	var email = req.body.email
	var password = req.body.password
	var role = req.body.role

	User.findOne({email: email}, function(err, existingUser){

		if(err){
			return next(err)
		}

		if(existingUser){
			return res.status(422).send({error: "Este e-mail está indisponível"})
		}

		var user = new User({
			email: email,
			password: password,
			role: role
		})

		user.save(function(err, data){
			if (err){
				res.status(500).send(err)
			} else {
				res.send(data)
			}
		})
	})
}