var mongoose = require('mongoose');
var Favorito = require('../models/favorito.js');
var Receita = require('../models/receita.js');
var async = require('async');

exports.getAll = function(req, res) {
  var data = req.body.data;
  var options = req.body.options;
  var favoritos = Favorito.find(data);
  if (options.select) {favoritos.select(options.select)}
  if (options.populate) {favoritos.populate(options.populate)}

  favoritos.exec(function(err, data) {
    if (err){
      res.status(500).send(err);
    } else {
      res.status(200).send(data);
    }
  });
}

exports.getOne = function(req, res) {
  var data = req.body.data;
  var options = req.body.options;

  if (options.aggregate) {

  } else {
    var favorito = Favorito.findOne(data);
  
    if (options.populate) {favorito.populate(options.populate)}

    favorito.exec(function(err, data) {
      if (err){
        res.status(500).send(err);
      } else {
        if (data != null) {
          res.status(200).send(data);
        } else {
          res.status(200).send({active: false});
        }
      }
    });
  }
}

exports.create = function(req, res, next){

  

  var favorito = new Favorito(req.body);

  favorito.markModified('anything');
  favorito.save(function(err, data){
    if (err){
      res.status(500).send(err);
    } else {
      res.status(200).send(data);
    }
  });
 
}

exports.createOrUpdate = function(req, res){
  // Criando um id novo, se não houver um.
  var data = req.body.data;
  // if(data._id == null){
  //   data._id = new mongoose.Types.ObjectId();
  // }

  async.waterfall([
    function(callback){
      Favorito.update({user_id: data.user_id, receita_id: data.receita_id}, 
        data,
        {upsert: true},
        function(err, favorito){
          if(err)
            res.status(500).send(err);
          else{
            callback(null, favorito);
          }
        }
      );
    },
    function(favorito, callback){
      console.log(data.active); 
      if (data.active) {
        update = { $addToSet: { favoritos: data.user_id } }
      } else {
        update = { $pull: { favoritos: data.user_id } }
      }
      Receita.update(
        {_id: data.receita_id},
        update
      ).exec(function(err, receita){
        callback(favorito);
      })
    }
  ], 
  function(favorito){
    res.status(200).json(favorito);
  })
}