const Lista = require("../models/lista.js"),
	Receita = require("../models/receita.js"),
	Produto = require("../models/produto.js"),
	ObjectId = require("mongoose").mongo.ObjectId,
	async = require("async"),
	_ = require("lodash"),
	debug = require("debug")("env")

exports.getAll = function(req, res) {
	const data = req.body.data,
		options = req.body.options

	debug("data", data)

	const listas = Lista.find(data)
	if(options.select){listas.select(options.select)}
	if(options.count){listas.count()}
	if(options.populate){listas.populate(options.populate.field, options.populate.select)}
	listas.exec(function(err, listas) {
		if (err){
			res.status(500).send(err)
		} else {
			res.status(200).json(listas)
		}
	})
}

exports.getOne = function(req, res) {    
	const data = req.body.data,
		options = req.body.options

	const lista = Lista.findOne(data)
	if(options.populate){
		for (var i = options.populate.length - 1; i >= 0; i--) {
			const pop = options.populate[i]
			lista.populate(pop.field, pop.select)
		}
	}

	lista.exec(function(err, lista) {
		if (err){
			res.status(500).send(err)
		} else {
			res.status(200).json(lista)
		}
	})
}

exports.create = function(req, res){
	const data = req.body.data

	// Soma a coluna porcoes de cada receita cujo _id for igual
	data.receitas = _.chain(data.receitas)
		.flatten()
		.groupBy("_id")
		.map(function(value, key) {
			var sum = _.reduce(value, function(memo, val) { return Number(memo) + Number(val.porcoes) }, 0)
			return {_id: key, porcoes: sum}
		})
		.value()

	async.waterfall([
		function(callback){
			Receita.find({"_id": { $in: data.receitas}}).select({_id: 1, ingredientes: 1}).exec().then(
				function(receitas){
					callback(null, receitas)
				}, function(err){
					res.status(500).send(err)
				})  
		},
		// Função para realizar em cada receita recuperada do cardapio
		function(receitas, callback){
			// cria lista
			var lista = {titulo: data.titulo, user_id: data.user_id, cardapio_id: data.cardapio_id, itens: []}
			// Cria um array pat (produto asynchrone tasks) de funções que serão executadas em série, assincronicamente na proxima função do waterfall
			var tap = []

			var porcoes

			// Itera sob as receitas
			_.forEach(receitas, function(_r){
				var r = _r.toObject()
				
				// Itera sob os ingredientes da receitas
				_.forEach(r.ingredientes, function(ing){
					var it = {}
					it.titulo = ing.ingrediente.toLowerCase()
					it.quantidade = ing.porcao
					it.unidade = ing.unidade
					

					// Se as porções de cada receita são maiores que 1, ele deve adicionar cada ingrediente multiplicado pelo numero de porções
					porcoes = _.find(data.receitas, function(dr){ return dr._id == r._id }).porcoes
					
					for (var j = porcoes - 1; j >= 0; j--) {
						lista.itens.push(it)
					}

					// Adiciona as funções para o array tap
					tap.push(function(cb){
						//checa se o ingrediente já é um, produto, ou sinonimo. Se não for, o cria.
						Produto.findOne({
							$or: [
								{ titulo: it.titulo },
								{ sinonimos: it.titulo }
							]
						})
							.select("_id titulo sinonimos")
							.exec(function(err, _prod){
								if (_prod) { // Se produto existir
									cb( null, _prod.toObject() )
								} else { // Se não existir
									var produtoData = {
										titulo: it.titulo,
										sinonimos: [],
										unidades_domesticas: [],
										unidades_comerciais: [],
										marcas_id: []
									}

									//cria o produto
									new Produto(produtoData).save(function(err, _prod){
										cb( null, { _id: _prod._id, titulo: _prod.titulo, sinonimos: _prod.sinonimos } )
									})
								}
							})
					})
				})
			})      
			callback(null, lista, tap)
		},

		function(lista, tap, callback){
			// serie assincrona de funções para criar os Produtos
			async.series(tap, function(err, results) {
				callback(null, lista, results)
			})
			
		},

		// Agrega os produtos encontrados/recem-criados com seus sinonimos e iguais 
		// (para caso de receitas diferentes com o mesmo ingrediente, ou sinonimos, como "ovo" e "gema de ovo")
		function(lista, produtosData, callback){
			_.forEach(lista.itens, function(item){
				item.produto_id = _.find(produtosData, function(produto){              
					if(item.titulo === produto.titulo){
						// produtosData = produtosData.filter(p => p !== produto)
						return true
					} else {
						return _.find(produto.sinonimos, function(sinonimo){
							if(item.titulo === sinonimo){                    
								// produtosData = produtosData.filter(p => p !== produto)
								return true
							}
						})
					}
				})._id
			})

			callback(null, lista)
		},
		// Soma todos os produtos repetidos com suas unidades
		function(lista, callback){
			lista.itens = _.chain(lista.itens)
				.flatten()
				.groupBy("produto_id")
				.map(function(value, key) {
					//reduce para somar a quantidade
					var sum = _.reduce(value, function(memo, val) { return Number(memo) + Number(val.quantidade) }, 0)
					//retorna novo item
					if (isNaN(sum)) { sum = 0 }
					return {produto_id: key, quantidade: sum, unidade: _.first(value).unidade}
				})
				.value()
			callback(lista)
		}
	],

	function(lista){
		new Lista(lista).save(function(err, l){
			if(err){
				res.status(500).send(err)
			} else {
				res.status(200).send(l)
			}
		})        
	}) 
}

exports.update = function(req, res){
	const data = req.body.data

	// Criando um id novo, se não houver um.
	if(data._id == null){
		data._id = new ObjectId()
	}

	Lista.update({_id: data._id, user_id: data.user_id},
		data,
		{upsert: true},
		function(err, data){
			if(err)
				res.status(500).send(err)
			else
				res.status(200).json(data)
		}
	)
}

exports.delete = function(req, res){
	const data = req.body.data

	Lista.remove(data, function(err) {
		if (err){
			res.status(500).send(err)
		} else {
			res.status(200).json("Removido com sucesso")
		}
	})
}
