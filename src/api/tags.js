const mongoose = require("mongoose"),
	Tag = require("../models/tag.js"),
	XLSX = require("xlsx"),
	async = require("async"),
	debug = require("debug")("dev")

exports.getAll = function(req, res) {
	var data = req.body.data,
		options = req.body.options,
		tag

	if (options.aggregate) {
		tag = Tag.aggregate()
			.match({"tipo": {$in: options.match} })
			.group({_id: "$tipo", tags: {$push: {_id: "$_id", titulo: "$titulo", tipo: "$tipo"} } })
			.project({_id: 0, tipo: "$_id", tags: "$tags"})
	} else if(options.page) {
		tag = Tag.paginate(data, options)
	}	else {
		tag = Tag.find(data)

		if (options.sort) {tag.sort(options.sort)}
	}

	tag.then(function(tags) {
		res.status(200).json(tags)
	}).catch(function(err){
		res.status(500).send(err)
	})
}

exports.getOne = function(req, res) {
	var data = req.body.data
	var tag = Tag.findOne(data)

	tag.exec(function(err, data) {
		if (err){
			res.status(500).send(err)
		} else {
			res.status(200).json(data)
		}
	})
}

exports.createOrUpdate = function (req, res) {
	var data = req.body.data
	if (data._id === undefined) {
		data._id = mongoose.Types.ObjectId()
		var r = new Tag(data)
		r.save(function(err, receita){
			if (err) {
				res.status(500).send(err)
			} else {
				res.status(200).send(receita)
			}
		})
	} else {
		Tag.update({_id: data._id}, data)
			.exec(function (err, receita) {
				if (err) {
					res.status(500).send(err)
				} else {
					res.status(200).send(receita)
				}
			})
	}
}

exports.create = function(req, res){
	Tag.create(req.body.data, function(err, data){
		if (err){
			res.status(500).send(err)
		} else {
			res.status(200).send(data)
		}
	})
}

exports.import = function(req, res){
	var wb = XLSX.readFile("./uploads/" + req.file.filename )
	// export 1st worksheet
	var ws = wb.Sheets[wb.SheetNames[0]]
	var json =  XLSX.utils.sheet_to_json(ws)

	async.each(
		json,
		function(j, callback){

			Tag.create(j, function(err){
				if (err){
					res.status(500).send(err)
				} else {
					callback()
				}
			})
		},
		function(err){
			res.status(500).send(err)
		})
	res.status(200).send("Feito")
}