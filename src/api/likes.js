var mongoose = require('mongoose');
var Like = require('../models/like.js');

exports.getAll = function(req, res) {
  var data = req.body.data;
  var options = req.body.options;
  var likes = Like.find(data);

  likes.exec(function(err, data) {
    if (err){
      res.status(500).send(err);
    } else {
      res.status(200).send(data);
    }
  });
}

exports.getOne = function(req, res) {
  var data = req.body.data;
  var options = req.body.options;

  if (options.aggregate) {
  } else {
    var like = Like.findOne(data);
    if (options.populate) {like.populate(options.populate)}
    like.exec(function(err, like) {

      if (err){
        res.status(500).send(err);
      } else {
        Like.find({receita_id: data.receita_id, active: true}, function(err, likesCount){
          if (like) {
            res.status(200).send({ likeData: like, count: likesCount ? likesCount.length : 0 });
          } else {
            res.status(200).send({ likeData: {active: false}, count: likesCount ? likesCount.length : 0 });
          }    
        });

        
      }
    });
  }
}

exports.create = function(req, res, next){

  var like = new Like(req.body);

  like.markModified('anything');
  like.save(function(err, data){
    if (err){
      res.status(500).send(err);
    } else {
      res.status(200).send(data);
    }
  });
 
}

exports.createOrUpdate = function(req, res){
  // Criando um id novo, se não houver um.
  var data = req.body.data;

  if(data._id == null){
    data._id = new mongoose.Types.ObjectId();
  }

  console.log(req.body._id);

  Like.update({_id: data._id}, 
    data,
    {upsert: true},
    function(err, data){
      if(err)
        res.status(500).send(err);
      else
        res.status(200).json(data);
    }
  );
}