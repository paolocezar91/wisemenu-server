const User = require("../models/user"),
	Filtro = require("../models/filtro"),
	authServices = require("../services/auth"),
	debug = require("debug")("dev")

exports.login = function(req, res){
	const userInfo = authServices.setUserInfo(req.user)
	const json = {
		token: "JWT " + authServices.generateToken(userInfo),
		user: userInfo
	}

	res.status(200).send(json)
}

exports.logout = function(req, res){

	const userInfo = authServices.setUserInfo(req.user)

	res.status(200).json({
		token: "JWT " + authServices.generateToken(userInfo),
		user: userInfo
	})
}

exports.facebook = function(req, res, next){
	const name = req.body.name,
		uuid = req.body.uuid,
		facebook_id = req.body.facebook_id

	User.findOne( { "facebook_id": facebook_id } , function(err, existingUser){
		debug(existingUser)

		if (existingUser != null) {
			const userInfo = authServices.setUserInfo(existingUser)
			return res.status(201).json({
				token: "JWT " + authServices.generateToken(userInfo),
				user: userInfo
			})
		} else {
			const user = new User({
				name: name,
				facebook_id: facebook_id,
				uuid: uuid
			})

			user.save(function(err, user){
				if(err){return next(err)}
				const filtro = new Filtro({user_id: user._id})
				filtro.save((err) => {
					if(err){return next(err)}

					const userInfo = authServices.setUserInfo(user)
					return res.status(201).json({
						token: "JWT " + authServices.generateToken(userInfo),
						user: userInfo
					})
				})
			})
		}
	})
}

exports.guest = function(req, res, next){
	const email = req.body.email,
		name = req.body.name,
		uuid = req.body.uuid,
		guest = req.body.guest,
		role = "guest"

	if (!guest) {
		return res.status(422).send({error: "Houve um erro ao fazer seu acesso"})
	} else {
		User.findOne( { "uuid": uuid, "facebook_id" : { $exists : false } } , function(err, existingUser){
			debug("existingUser", existingUser)

			if (existingUser != null) {
				const userInfo = authServices.setUserInfo(existingUser)
				res.status(201).json({
					token: "JWT " + authServices.generateToken(userInfo),
					user: userInfo
				}).end()
			} else {
				const user = new User({
					name: name,
					email: email,
					uuid: uuid,
					role: role
				})
				debug("user", user)
				user.save(function(err, user){
					if(err){return next(err)}
					const userInfo = authServices.setUserInfo(user)
					res.status(201).json({
						token: "JWT " + authServices.generateToken(userInfo),
						user: userInfo
					})
				})
			}
		})
	}
}

exports.protected = function(req, res){
	const userInfo = authServices.setUserInfo(req.user)
	res.status(200).send({user: userInfo})
}

exports.error = function (req, res) {
	res.status(401).send({error: req.flash()})
}

exports.register = function(req, res, next){
	const email = req.body.email,
		password = req.body.password,
		name = req.body.name,
		uuid = req.body.uuid

	if(!name){
		return res.status(422).send({error: "Você deve digitar seu nome."})
	}

	if(!email){
		return res.status(422).send({error: "Você deve digitar um e-mail."})
	}

	if(!password){
		return res.status(422).send({error: "Você deve digitar uma senha."})
	}

	User.findOne( { email: email }, function(err, existingUser){
		if(err){
			return next(err)
		}

		if(existingUser){// Usuário já existe
			return res.status(422).send({error: "Este e-mail já está em uso"})
		} else {//Se usuário não existe, o cria

			const user = new User({
				name: name,
				email: email,
				password: password,
				uuid: uuid
			})

			user.save(function(err, user){
				if(err){return next(err)}
				const filtro = new Filtro({user_id: user._id})
				filtro.save((err) => {
					if(err){return next(err)}

					const userInfo = authServices.setUserInfo(user)
					return res.status(201).json({
						token: "JWT " + authServices.generateToken(userInfo),
						user: userInfo
					})
				})
			})
		}
	})
}

exports.resetPassword = function(req, res){

	User.findOne({email: req.body.email}, function(err, user){
		user.password = req.body.newPassword
		user.save(function(err){
			if(err){
				res.status(422).send({
					error: "Erro ao atualizar a senha"
				})
			} else {
				res.status(200).json({
					success: "Senha atualizada"
				})
			}
		})
	})
}
