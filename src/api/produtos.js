var mongoose = require('mongoose');
var Produto = require('../models/produto.js');

exports.getAll = function(req, res) {
  var data = req.body.data;
  var options = req.body.options;

  var produtos = Produto.find(data);
  if (options.select) {produtos.select(options.select)}
  produtos.exec(function(err, data) {
    if (err){
      res.status(500).send(err);
    } else {
      res.status(200).send(data);
    }
  });
}

exports.getOne = function(req, res) {
  var data = req.body.data;
  var options = req.body.options;

  if (options.aggregate) {
  } else {
    var produto = Produto.findOne(data);
    if (options.populate) {produto.populate(options.populate)}
    produto.exec(function(err, produto) {

      if (err){
        res.status(500).send(err);
      } else {
        Produto.find({receita_id: data.receita_id, active: true}, function(err, produtosCount){
          if (produto) {
            res.status(200).send({ produtoData: produto, count: produtosCount ? produtosCount.length : 0 });
          } else {
            res.status(200).send({ produtoData: {active: false}, count: produtosCount ? produtosCount.length : 0 });
          }    
        });

        
      }
    });
  }
}

exports.create = function(req, res, next){


  var data = req.body.data;
  console.log(data);

  Produto.findOrCreate({titulo: data.titulo}, data,
    function(err, produto, created){
      if (err){
        res.status(500).send(err);
      } else {
        res.status(200).send(produto);
      }
    });
 
}

exports.createOrUpdate = function(req, res){
  // Criando um id novo, se não houver um.
  var data = req.body.data;

  if(data._id == null){
    data._id = new mongoose.Types.ObjectId();
  }

  Produto.update({_id: data._id}, 
    data,
    {upsert: true},
    function(err, data){
      if(err)
        res.status(500).send(err);
      else
        res.status(200).json(data);
    }
  );
}