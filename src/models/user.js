var mongoose = require('mongoose'), Schema = mongoose.Schema, ObjectId = Schema.ObjectId;
var bcrypt   = require('bcrypt-nodejs');
 
var userSchema = new Schema({
    name:{
        type: String,
        required: true
    },
    email: {
        type: String,
        lowercase: true,
    },
    password: {
        type: String,
    },
    role: {
        type: String,
        default: 'user'
    },
    attempts: {
        type: Number,
        default: 0
    },
    facebook:{
        id: String,
        name: String,
        token: String,
    }

}, {
    collection: 'users',
    "strict": false,
    timestamps: true
});
 
userSchema.pre('save', function(next){
 
    var user = this;
    var SALT_FACTOR = 5;
 
    if(!user.isModified('password')){
        return next();
    } 
 
    bcrypt.genSalt(SALT_FACTOR, function(err, salt){
 
        if(err){
            return next(err);
        }
 
        bcrypt.hash(user.password, salt, null, function(err, hash){
 
            if(err){
                return next(err);
            }
 
            user.password = hash;
            next();
 
        });
 
    });
 
});
 
userSchema.methods.comparePassword = function(passwordAttempt, cb){
 
    bcrypt.compare(passwordAttempt, this.password, function(err, isMatch){
 
        if(err){
            return cb(err);
        } else {
            cb(null, isMatch);
        }
    });
 
}
 
module.exports = mongoose.model('User', userSchema);