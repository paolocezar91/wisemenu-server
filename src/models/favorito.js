var mongoose = require('mongoose'), Schema = mongoose.Schema;

var favoritoSchema = new Schema({
	user_id: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	receita_id: {
		type: Schema.ObjectId,
		ref: 'Receita'
	},
	
}, {
	strict: false,
	timestamps: true,
	collection: 'favoritos'
});

module.exports = mongoose.model('Favorito', favoritoSchema);