const mongoose = require("mongoose"),
	Schema = mongoose.Schema,
	mongoosePaginate = require("mongoose-paginate")

mongoose.Promise = global.Promise

var receitaSchema = new Schema({
	titulo: String,
	favoritos: [{
		type: Schema.ObjectId,
		ref: "User",
		default: []
	}],
	tags: [{
		type: Schema.ObjectId,
		ref: "Tag",
		default: []
	}]
},
{
	strict: false,
	collection: "receitas"
})
receitaSchema.index({ titulo: "text" })
receitaSchema.plugin(mongoosePaginate)

module.exports = mongoose.model("Receita", receitaSchema)
