var findOrCreate = require("mongoose-findorcreate")
var mongoose = require("mongoose"), Schema = mongoose.Schema,
	mongoosePaginate = require("mongoose-paginate")

var tagSchema = new Schema(
	{ parent_id: {type: Schema.ObjectId, ref: "Tag"} },
	{ "strict": false}
)
tagSchema.plugin(findOrCreate)
tagSchema.plugin(mongoosePaginate)

module.exports = mongoose.model("Tag", tagSchema)