var mongoose = require('mongoose'), Schema = mongoose.Schema, ObjectId = Schema.ObjectId;

var cardapioSchema = new Schema({
		user_id: {
			type: Schema.ObjectId,
			ref: 'User'
		},
	}, 	{
		strict: false, 
		timestamps: true
	}
);

module.exports = mongoose.model("Cardapio", cardapioSchema);