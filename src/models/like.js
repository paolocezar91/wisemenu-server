var mongoose = require('mongoose'), Schema = mongoose.Schema;

var likeSchema = new Schema({
	user_id: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	receita_id: {
		type: Schema.ObjectId,
		ref: 'Receita'
	},
	
}, {
	strict: false,
	timestamps: true,
	collection: 'likes'
});

module.exports = mongoose.model('Like', likeSchema);