var mongoose = require('mongoose'), Schema = mongoose.Schema;

var filtroSchema = new Schema({
	user_id: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	preferencias: {
		refeicao: {type: Boolean, default: false}, 
		favoritos: {type: Boolean, default: true}, 
		tags: [
			{
				type: Schema.ObjectId,
				ref: 'Tag'
			}
		]
	},
	restricoes: {
		tags: [
			{
				type: Schema.ObjectId,
				ref: 'Tag'
			}
		]
	}
}, {
	strict: false,
	timestamps: true,
	collection: 'filtros'
});

module.exports = mongoose.model('Filtro', filtroSchema);