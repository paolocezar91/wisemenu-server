var mongoose = require('mongoose'), Schema = mongoose.Schema, ObjectId = Schema.ObjectId;

var listaSchema = new Schema({
		user_id: {
			type: Schema.ObjectId,
			ref: 'User'
		}, 
		cardapio_id: {
			type: Schema.ObjectId,
			ref: 'Cardapio'
		},
		itens: [{
			produto_id: {
				type: Schema.ObjectId,
				ref: 'Produto'
			},
			quantidade: {
				type: Number,
			},
			unidade: {
				type: String
			}
		}]
	}, {
		strict: false, 
		timestamps: true
	});

module.exports = mongoose.model("Lista", listaSchema);