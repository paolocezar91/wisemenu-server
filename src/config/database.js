require('dotenv').config();

module.exports = {
	'user': process.env.DB_USER,
	'password': process.env.DB_PASSWORD,
	'authSource': process.env.DB_AUTHSOURCE,
	'db_name': process.env.DB_NAME
}