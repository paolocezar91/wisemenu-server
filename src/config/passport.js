const passport = require("passport"),
	User = require("../models/user"),
	config = require("./auth"),
	JwtStrategy = require("passport-jwt").Strategy,
	ExtractJwt = require("passport-jwt").ExtractJwt,
	LocalStrategy = require("passport-local").Strategy,
	debug = require("debug")("dev")

const localOptions = {
	usernameField: "email",
	passwordField: "password",
	passReqToCallback : true
}

const localLogin = new LocalStrategy(localOptions, function(req, email, password, done){
	User.findOne({
		email: email
	}, function(err, user){
		if(err){return done(err, false)}
		if(!user){return done(null, false)}
		if (user.attempts < 3) {
			user.comparePassword(password, function(err, isMatch){
				if(err){return done(err, false)}
				else {
					if(!isMatch){
						//user.attempts = user.attempts + 1
						user.save(function(){
							return done(null, false)
						})
					} else {
						// Sucesso
						return done(null, user)
					}
				}
			})
		} else {return done(null, false)}
	})
})
 
const jwtOptions = {
	jwtFromRequest: ExtractJwt.fromAuthHeader(),
	secretOrKey: config.secret
}

const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done){ 
	User.findById(payload._id, function(err, user){
		if(err){
			return done(err, false)
		}
		if(user){
			return done(null, user)
		} else {
			return done(null, false)
		}
	})
})

passport.use(jwtLogin)
passport.use(localLogin)