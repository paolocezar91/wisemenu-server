const jwt = require("jsonwebtoken"),
	authConfig = require("../config/auth")

exports.generateToken = (user) => {
	return jwt.sign(user, authConfig.secret, {
		expiresIn: "30 days"
	})
}

exports.setUserInfo = (user) => {
	return {
		_id: user._id,
		email: user.email,
		name: user.name,
		role: user.role,
	}
}

exports.roleAuthorization = (role) => {
	return (req, res, next) => {
		if(req.user.role == role)
			return next()
		else
			return next("Error, should have " + role + " role")
	}
}